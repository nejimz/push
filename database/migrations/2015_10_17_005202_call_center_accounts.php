<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CallCenterAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('call_center_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account')->unique();
        });

        DB::table('call_center_accounts')->insert( [ 'account' => 'StraightTalk' ] );    
        DB::table('call_center_accounts')->insert( [ 'account' => 'SimpleMobile' ] );   
        DB::table('call_center_accounts')->insert( [ 'account' => 'PagePlus' ] );   
        DB::table('call_center_accounts')->insert( [ 'account' => 'Legacy' ] );   
        DB::table('call_center_accounts')->insert( [ 'account' => 'Tracfone Wireless' ] );   
        DB::table('call_center_accounts')->insert( [ 'account' => 'SafeLink' ] );   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('call_center_accounts');
    }
}
