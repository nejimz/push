<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FileUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('file_uploads', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('file_type')->unsigned();
            $table->foreign('file_type')->references('id')->on('file_upload_type');

            $table->string('file_name')->unique();
            $table->text('file_location');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('file_uploads');
    }
}
