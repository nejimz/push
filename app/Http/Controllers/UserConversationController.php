<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UsersConversation;
use App\UsersAdmin;

use Auth;

class UserConversationController extends Controller
{
    public function __construct()
    {
        if(UsersAdmin::where('ntlogin', Auth::user()->ntlogin)->count() == 0)
        {
            abort(404);
        }
    }

    public function index()
    {
        //
        $users = UsersConversation::leftJoin('users', 'users_conversation.ntlogin', '=', 'users.ntlogin')
                    ->whereNotNull('users.id')
                    ->orderBy('name', 'ASC')
                    ->paginate(50);

        $data = compact('users');
        
        return view('push.cpanel.conversation.index', $data);
    }

    public function create()
    {
        //
        return view('push.cpanel.conversation.add');
    }

    public function store(Request $request)
    {
        //
        $rules = [
                'ntlogin'=>'required|unique:users_conversation,ntlogin|exists:users,ntlogin',
            ];
        $message = [
                'ntlogin.required'  => 'Ntlogin is required.',
                'ntlogin.unique'    => 'Ntlogin already exists.',
                'ntlogin.exists'    => 'Ntlogin not exists.'
            ];

        $this->validate($request, $rules, $message);
        //
        $admin = new UsersConversation;
        $admin->ntlogin = trim($request->input('ntlogin'));
        $admin->save();

        return redirect()->route('conversation_create')->with('success', 'Ntlogin successfully added!');
    }

    public function destroy($ntlogin)
    {
        //
        UsersConversation::where('ntlogin', $ntlogin)->delete();

        return redirect()->route('conversation_index')->with('success', 'Ntlogin successfully deleted!');
    }
}
