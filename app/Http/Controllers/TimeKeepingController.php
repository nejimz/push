<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Users;
use Auth;
use DB;
use Validator;
use App\TimeKeeping;
use App\OuTimekeeping;

class TimeKeepingController extends Controller
{
    /**
    Filter
    **/
    public function __construct()
    {

    }

    /**
    Ezwatch
    **/
    public function ezwatch()
    {
        $departments = OuTimekeeping::departmentOptions();

        if(count($departments)>1)
        {
            $data = [
                'departments' => $departments
            ];
    	   return view('push.timekeeping.ezwatch', $data);
        }
        else
        {
            abort(404);
        }
    }

    /**
    Show Employee list
    **/
    public function ezsearchemployee(Request $request)
    {
    	$employee_list = TimeKeeping::employeeList($request->department)->get(); 
		$employee_select = "<option value='0'>-- Select Name --</option>";
		
		foreach($employee_list as $employee)
		{
    		$employee_select .= '<option value="'.$employee->EmpId.'">'.$employee->EmpName.'</option>';
		}

		return $employee_select;
    }

    /**
    Generate LILO
    **/
    public function ezsearchlilogenerate(Request $request)
    {
    	if($request->ez_dept != 0)
    	{
            if($request->ez_start >= $request->ez_end)
            {
                echo "$('#ez-table tbody').html('<tr><td colspan=6><div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Invalid date argument.</div></td></tr>'); ";
            }
            else
            {
                $ez_table_query = TimeKeeping::liloResult($request->ez_dept, $request->ez_start, $request->ez_end);

                if($request->ez_emp != 0)
                {
                    $ez_table_query->where('EmpId','=',$request->ez_emp);
                }

                $ez_table_result = $ez_table_query->get();

                $ez_table = "";

                foreach ($ez_table_result as $lilo) 
                {
                   $ez_table .= '<tr onclick="ezpreview('.$lilo->EmpId.', '.$lilo->TransactionId.')"><td>'.$lilo->EmpCode.'</td>'.
                                 '<td>'.$lilo->SectionName.'</td>'.
                                 '<td>'.$lilo->EmpName. '</td>'.
                                 '<td>'.date('m/d/Y H:i', strtotime($lilo->TransactionDateTime)).'</td>'.
                                 '<td>'.$lilo->TerminalName.'</td>'.
                                 '<td>'.$lilo->TransactionTypeName.'</td>'.
                                 '</tr>';
                }
                if($ez_table == '')
                {
                    echo "$('#ez-table tbody').html('<tr><td colspan=6><center>No data to return.</center></td></tr>'); ";
                }
                else
                {
                    echo "$('#ez-table tbody').html(".json_encode($ez_table).")";
                }                
            }    		
    	}
    	else
    	{
    		echo "$('#ez-table tbody').html('<tr><td colspan=6><div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Please fill-up the provided form.</div></td></tr>'); ";
    	}
    }

    /**
    Download LILO
    **/
    public function ezsearchlilodownload(Request $request)
    {
        if($request->ez_dept != 0)
        {
            if($request->ez_start >= $request->ez_end)
            {
                echo "$('#ez-table tbody').html('<tr><td colspan=6><div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Invalid date argument.</div></td></tr>'); ";
            }
            else
            {
                $ez_table_query = TimeKeeping::liloResult($request->ez_dept, $request->ez_start, $request->ez_end);

                if($request->ez_emp != 0)
                {
                    $ez_table_query->where('EmpId','=',$request->ez_emp);
                }

                $ez_table_result = $ez_table_query->get();

                $ez_table = "Emp. No.\t"."Department\t"."Name\t"."Attendance Date Time\t"."Trans. Type\n";

                foreach ($ez_table_result as $lilo) 
                {
                   $ez_table .= $lilo->EmpCode."\t".
                                $lilo->SectionName."\t".
                                $lilo->EmpName."\t".
                                $lilo->TransactionDateTime."\t".
                                $lilo->TransactionTypeName."\n";
                }
                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=LILO".$request->ez_emp."_".date('Ymdhis').".xls");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Pragma: public");
                print $ez_table;
            }           
        }
        else
        {   
            echo "$('#ez-table tbody').html('<tr><td colspan=6><div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Please fill-up the provided form.</div></td></tr>'); ";
        }
    }

    /**
    Preview LILO images
    **/
    public function ezsearchlilopreview(Request $request)
    {
        echo "$('#ez-archive').attr('src', '". route('ez_preview_archive') ."?empid=".$request->empid."').error(function(){ Holder.run({ images : '#ez-archive' }) }); ";
        echo "$('#ez-capture').attr('src', '". route('ez_preview_transaction') ."?transid=".$request->transid."').error(function(){ Holder.run({ images : '#ez-capture' }) }); ";
    }

    /**
    Archive Image
    **/
    public function archivepreview(Request $request)
    {
        if($request->empid != "")
        {
            $ez_preview = DB::connection('ezpayroll')
                                ->table('EmployeeImage')
                                ->where('EmpId','=',$request->empid)->select()->first();

            if($ez_preview->Image){
                header('Content-Type: image/jpeg');
                echo pack('H*', strtolower($ez_preview->Image));
            }
        }
    }

    /**
    Transaction Image
    **/
    public function transactionpreview(Request $request)
    {
        if($request->transid != "")
        {
            $ez_preview = DB::connection('ezwatch')
                                ->table('TKTimekeepingTransaction')
                                ->where('TransactionId','=',$request->transid)->select()->first();

            header('Content-Type: image/jpeg');
            echo pack('H*', strtolower($ez_preview->Image));
        }
    }

    /**
    Doorlog
    **/
    public function doorlog()
    {
    	$data = [];

    	return view('push.timekeeping.doorlog', $data);
    }
}