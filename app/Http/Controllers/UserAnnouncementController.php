<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UsersAnnouncement;
use App\Announcements;
use App\UsersAdmin;

use Auth;

class UserAnnouncementController extends Controller
{

    public function __construct()
    {
        if(UsersAdmin::where('ntlogin', Auth::user()->ntlogin)->count() == 0)
        {
            abort(404);
        }
    }

    public function index()
    {
        //
        $users = UsersAnnouncement::leftJoin('users', 'users_announcement.ntlogin', '=', 'users.ntlogin')
                    ->whereNotNull('users.id')
                    ->orderBy('name', 'ASC')
                    ->paginate(50);

        $data = compact('users');
        
        return view('push.cpanel.announcements.index', $data);
    }

    public function create()
    {
        //
        return view('push.cpanel.announcements.add');
    }

    public function show()
    {
        //
        $count_announcement = Announcements::where('active', 1)->count();
        $announcements = Announcements::orderBy('active', 'DESC')->orderBy('created_at', 'DESC')->paginate(50);

        $data = compact('announcements', 'count_announcement');

        return view('push.cpanel.announcements.show', $data);
    }

    public function store(Request $request)
    {
        //
        $rules = [
                'ntlogin'=>'required|unique:users_announcement,ntlogin|exists:users,ntlogin',
            ];
        $message = [
                'ntlogin.required'  => 'Ntlogin is required.',
                'ntlogin.unique'    => 'Ntlogin already exists.',
                'ntlogin.exists'    => 'Ntlogin not exists.'
            ];

        $this->validate($request, $rules, $message);
        //
        $admin = new UsersAnnouncement;
        $admin->ntlogin = trim($request->input('ntlogin'));
        $admin->save();

        return redirect()->route('announcement_create')->with('success', 'Ntlogin successfully added!');
    }

    public function update($id, $active)
    {
        //
        Announcements::where('id', $id)->update(['active'=>$active]);

        return redirect()->route('announcement_show')->with('success', 'Announcement successfully update to ' . ucwords(($active)?'Active':'Inactive') . '!');
    }

    public function destroy($ntlogin)
    {
        //
        UsersAnnouncement::where('ntlogin', $ntlogin)->delete();

        return redirect()->route('announcement_index')->with('success', 'Ntlogin successfully deleted!');
    }
}
