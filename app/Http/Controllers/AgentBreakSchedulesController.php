<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AgentBreakSchedule;
use Validator;

class AgentBreakSchedulesController extends Controller
{

    public function fileUploadsValidationRules() 
    {
        return [
            #'file' => 'required|mimes:csv,text'
            'file' => 'required',
            'extension' => 'in:csv'
        ];
    }

    public function fileUploadsValidationMessages() 
    {
        return [
            'file.required' => 'File is required.',
            'extension.in' => 'File is not a Comma Delimited or CSV file extension.'
        ];
    }

    public function index()
    {
        //

        $data = [
            #'files'=>$files,
            #'types'=>$types,
            'title'=>"Upload Agent Break Schedule"
        ];

        return view('push.cpanel.uploads.upload_agent_break_schedules', $data);
    }

    public function store(Request $request)
    {
        //
        $ext = '';

        if ($request->hasFile('file'))
        {
            $ext = strtolower($request->file('file')->getClientOriginalExtension());

            if ($ext != 'csv')
            {
                $ext = 'others';
            }
        }

        $input = [
            'file' => $request->file('file'),
            'extension' => $ext
        ];

        $validator = Validator::make(
            $input,
            $this->fileUploadsValidationRules(),
            $this->fileUploadsValidationMessages()
        );

        if ($validator->fails())
        {
            return redirect() -> route('upload_agent_schedules') -> withInput() -> withErrors($validator);
        }
        else
        {
            $week_ending = substr($request->file('file')->getClientOriginalName(), 0, -4);
            $file_location = '';

            $fileName = $week_ending.'_'.date('Ymd@his').'_'.str_random(15).'.'.$request->file('file')->getClientOriginalExtension();
            $file_location = 'uploads/agent_schedules/';
            //  upload file
            $request->file('file')->move($file_location, $fileName);
            //  Include filename
            $file_location .= ''.$fileName;
            //  Open File
            $csv = fopen($file_location, 'r');
            $flag = TRUE;
            //  
            $week_ending = date('Y-m-d', strtotime($week_ending));

            $monday     = date('Y-m-d', strtotime('-6 day', strtotime($week_ending)));
            $tuesday    = date('Y-m-d', strtotime('-5 day', strtotime($week_ending)));
            $wednesday  = date('Y-m-d', strtotime('-4 day', strtotime($week_ending)));
            $thursday   = date('Y-m-d', strtotime('-3 day', strtotime($week_ending)));
            $friday     = date('Y-m-d', strtotime('-2 day', strtotime($week_ending)));
            $saturday   = date('Y-m-d', strtotime('-1 day', strtotime($week_ending)));

            while (($element = fgetcsv($csv, 1000, ',')) !== FALSE)
            {
                if ($flag)
                {
                    $flag = FALSE;
                    continue;
                }

                if (!empty($element[0]) || $element[0] != '-')
                {
                    $week = [];
                    $employee_number        = trim($element[0]);
                    $avaya                  = trim($element[1]);
                    $name                   = trim($element[2]);
                    /**
                    Monday
                    **/
                    //  Shift
                    $week['monday']['shift']              = $monday;

                    if (!empty($element[3]))
                    {
                        //  Monday Break 1
                        $week['monday']['break_1_start']      = date('H:i', strtotime(trim($element[3])));
                        $week['monday']['break_1_end']        = date('H:i', strtotime(trim($element[4])));
                        //  Monday Break 2
                        $week['monday']['break_2_start']      = date('H:i', strtotime(trim($element[5])));
                        $week['monday']['break_2_end']        = date('H:i', strtotime(trim($element[6])));
                        //  Monday Break 3
                        $week['monday']['break_3_start']      = date('H:i', strtotime(trim($element[7])));
                        $week['monday']['break_3_end']        = date('H:i', strtotime(trim($element[8])));
                    }
                    else
                    {
                        //  Monday Break 1
                        $week['monday']['break_1_start']      = null;
                        $week['monday']['break_1_end']        = null;
                        //  Monday Break 2
                        $week['monday']['break_2_start']      = null;
                        $week['monday']['break_2_end']        = null;
                        //  Monday Break 3
                        $week['monday']['break_3_start']      = null;
                        $week['monday']['break_3_end']        = null;
                    }
                    /**
                    Tuesday
                    **/
                    //  Shift
                    $week['tuesday']['shift']              = $tuesday;

                    if (!empty($element[9]))
                    {
                        //  Tuesday Break 1
                        $week['tuesday']['break_1_start']      = date('H:i', strtotime(trim($element[9])));
                        $week['tuesday']['break_1_end']        = date('H:i', strtotime(trim($element[10])));
                        //  Tuesday Break 2
                        $week['tuesday']['break_2_start']      = date('H:i', strtotime(trim($element[11])));
                        $week['tuesday']['break_2_end']        = date('H:i', strtotime(trim($element[12])));
                        //  Tuesday Break 3
                        $week['tuesday']['break_3_start']      = date('H:i', strtotime(trim($element[13])));
                        $week['tuesday']['break_3_end']        = date('H:i', strtotime(trim($element[14])));
                    }
                    else
                    {
                        //  Tuesday Break 1
                        $week['tuesday']['break_1_start']      = null;
                        $week['tuesday']['break_1_end']        = null;
                        //  Tuesday Break 2
                        $week['tuesday']['break_2_start']      = null;
                        $week['tuesday']['break_2_end']        = null;
                        //  Tuesday Break 3
                        $week['tuesday']['break_3_start']      = null;
                        $week['tuesday']['break_3_end']        = null;
                    }
                    /**
                    Wednesday
                    **/
                    //  Shift
                    $week['wednesday']['shift']                     = $wednesday;

                    if (!empty($element[15]))
                    {
                        //  Wednesday Break 1
                        $week['wednesday']['break_1_start']   = date('H:i', strtotime(trim($element[15])));
                        $week['wednesday']['break_1_end']     = date('H:i', strtotime(trim($element[16])));
                        //  Wednesday Break 2
                        $week['wednesday']['break_2_start']   = date('H:i', strtotime(trim($element[17])));
                        $week['wednesday']['break_2_end']     = date('H:i', strtotime(trim($element[18])));
                        //  Wednesday Break 3
                        $week['wednesday']['break_3_start']   = date('H:i', strtotime(trim($element[19])));
                        $week['wednesday']['break_3_end']     = date('H:i', strtotime(trim($element[20])));
                    }
                    else
                    {
                        //  Wednesday Break 1
                        $week['wednesday']['break_1_start']   = null;
                        $week['wednesday']['break_1_end']     = null;
                        //  Wednesday Break 2
                        $week['wednesday']['break_2_start']   = null;
                        $week['wednesday']['break_2_end']     = null;
                        //  Wednesday Break 3
                        $week['wednesday']['break_3_start']   = null;
                        $week['wednesday']['break_3_end']     = null;
                    }
                    /**
                    Thursday
                    **/
                    //  Shift
                    $week['thursday']['shift']              = $thursday;

                    if (!empty($element[21]))
                    {
                        //  Thursday Break 1
                        $week['thursday']['break_1_start']      = date('H:i', strtotime(trim($element[21])));
                        $week['thursday']['break_1_end']        = date('H:i', strtotime(trim($element[22])));
                        //  Thursday Break 2
                        $week['thursday']['break_2_start']      = date('H:i', strtotime(trim($element[23])));
                        $week['thursday']['break_2_end']        = date('H:i', strtotime(trim($element[24])));
                        //  Thursday Break 3
                        $week['thursday']['break_3_start']      = date('H:i', strtotime(trim($element[25])));
                        $week['thursday']['break_3_end']        = date('H:i', strtotime(trim($element[26])));
                    }
                    else
                    {
                        //  Thursday Break 1
                        $week['thursday']['break_1_start']      = null;
                        $week['thursday']['break_1_end']        = null;
                        //  Thursday Break 2
                        $week['thursday']['break_2_start']      = null;
                        $week['thursday']['break_2_end']        = null;
                        //  Thursday Break 3
                        $week['thursday']['break_3_start']      = null;
                        $week['thursday']['break_3_end']        = null;
                    }
                    /**
                    Friday
                    **/
                    //  Shift
                    $week['friday']['shift']              = $friday;

                    if (!empty($element[27]))
                    {
                        //  Friday Break 1
                        $week['friday']['break_1_start']      = date('H:i', strtotime(trim($element[27])));
                        $week['friday']['break_1_end']        = date('H:i', strtotime(trim($element[28])));
                        //  Friday Break 2
                        $week['friday']['break_2_start']      = date('H:i', strtotime(trim($element[29])));
                        $week['friday']['break_2_end']        = date('H:i', strtotime(trim($element[30])));
                        //  Friday Break 3
                        $week['friday']['break_3_start']      = date('H:i', strtotime(trim($element[31])));
                        $week['friday']['break_3_end']        = date('H:i', strtotime(trim($element[32])));
                    }
                    else
                    {
                        //  Friday Break 1
                        $week['friday']['break_1_start']      = null;
                        $week['friday']['break_1_end']        = null;
                        //  Friday Break 2
                        $week['friday']['break_2_start']      = null;
                        $week['friday']['break_2_end']        = null;
                        //  Friday Break 3
                        $week['friday']['break_3_start']      = null;
                        $week['friday']['break_3_end']        = null;
                    }
                    /**
                    Saturday
                    **/
                    //  Shift
                    $week['saturday']['shift']              = $saturday;

                    if (!empty($element[33]))
                    {
                        //  Saturday Break 1
                        $week['saturday']['break_1_start']      = date('H:i', strtotime(trim($element[33])));
                        $week['saturday']['break_1_end']        = date('H:i', strtotime(trim($element[34])));
                        //  Saturday Break 2
                        $week['saturday']['break_2_start']      = date('H:i', strtotime(trim($element[35])));
                        $week['saturday']['break_2_end']        = date('H:i', strtotime(trim($element[36])));
                        //  Saturday Break 3
                        $week['saturday']['break_3_start']      = date('H:i', strtotime(trim($element[37])));
                        $week['saturday']['break_3_end']        = date('H:i', strtotime(trim($element[38])));
                    }
                    else
                    {
                        //  Saturday Break 1
                        $week['saturday']['break_1_start']      = null;
                        $week['saturday']['break_1_end']        = null;
                        //  Saturday Break 2
                        $week['saturday']['break_2_start']      = null;
                        $week['saturday']['break_2_end']        = null;
                        //  Saturday Break 3
                        $week['saturday']['break_3_start']      = null;
                        $week['saturday']['break_3_end']        = null;
                    }
                    /**
                    Sunday
                    **/
                    //  Shift
                    $week['sunday']['shift']              = $week_ending;

                    if (!empty($element[39]))
                    {
                        //  Sunday Break 1
                        $week['sunday']['break_1_start']      = date('H:i', strtotime(trim($element[39])));
                        $week['sunday']['break_1_end']        = date('H:i', strtotime(trim($element[40])));
                        //  Sunday Break 2
                        $week['sunday']['break_2_start']      = date('H:i', strtotime(trim($element[41])));
                        $week['sunday']['break_2_end']        = date('H:i', strtotime(trim($element[42])));
                        //  Sunday Break 3
                        $week['sunday']['break_3_start']      = date('H:i', strtotime(trim($element[43])));
                        $week['sunday']['break_3_end']        = date('H:i', strtotime(trim($element[44])));
                    }
                    else
                    {
                        //  Sunday Break 1
                        $week['sunday']['break_1_start']      = null;
                        $week['sunday']['break_1_end']        = null;
                        //  Sunday Break 2
                        $week['sunday']['break_2_start']      = null;
                        $week['sunday']['break_2_end']        = null;
                        //  Sunday Break 3
                        $week['sunday']['break_3_start']      = null;
                        $week['sunday']['break_3_end']        = null;
                    }
                    /**
                    Insert Schedule
                    **/
                    #dd($week);
                    foreach ($week as $day) 
                    {
                        $schedule = new AgentBreakSchedule;

                        $schedule->avaya = $avaya;
                        $schedule->employee_number = $employee_number;
                        //  Shift
                        $schedule->shift_date = $day['shift'];
                        $schedule->week_ending = $week_ending;
                        //  Break 1
                        $schedule->break_1_start = $day['break_1_start'];
                        $schedule->break_1_end = $day['break_1_end'];
                        //  Break 2
                        $schedule->break_2_start = $day['break_2_start'];
                        $schedule->break_2_end = $day['break_2_end'];
                        //  Break 3
                        $schedule->break_3_start = $day['break_3_start'];
                        $schedule->break_3_end = $day['break_3_end'];

                        $schedule->save();
                    }
                }
            }

            return redirect() -> route('upload_agent_break_schedules') -> with('fileUploadSuccess', 'Agent Schedule successfully uploaded.');
        }
    }
}
