<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileUploadTypes extends Model
{
    //
     public $table = 'file_upload_type';
     public $timestamps = true;
    
    public function file()
    {
    	return $this->belongsTo('App\FileUploads', 'id', 'file_type');
    }
}
