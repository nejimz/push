<div class="row">
		<!-- Message Layout Start-->
		@if(!is_null($conversation_id))
<?php
			$count = 1;
?>
			@foreach($my_message as $row)
<?php
				$to = '';

				$group = '';

				$recipients = App\ConversationMembers::recipients($conversation_id,$row->ntlogin)
        					->orderBy('group')
        					->get();
				#print_r($recipients);
				foreach ($recipients as $user)
				{
					if(is_null($user->name))
					{
						if($user->group == "")
						{
							$to .= '<u title="'.$user->ntlogin.'">'.$user->ntlogin.'</u>; ';
						}
					}
					elseif($user->group != "")
					{
						$user->group." => ".$user->ntlogin."</br>";
						if($user->group != $group)
						{
							$group = $user->group;
							$groupMembers = App\Distributions::leftJoin('users_distribution', 'users_distribution.distribution_id', '=' ,'distributions.id')
								    				->leftJoin('users', 'users.ntlogin', '=' ,'users_distribution.ntlogin')    				
								    				->where('distributions.distribution', '=', $group)
								    				->select(['users.name'])
								    				->orderBy('name', 'asc')
								    				->get();
							$grpMember = "";
							$grpMemberCnt = 1;
							foreach ($groupMembers as $groupMember) {
								if($groupMember->name != "")
								{
									$grpMember .= (count($groupMembers)==$grpMemberCnt) ? $groupMember->name." " : $groupMember->name.", ";
								}
								$grpMemberCnt++;
							}

							$to .= '<u data-tooltip aria-haspopup="true" data-options="show_on:large" title="'.$grpMember.'">'.$group.'</u>; ';
						}
					}
					else
					{
						$to .= '<u title="'.$user->ntlogin.'">'.$user->name.'</u>; ';
					}
				}
				
				$to = substr($to, 0, -2);
				//echo $to;
				// echo "Created At: ".$row->created_at;
				// echo "Last View: ".$message_last_view->last_view;
				$unread_panel = (strtotime($row->created_at) > strtotime($message_last_view->last_view))? 'unread_msg ' : 'read_msg';
?>
			<div class="panel {{ $unread_panel }} in-panel" >
				<div class="row">
					<div class="large-8 columns">
						<strong>Sent:</strong> <em title="{{ date('D, F d, Y h:i:s', strtotime($row->created_at)) }}">{{ date('D, m/d/Y h:i:s', strtotime($row->created_at)) }}</em>
						<br />
						<strong>From:</strong> 
						@if(is_null($row->name))
							<u>{{ $row->ntlogin }}</u>
						@else
							{{ $row->name }}
						@endif
					</div>
					<div class="large-4 columns">
						@if($count == 1)
							<!-- <a class="" title="Reply" href="{{ route('reply_message', [$row->conversation_id, $conversation_member_id, str_random(40)]) }}"><i class="fa fa-mail-reply"></i> Reply</a>&nbsp;  -->
							<!--a class="" title="Forward" href="#forward"><i class="fa fa-mail-forward"></i> Forward</a-->
						@endif
					</div>
					<div class="large-12 columns">
						<strong>To:</strong> 
						@if($to=="")
							Me
						@else
							{!! $to !!}
						@endif
					</div>
					<div class="large-12 columns">
						<strong>Subject: </strong> {!! $row->subject !!}
					</div>
					<div class="large-12 columns">
						<br />
						{!! $row->message !!}
						<br /><br />
					</div>
				</div>
			</div>
<?php
				$count++;
?>
			@endforeach
		@endif
		<!-- Message Layout End-->
		</div>