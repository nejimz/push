<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentBreakSchedule extends Model
{
    //
     public $table = 'agent_break_schedule';
     public $timestamps = false;

     public function scopeMyCurrentWeekEnding($query, $employee_number, $week_ending)
     {
     	return $query->where('employee_number', $employee_number)->where('week_ending', $week_ending);
     }
     
}
