<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <script src="{{ asset('js/vendor/jquery.js') }}"></script>
        <style>
            @font-face {
              font-family: 'Lato';
              font-style: normal;
              font-weight: 100;
              /*src: local('Lato Hairline'), local('Lato-Hairline'), url(../../fonts/lato-hairline.woff) format('woff');*/
            }

            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Authenticating...</div>
            </div>
        </div>

        <input type="hidden" name="authentication_url" value="{{ route('authentication') }}">
        {!! csrf_field() !!}

        <script type="text/javascript">
        var xhr_authentication = null;
        
        $(function()
        {
            xhr_authentication = $.ajax({
                type : "POST",
                url : $("input[name='authentication_url']").val(),
                data : "_token=" + $("input[name='_token']").val(),
                cache : false,
                dataType : "script",
                beforeSend : function(xhr) {
                    if (xhr_authentication != null) {
                        xhr_authentication.abort();
                    }
                }
            }).done(function(response){
                //  Start Code...
                //  End Code
            });
        });
        </script>
    </body>
</html>
