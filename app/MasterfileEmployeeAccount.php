<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileEmployeeAccount extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'employeeaccount';
    public $timestamps = false;
}
