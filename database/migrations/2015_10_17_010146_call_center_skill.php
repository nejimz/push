<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CallCenterSkill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('call_center_skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->string('skill')->unique();
        });

        DB::table('call_center_skills')->insert( [ 'account_id' => '1', 'skill' => 'ST Univ BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '1', 'skill' => 'ST CRT BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '1', 'skill' => 'ST Sup BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '1', 'skill' => 'ENT VAS Team BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '1', 'skill' => 'ST ERD BAC' ] ); 


        DB::table('call_center_skills')->insert( [ 'account_id' => '2', 'skill' => 'SM Univ BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '2', 'skill' => 'SM Sup BAC' ] ); 


        DB::table('call_center_skills')->insert( [ 'account_id' => '3', 'skill' => 'PP Univ BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '3', 'skill' => 'PP Sup BAC' ] ); 


        DB::table('call_center_skills')->insert( [ 'account_id' => '4', 'skill' => 'Total Wireless Univ BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '4', 'skill' => 'Total Wireless CRT BAC' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '4', 'skill' => 'Total Wireless SUP BAC' ] ); 


        DB::table('call_center_skills')->insert( [ 'account_id' => '5', 'skill' => 'Tracfone Univ' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '5', 'skill' => 'Tracfone CRT' ] ); 


        DB::table('call_center_skills')->insert( [ 'account_id' => '6', 'skill' => 'SafeLink Univ' ] ); 
        DB::table('call_center_skills')->insert( [ 'account_id' => '6', 'skill' => 'SafeLink CRT' ] ); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('call_center_skills');
    }
}
