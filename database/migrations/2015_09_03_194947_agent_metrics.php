<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgentMetrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('metric_staffed_hours', function (Blueprint $table) {
            $table->increments('recordID');
            $table->string('employeeID', 10);
            $table->date('dateofaudit');
            $table->time('staffedhours');
            $table->text('remarks');
            $table->string('loggedby', 45);
            $table->datetime('loggedon');
        });

        Schema::create('metric_hold_time', function (Blueprint $table) {
            $table->increments('recordID');
            $table->string('employeeID', 10);
            $table->date('dateofaudit');
            $table->decimal('holdtime', 10, 2);
            $table->text('remarks');
            $table->string('loggedby', 45);
            $table->datetime('loggedon');
        });

        Schema::create('metric_schedule_adherence', function (Blueprint $table) {
            $table->increments('recordID');
            $table->string('employeeID', 10);
            $table->date('dateofaudit');
            $table->decimal('scheduleadherence', 10, 2);
            $table->text('remarks');
            $table->string('loggedby', 45);
            $table->datetime('loggedon');
        });

        Schema::create('metric_transfer_rate', function (Blueprint $table) {
            $table->increments('recordID');
            $table->string('employeeID', 10);
            $table->integer('tier');
            $table->date('dateofaudit');
            $table->decimal('transferrate', 10, 2);
            $table->text('remarks');
            $table->string('loggedby', 45);
            $table->datetime('loggedon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('metric_staffed_hours');
        Schema::drop('metric_hold_time');
        Schema::drop('metric_schedule_adherence');
        Schema::drop('metric_transfer_rate');
    }
}
