<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MasterfileEmployeeAccount;
use App\MasterfileEmployeeInformation;
use App\MasterfileDepartments;
use \Carbon\Carbon;

class Masterfile extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'employee';
    public $timestamps = false;

    /**
    Some Functions
    **/
    public function getEmployeeNumberAttribute()
    {
		$id 		= $this->empID;
		$employee 	= MasterfileEmployeeAccount::where('empID', $id)->first();

     	return $employee->empNumber;
    }

    public function getDepartmentNameAttribute()
    {
		$id 	= $this->empID;
		$info 	= MasterfileEmployeeInformation::where('empID', $id)->first();

		if($info->department == 0)
		{
			return '';
		}

		$row 	= MasterfileDepartments::where('depID', $info->department)->first();
	    
	    return $row->department;
    }

    public function getEmployeeFullNameAttribute()
    {
        return $this->firstName . ' ' . $this->middleName . ' ' . $this->lastName . ' ' . $this->suffix;
    }

    public function getBirthDateFormatAttribute()
    {
        return date('F d, Y', strtotime($this->birthDate));
    }

    public function getGenderFormatAttribute()
    {
        return ($this->gender == 0)? 'Male' : 'Female';
    }

    public function getCivilStatusFormatAttribute()
    {
        $value = $this->civil_status;
        if($value == 1)
        {
            return 'Single';
        }
        elseif($value == 2)
        {
            return 'Married';
        }
        elseif($value == 3)
        {
            return 'Widowed';
        }
        elseif($value == 4)
        {
            return 'Divorced';
        }
    }
}
