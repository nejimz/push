<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distributions extends Model
{
    //
     public $table = 'distributions';
     public $timestamps = false;

    public function users()
    {
    	return $this->hasMany('App\UsersDistribution', 'distribution_id', 'id');
    }

    public function scopeDistributionInformation($query, $id){
    	return $query->where('id', '=', $id)
    				->select(['id','distribution'])
    				->first();
    }

    public function scopeDistributionMembers($query, $id){
    	return $query->leftJoin('users_distribution', 'users_distribution.distribution_id', '=' ,'distributions.id')
    				->leftJoin('users', 'users.ntlogin', '=' ,'users_distribution.ntlogin')    				
    				->where('distributions.id', '=', $id)
    				->select(['users_distribution.ntlogin', 'users.name', 'users.employee_number'])
    				->orderBy('name', 'asc')
    				->get();
    }
}