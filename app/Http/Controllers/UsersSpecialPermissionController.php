<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UsersAdmin;
use App\UsersSpecialPermission;
use App\Ous;

class UsersSpecialPermissionController extends Controller
{
    /**
    Authenticate Admin Account
    **/
    public function __construct()
    {
        //  Only Allowed OU's can use the messaging 
        if(UsersAdmin::where('ntlogin', Auth::user()->ntlogin)->count() == 0)
        {
            abort(404);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $admins = UsersSpecialPermission::leftJoin('users', 'users_special_permissions.ntlogin', '=', 'users.ntlogin')
                    ->whereNotNull('users.id')
                    ->orderBy('name', 'ASC')
                    ->paginate(50);

        $data = compact('admins');
        
        return view('push.cpanel.users.special.index', $data);
    }

    public function add()
    {
        //
        $types = ['nurse'];
        $data = compact('types');
        return view('push.cpanel.users.special.add', $data);
    }

    public function store(Request $request)
    {
        $rules = [
                'ntlogin'=>'required|exists:users,ntlogin|exists:users,ntlogin',
                'type'=>'required'
            ];
        $message = [
                'ntlogin.required'  => 'Ntlogin is required.',
                'ntlogin.exists'    => 'Ntlogin not exists.',
                'type.required'     => 'Type is required.'
            ];

        $this->validate($request, $rules, $message);
        //
        #dd($request->all());
        $user = new UsersSpecialPermission;
        $user->ntlogin = trim($request->input('ntlogin'));
        $user->type    = trim($request->input('type'));
        $user->save();

        return redirect()->route('users_special_add')->with('success', 'Ntlogin successfully added!');
    }

    public function destroy($ntlogin)
    {
        //
        UsersSpecialPermission::where('ntlogin', $ntlogin)->delete();

        return redirect()->route('users_special')->with('success', 'Ntlogin successfully deleted!');
    }
}
