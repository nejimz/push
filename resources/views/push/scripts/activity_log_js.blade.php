<script type="text/javascript">
	/**
		Activity Log
	**/

	$(function(){
		$('#startTime').on('click', function(e){
        	if($(this).hasClass("success")){
        		$(this).removeClass("success");
	        	$(this).addClass("secondary");
	        	$('#stopTime').removeClass("secondary");
	        	$('#stopTime').addClass("alert");
        		Logger('start');
        	}
		});

		$('#stopTime').on('click', function(e){
        	if($(this).hasClass("alert")){
        		$(this).removeClass("alert");
	        	$(this).addClass("secondary");
	        	$('#startTime').removeClass("secondary");
	        	$('#startTime').addClass("success");	
        		Logger('stop');
        	}
		});

		function Logger(logAction){
			console.log('I log as: '+logAction);
		}
	});

</script>