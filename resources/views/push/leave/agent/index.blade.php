@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

  <div class="row">
    <div class="large-12 columns">
      <h4><i class="fa fa-sign-in"></i> My Leave</h4>
    </div>
  </div><!-- row -->

  <div class="row">
    <div class="large-12 columns">
      <a href="{{ route('leave_agent_create') }}" class="button"><i class="fa fa-plus"></i> File a Leave</a>
    </div>
  </div><!-- row -->

  <div class="row">
    <div class="large-12 columns">
      {!! str_replace('/?', '?', $leaves->render()) !!}
      <table width="100%">
        <thead>
          <tr>
            <th width="25%">Leave Type</th>
            <th width="25%">Leave Date</th>
            <th width="50%">Reason</th>
          </tr>
        </thead>
        <tbody>
          @foreach($leaves as $row)
          <tr>
            <td>{{ $row->leave_type }}</td>
            <td>{{ $row->leave_date_format }} <i>EST</i></td>
            <td>{{ $row->reason }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {!! str_replace('/?', '?', $leaves->render()) !!}
    </div>
  </div><!-- row -->

<!-- Stops -->
@stop