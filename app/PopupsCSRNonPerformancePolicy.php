<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupsCSRNonPerformancePolicy extends Model
{
    //
     public $table = 'popups_csr_non_performance_policy';
     public $timestamps = false;
}
