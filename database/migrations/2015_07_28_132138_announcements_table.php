<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->string('ntlogin', 100)->index();
            $table->tinyInteger('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('announcements');
    }
}
