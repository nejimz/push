<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Auth;
use App\AgentBreakSchedule;
use App\Roster;
use App\Users;

class ProfileController extends Controller
{
    public function last_week_ending($employee_number)
    {
        $schedule = AgentBreakSchedule::where('employee_number', $employee_number)->orderBy('week_ending', 'DESC')->first();
        return $schedule->week_ending;
    }

    public function index()
    {
        $profile_pix = Auth::user()->image;
        $employee_number = Auth::user()->employee_number;
        $check_employee_number = Users::where('employee_number', $employee_number)->count();

        $employee = DB::connection('mysql2')
                ->table('employee')
                ->select([
                    'firstName','middleName','lastName','suffix','birthDate','gender',
                    'civil_status_name','contactNumber','contactNumber2','telNumber',
                    'oldAddress','street','subd','sitio','brgy','city','province',
                    'empNumber','waveNumber','branchName','empDate','TaxStatusName',
                    'departments.department','departments.position',
                    'tin','sss','philhealth','hdmf'
                ])
                ->leftJoin('employeeaccount', 'employeeaccount.empID', '=', 'employee.empID')
                ->leftJoin('employeeaddress', 'employeeaddress.empID', '=', 'employee.empID')
                ->leftJoin('employmentinfo', 'employmentinfo.empID', '=', 'employee.empID')
                ->leftJoin('employeecredentials', 'employeecredentials.empID', '=', 'employee.empID')
                ->leftJoin('employeepaydetails', 'employeepaydetails.empID', '=', 'employee.empID')

                ->leftJoin('civil_status', 'civil_status.civil_status_id', '=', 'employeepaydetails.civilStatus')
                ->leftJoin('taxstatus', 'taxstatus.TaxStatusID', '=', 'employeepaydetails.taxStatus')
                ->leftJoin('departments', 'departments.idDep', '=', 'employmentinfo.position')
                ->leftJoin('branch', 'branch.branchID', '=', 'employmentinfo.branchID')
                ->where('empNumber', $employee_number)
                ->first();

        $roster = Roster::select('organization','avaya','hire_date','work_pattern','tier','week_ending',
                    'supervisor_ID','ac_ID',
                    DB::raw('(SELECT name FROM users WHERE employee_number=supervisor_ID) AS supervisor'),
                    DB::raw('(SELECT name FROM users WHERE employee_number=ac_ID) AS ac')
                    )
                    ->where('employee_ID', $employee_number)
                    ->orderBy('week_ending','DESC')
                    ->first();

        if(is_null($employee))
        {
           abort(404);
        }

        $data = compact('roster', 'employee', 'profile_pix', 'check_employee_number');

        return view('push.profile', $data);
    }
}
