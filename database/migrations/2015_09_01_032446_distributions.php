<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Distributions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('distribution')->unique();
        });

        DB::table('distributions')->insert( [ 'distribution' => 'ITSystems' ] );
        DB::table('distributions')->insert( [ 'distribution' => 'ITProgrammers' ] );
        DB::table('distributions')->insert( [ 'distribution' => 'NetworkTeam' ] );
        DB::table('distributions')->insert( [ 'distribution' => 'SystemAdministrators' ] );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('distributions');
    }
}
