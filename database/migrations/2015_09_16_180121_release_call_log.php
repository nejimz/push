<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleaseCallLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('release_call_log', function (Blueprint $table) {
            $table->increments('log_ID');
            $table->string('employee_ID', 10);
            $table->integer('reason_ID')->references('reason_ID')->on('release_call_valid_reasons');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('release_call_log');
    }
}
