<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_admin', function (Blueprint $table) {
            $table->string('ntlogin')->unique();
        });

        DB::table('users_admin')->insert( [ 'ntlogin' => 'jaasong' ] );
        DB::table('users_admin')->insert( [ 'ntlogin' => 'dgquijote' ] );
        DB::table('users_admin')->insert( [ 'ntlogin' => 'mmsapalo' ] );
        DB::table('users_admin')->insert( [ 'ntlogin' => 'nbzaragoza' ] );
        DB::table('users_admin')->insert( [ 'ntlogin' => 'rtsevilla' ] );
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users_admin');
    }
}
