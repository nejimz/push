<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TransferLogStore extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transactionType' => 'numeric|min:1',
            'callsreceived' => 'numeric|min:1',
            'notesTransferLog' => 'required'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
            'transactionType.min' => 'Select a valid Transaction Type.',
            'callsreceived.min' => 'Select a valid Call Received.',
            'notesTransferLog.required' => 'Please input Notes.'
        ];
    }
}
