<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounts extends Model
{
    //
     public $table = 'call_center_accounts';
     public $timestamps = false;

     public function skills()
     {
    	return $this->belongsTo('App\Skills', 'account_id', 'id');
     }
}
