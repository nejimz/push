<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersSpecialPermission extends Model
{
    //
    public $table = 'users_special_permissions';
    public $timestamps = false;
}
