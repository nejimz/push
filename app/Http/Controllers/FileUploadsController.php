<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\FileUploads;
use App\FileUploadTypes;
use Validator;

class FileUploadsController extends Controller
{

    public function fileUploadsValidationRules() 
    {
        return [
            'file_type' => 'required',
            'file_name' => 'required|max:254|unique:file_uploads,file_name',
            'file'      => 'required|mimes:pdf'
        ];
    }

    public function fileUploadsValidationMessages() 
    {
        return [

            'file_type.required'    => 'Type is required.',

            'file_name.required'    => 'File Name is required.',
            'file.required'         => 'File Name must be 255 characters only.',
            'file.exists'           => 'File Name Already exists.',

            'file.required'         => 'File is required.',
            'file.mimes'            => 'File is not a PDF file..'
        ];
    }

    public function index()
    {
        //
        $files = FileUploads::orderBy('file_name', 'DESC')->paginate(10); 
        $types = FileUploadTypes::orderBy('name', 'ASC')->get(); 

        $data = [
            'files'=>$files,
            'types'=>$types,
            'title'=>"File Uploads"
        ];

        return view('push.cpanel.uploads.file_uploads', $data);
    }

    public function store(Request $request)
    {
        //
        $validator = Validator::make(
            $request->all(),
            $this->fileUploadsValidationRules(),
            $this->fileUploadsValidationMessages()
        );

        if ($validator->fails())
        {
            return redirect() -> route('file_uploads') -> withInput() -> withErrors($validator);
        }
        else
        {
            $file_location = '';

            if ($request->hasFile('file'))
            {
                $fileName = date('Ymd@his').'_'.str_random(15).'.'.$request->file('file')->getClientOriginalExtension();
                $file_location = 'uploads/files/'.$request->file_type;
                //  upload file
                $request->file('file')->move($file_location, $fileName);
                //  Include filename
                $file_location .= '/'.$fileName;
            }

            $file_uploads = new FileUploads;
            $file_uploads->file_type = $request->file_type;
            $file_uploads->file_name = $request->file_name;
            $file_uploads->file_location = $file_location;
            $file_uploads->save();

            return redirect() -> route('file_uploads') -> with('fileUploadSuccess', 'File successfully save.');
        }
    }

    public function delete($id)
    {
        //
        $file = FileUploads::find($id);
        $file->delete();

        return redirect() -> route('file_uploads') -> with('fileUploadSuccessDelete', 'File successfully deleted.');
    }

    public function avaya_one_x_guide()
    {
        //
        $files = FileUploads::where('file_type', 1)->orderBy('created_at', 'DESC')->get();

        $data = [
            'files'=>$files,
            'title'=>"Avaya ONE-X Guide"
        ];

        return view('push.avaya_one_x_guide', $data);
    }

    public function paid_leaves()
    {
        //
        $files = FileUploads::where('file_type', 2)->orderBy('created_at', 'DESC')->get();

        $data = [
            'files'=>$files,
            'title'=>"Paid Leaves"
        ];

        return view('push.paid_leaves', $data);
    }

    public function it_security_policy()
    {
        //
        $files = FileUploads::where('file_type', 3)->orderBy('created_at', 'DESC')->get();

        $data = [
            'files'=>$files,
            'title'=>"IT Security Policy"
        ];

        return view('push.it_security_policy', $data);
    }

    public function qa_newsletter()
    {
        //
        $files = FileUploads::where('file_type', 4)->orderBy('created_at', 'DESC')->get();

        $data = [
            'files'=>$files,
            'title'=>"QA Newsletter"
        ];

        return view('push.qa_newsletter', $data);
    }
}
