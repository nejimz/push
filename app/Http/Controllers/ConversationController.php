<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Ldap;

use App\Users;
use App\Conversations;
use App\ConversationMembers;
use App\ConversationMessages;
use App\OusConversation;
use App\UsersConversation;
use App\UsersDistribution;

use Auth;
use DB;
use Validator;

class ConversationController extends Controller
{
    /**
    Filter
    **/
    public function __construct()
    {
        if(!Auth::check())
        {
            return 0;
        }
        //  Only Allowed OU's can use the messaging 
        if(OusConversation::where('ou', Auth::user()->ou)->count() == 0)
        {
            //  Only allowed NTLogins can use the Messaging
            if(UsersConversation::where('ntlogin',Auth::user()->ntlogin)->count() == 0)
            {
                abort(404);
            }
        }
    }

    /**
    Inbox
    **/
    public function index(Request $request, $conversation_id = null,$conversation_member_id = null,$no_cache = null)
    {
        $data = [
            'conversation_id' => $conversation_id,
            'conversation_member_id' => $conversation_member_id
        ];

        $ntlogin = Auth::user()->ntlogin;

        if (!is_null($conversation_id) && !is_null($conversation_member_id))
        {
            #First Click
            $data['message_last_view'] = ConversationMembers::messageLastView($conversation_member_id)->first();
            ConversationMembers::messageItemClick($conversation_member_id);
            $my_message = ConversationMessages::myMessages($conversation_id)->get();            
            $data['my_message'] = $my_message;
        }


        if($request->has('search'))
        {
            $data['search'] = trim($request->search);
            if($request->has('sort'))
            {
                $my_inbox = Conversations::myInbox($ntlogin, $data['search'])
                            ->having( DB::raw("CASE WHEN conversation_members.last_view > MAX(conversation_messages.created_at) THEN 1 ELSE 0 END"),'=',0)
                            ->simplePaginate(10);
            }
            else
            {
                $my_inbox = Conversations::myInbox($ntlogin, $data['search'])->simplePaginate(10);
            }
        }
        else
        {
            if($request->has('sort'))
            {
                $my_inbox = Conversations::myInbox($ntlogin)
                            ->having( DB::raw("CASE WHEN conversation_members.last_view > MAX(conversation_messages.created_at) THEN 1 ELSE 0 END"),'=',0)
                            ->simplePaginate(10);
            }
            else
            {
                $my_inbox = Conversations::myInbox($ntlogin)->simplePaginate(10);
            }
        }

        if($request->has('page'))
        {
             $data['page'] = $request->page;
        }

        $data['my_inbox'] = $my_inbox;
       
        //dd($data);

        return view('push.messaging.inbox', $data);
    }

    /**
    Inbox
    **/
    public function messages(Request $request, $conversation_id = null,$conversation_member_id = null,$no_cache = null)
    {

        $data = [
            'conversation_id' => $conversation_id,
            'conversation_member_id' => $conversation_member_id
        ];

        $ntlogin = Auth::user()->ntlogin;

        if (!is_null($conversation_id) && !is_null($conversation_member_id))
        {
            #First Click
            $data['message_last_view'] = ConversationMembers::messageLastView($conversation_member_id)->first();
            ConversationMembers::messageItemClick($conversation_member_id);
            $my_message = ConversationMessages::myMessages($conversation_id)->get();            
            $data['my_message'] = $my_message;
        }

        return view('push.messaging.messages', $data);
    }

    /**
    Compose Message
    **/
    public function compose()
    {
        //
        return view('push.messaging.compose');
    }

    /**
    Send Message
    **/
    public function composeValidationRules() 
    {
        return [
            'message-to'        => 'required',
            'message-subject'   => 'required',
            'message-body'      => 'required'
        ];
    }

    public function composeValidationMessages() 
    {
        return [
            'message-to'        => 'Kindly add a contact you want to send a message to',
            'message-subject'   => 'Kindly add a subject.',
            'message-body'      => 'Kindly add a message.'
        ];
    }

    public function send(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            $this->composeValidationRules(),
            $this->composeValidationMessages()
        );

        if ($validator->fails())
        {
            return redirect()->route('compose_message')->withErrors($validator)->withInput();
        }
        else
        {

            $to = explode(",", $request->input('message-to'));
            $Users[] = "";
            $Groups[] ="";

            # Separate Users From Groups
            foreach ($to as $key => $value) 
            {
                $toSort = explode(";",$value);

                if($toSort[1]=="User")
                {
                    $Users[] = $toSort[0];
                }
                if($toSort[1]=="Group")
                {
                    $Groups[] = $toSort[0];
                }
            }

            # Converstaion Subject
            $subject = $request->input('message-subject');

            # Conversation Message body
            $body = $request->input('message-body');

            # Conversation sender
            $sender = Auth::user()->ntlogin;

            # Save subject to Conversation::DB
            $conversation = new Conversations;
            $conversation->subject  = $subject;
            $conversation->save();

            $sender_is_in_group = '';

            if(count($Groups)>1)
            {
                # Conversation group extraction
                foreach ($Groups as $grpKey => $grpValue) 
                {
                    if($grpValue != "")
                    {
                        $userList = UsersDistribution::select('ntlogin')
                                            ->leftJoin('distributions', 'id','=', 'distribution_id')
                                            ->where('distributions.distribution', '=', $grpValue)->get();  
                        
                        # Save users in a group to ConversationMembers:DB
                        foreach ($userList as $person) 
                        {
                            if(!in_array($person->ntlogin, $Users) && $person->ntlogin != $sender && !empty($person->ntlogin))
                            {
                                $member = new ConversationMembers;
                                $member->conversation_id = $conversation->id;
                                $member->ntlogin = $person->ntlogin;
                                $member->group = $grpValue;
                                $member->save();
                            }

                            if($person->ntlogin == $sender)
                            {
                                $sender_is_in_group = $grpValue;
                            }
                        }
                    }
                }   
            }

            if (!in_array($sender, $Users))
            {
                $member = new ConversationMembers;
                $member->conversation_id = $conversation->id;
                $member->ntlogin = Auth::user()->ntlogin;
                $member->group = ($sender_is_in_group == '' ? '' : $sender_is_in_group);
                $member->last_view = date('Y-m-d H:i:s.000',time()+1);
                $member->save();
            }

            # Save users to ConversationMembers:DB
            foreach ($Users as $key => $value) 
            {
                if($value != "")
                {
                    $member = new ConversationMembers;
                    $member->conversation_id = $conversation->id;
                    $member->ntlogin = $value;
                    $member->group = '';
                    $member->last_view = ($value == $sender ? date('Y-m-d H:i:s.000',time()+1) : null ); 
                    $member->save();
                }
            }

            $message = new ConversationMessages;
            $message->conversation_id = $conversation->id;
            $message->ntlogin = $sender;
            $message->message = $body;
            $message->created_at = date("Y-m-d H:i:s.000");
            $message->save();                       

            return redirect() -> route('inbox') -> with('messageSuccess', $request);
        }
    }

    /**
    Reply
    **/
    public function reply($conversation_id = null,$conversation_member_id = null,$no_cache = null)
    {
        //
        $ntlogin = Auth::user()->ntlogin;

        if (ConversationMembers::checkRecipients($conversation_id,$ntlogin)->count() == 0)
        {
            return redirect() -> route('inbox');
        }

        $recipients = ConversationMembers::recipients($conversation_id,$ntlogin)->get();
        $lastMessage = ConversationMessages::myMessages($conversation_id)->first();
        $my_message = ConversationMessages::myMessages($conversation_id)->get();
        #dd($recipients);
        $data = [
            'conversation_id'=>$conversation_id,
            'conversation_member_id'=>$conversation_member_id,
            'recipients'=>$recipients,
            'lastMessage'=>$lastMessage,
            'my_message'=>$my_message
        ];

        return view('push.messaging.reply', $data);
    }

    /**
    Send Reply
    **/
    public function send_reply(Request $request)
    {
        # dd($request->all());   exit;
        #echo $request->conversation_member_id;  exit;
        $Groups = [];
        $Users = [];
        $validator = Validator::make(
            $request->all(),
            ['message_body' => 'required'],
            ['message_body.required' => 'Kindly add a message.']
        );

       # $to = explode(",", $request->input('message-to'));

        if ($validator->fails())
        {
            return redirect()->route('reply_message',[$request->conversation_id, $request->conversation_member_id, str_random(40)])->withErrors($validator);
        }
        else
        {   

            $to = explode(",", $request->input('message_reply_to'));

            if($to[0] != '')
            {
                foreach ($to as $key => $value) 
                {
                    $toSort = explode(";",$value);

                    if($toSort[1]=="User")
                    {
                        $Users[] = $toSort[0];
                    }
                    if($toSort[1]=="Group")
                    {
                        $Groups[] = $toSort[0];
                    }
                }
            }

            ConversationMembers::where('deleted_at', '=', 1)
                                ->where('conversation_id', '=', $request->conversation_id)
                                ->update(['deleted_at'=>0]);
                                
            ConversationMembers::messageItemClick($request->conversation_member_id);

            $userDBList = ConversationMembers::select('ntlogin')->where('conversation_id',$request->conversation_id)->get();

            $message = new ConversationMessages;
            $message->conversation_id = $request->conversation_id;
            $message->ntlogin = Auth::user()->ntlogin;
            $message->message = $request->message_body;
            $message->created_at = date("Y-m-d H:i:s.000");
            $message->save();

            if($to[0] != '')
            {
                #For Groups
                if(count($Groups)>0)
                {
                    # Conversation group extraction
                    foreach ($Groups as $grpKey => $grpValue) 
                    {
                        $userList = UsersDistribution::select('ntlogin')
                                        ->leftJoin('distributions', 'id','=', 'distribution_id')
                                        ->where('distributions.distribution', '=', $grpValue)->get();                     
                        
                        # Save users in a group to ConversationMembers:DB
                        foreach ($userList as $person) 
                        {
                            # Check for valid users
                            if(!in_array($person->ntlogin, $Users) && !$userDBList->contains('ntlogin',$person->ntlogin) && $person->ntlogin != Auth::user()->ntlogin && !empty($person->ntlogin))
                            {
                                $member = new ConversationMembers;
                                $member->conversation_id = $request->conversation_id;
                                $member->ntlogin = $person->ntlogin;
                                $member->group = $grpValue;
                                $member->save();
                            }
                        }
                    }   
                }

                #For Users
                foreach ($Users as $key => $value) {
                    if($userDBList->contains('ntlogin',$value))
                    {
                        ConversationMembers::where('conversation_id', $request->conversation_id)
                                ->where('ntlogin', $value)
                                ->update(['group'=>'']);
                    }
                    else
                    {
                        $added_recepients = new ConversationMembers;
                        $added_recepients->conversation_id = $request->conversation_id;
                        $added_recepients->ntlogin = $value;
                        $added_recepients->last_view = date('Y-m-d H:i:s.000',time()+1);
                        $added_recepients->group = '';
                        $added_recepients->save();
                    }
                }  
            }
            return redirect()->route('inbox');
        }
    }

    /**
    Search LDAP
    **/
    public function searchldap(Request $request)
    {
        $array = Ldap::search($request['q']);
        echo json_encode($array);
    }

    /**
    Message Notification
    **/
    public function notification(Request $request)
    {
        if($request->ajax())
        {
            $ntlogin = Auth::user()->ntlogin;
            $my_inbox = Conversations::myInbox($ntlogin)->get();
            $message_count = 0;
            ///exit;
            foreach($my_inbox as $row)
            {
                if($row->last_viewed == 0)
                {
                    $message_count++;
                }
            }

            if ($message_count >= 1)
            {
                return response()->json(['message_count' => $message_count]);
            }
            else
            {
                return response()->json(['message_count' => 0]);
            }
            #$count = Conversation::where('ntlogin');
        }
    }

    /**
    Delete Message
    **/
    public function delete(Request $request)
    {
        ConversationMembers::messageDelete($request->id);
        // $data['conversation_id'] = 'null';
        // $data['conversation_member_id'] = 'null';
        // $data['_'] = 'null';

        // if($request->has('search'))
        // {
        //     if($request->search != '')
        //     {
        //         $data['search'] = $request->search;
        //     }
        // }

        // if($request->has('page'))
        // {
        //     $data['page'] = $request->page;
        // }
        // $url = str_replace('/null/null/null','',route('inbox', $data));
        // return redirect()->to($url);
    }
}
