
        <br /> <!-- Agent Straight Talk -->
        <ul id="pushMainNavLeft" class="side-nav">
          <li>
            <a href="{{ route('cpanel_index') }}">Control Panel</a>
          </li>
          <li>
            <a href="{{ route('masterfile_index') }}">Masterfile</a>
          </li>
          <li class="{{ \App\Helpers\Menu::activeMenu(['ezwatch']) }}">
          <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropTimekeeping" aria-controls="dropTimekeeping" aria-expanded="false" href="#">Timekeeping »</a>
            <ul id="dropTimekeeping" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="{{ route('ezwatch') }}">EzWatch</a></li>
            </ul>
          </li>
          <li class="{{ \App\Helpers\Menu::activeMenu(['release_call_log', 'reports_CMS', 'reports_supervisor']) }}">
            <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropReleaseCall" aria-controls="dropReleaseCall" aria-expanded="false" href="#">Release Call »</a>
            <ul id="dropReleaseCall" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="{{ route('release_call_log') }}">Release Call Log</a></li>
              <li><a href="{{ route('reports_CMS') }}">CMS Reports</a></li>
              <li><a href="{{ route('reports_supervisor') }}">Supervisor Reports</a></li>
            </ul>
          </li>
          
          <li class="{{ \App\Helpers\Menu::activeMenu(['transfer_log', 'transfer_log_report']) }}">
            <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropTransferLog" aria-controls="dropTransferLog" aria-expanded="false" href="#">Transfer Log »</a>
            <ul id="dropTransferLog" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="{{ route('transfer_log') }}">Logger</a></li>
              <li><a href="{{ route('transfer_log_report') }}">Transfer Log Report</a></li>
            </ul>
          </li>

          <li class="{{ \App\Helpers\Menu::activeMenu(['supervisor_logger']) }}">
            <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropActivityLogger" aria-controls="dropActivityLogger" aria-expanded="false" href="#">Activity Log »</a>
            <ul id="dropActivityLogger" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="{{ route('supervisor_logger') }}">Supervisor</a></li>
            </ul>
          </li>

          <li><a href="#">Case Notation</a></li>
          <li><a href="#">Schedule</a></li>
          <li class="{{ \App\Helpers\Menu::activeMenu(['metrics']) }}">
            <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropScorecard" aria-controls="dropScorecard" aria-expanded="false" href="#">Scorecard »</a>
            <ul id="dropScorecard" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li ><a href="{{ route('metrics', ['id' => 'Scorecard']) }}">Scorecard</a></li>
              <li ><a href="{{ route('metrics', ['id' => 'Attendance']) }}">Attendance</a></li>
              <!--li ><a href="{{ route('metrics', ['id' => 'QA']) }}">QA</a></li>
              <li><a href="{{ route('metrics', ['id' => 'Staff Hours']) }}">Staff Hours</a></li>
              <li><a href="{{ route('metrics', ['id' => 'Hold Time']) }}">Hold Time</a></li>
              <li><a href="{{ route('metrics', ['id' => 'Transfer Rate']) }}">Transfer Rate</a></li>
              <li><a href="{{ route('metrics', ['id' => 'Schedule Adherence']) }}">Schedule Adherence</a></li-->
            </ul>
          </li>
          <li><a href="{{ route('leave_agent_index') }}">Leave</a></li>
          <li><a href="#">DTR</a></li>
          <li class="{{ \App\Helpers\Menu::activeMenu(['seatplan_control']) }}">
            <a data-options="is_hover:true; hover_timeout:1000; align:right;" data-dropdown="dropSeatPlan" aria-controls="dropSeatPlan" aria-expanded="false" href="#">Seat Plan »</a>
            <ul id="dropSeatPlan" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li class=""><a href="{{ route('seatplan_control') }}">Control</a></li>
              <li><a href="#">My Business Unit</a></li>
              <li><a href="#">My Team</a></li>
            </ul>
          </li>
          <li>
            <a data-options="is_hover:true; hover_timeout:1000; align:right;" data-dropdown="dropRequest" aria-controls="dropRequest" aria-expanded="false" href="#">Request »</a>
            <ul id="dropRequest" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="#">Leave</a></li>
              <li><a href="#">Overtime</a></li>
            </ul>
          </li>
        </ul>
<!--script type="text/javascript">
  // To Store
  $(function() {
    //$.session.set("myVar1", "value");
     $.session.set("myVar3", [1, 2, 3]);
  });
  // To Read
  $(function() {
      //console.log($.session.get("myVar3")[2]);
  });
</script-->