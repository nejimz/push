<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Users;

use Auth;
use DB;
use Validator;
use App\PayslipPasswordReset;
use App\UsersSpecialPermission;

class PayslipPasswordResetController extends Controller
{
    /**
    Authenticate Admin Account
    **/
    public function __construct()
    {
        //  Only Allowed OU's can use the messaging 
        if(UsersSpecialPermission::where('ntlogin', Auth::user()->ntlogin)->count() != 0)
        {
            abort(404);
        }
    }

    public function index(Request $request)
    {
        return view('push.payslip-password-reset.index');
    }

    public function generate(Request $request)
    {

        $payslip_info = PayslipPasswordReset::payslipInfo(Auth::user()->employee_number)->first();

        $payslip_pass = str_random(8);

        if(!isset($payslip_info->username))
        {
            $payslip_info = DB::connection('payslip')
                                ->table('users')
                                ->insert([
                                        'name' => Auth::user()->name,
                                        'username' => Auth::user()->ntlogin,
                                        'password' => md5('default'),
                                        'active' => 1,
                                        'access' => 1,
                                        'cat' => 1,
                                        'pass' => 0,
                                        'passtrans' => 0,
                                        'passreset' => 1,
                                        'empno' => Auth::user()->employee_number
                                    ]);
            $payslip_info = PayslipPasswordReset::payslipInfo(Auth::user()->employee_number)->first();
        }

        $update_payslip = PayslipPasswordReset::updatePayslip($payslip_info->userid);   

        if($payslip_info->username != Auth::user()->ntlogin)
        {
            $update_payslip->update(['username'=> Auth::user()->ntlogin, 'password'=>md5($payslip_pass), 'passreset'=>1]);
        }
        else
        {
            $update_payslip->update(['password'=>md5($payslip_pass), 'passreset'=>1]);
        }

        $data = [
            'payslip_info'  => $payslip_info,
            'payslip_pass'  => $payslip_pass,
        ];

        return view('push.payslip-password-reset.index', $data);
    }

}
