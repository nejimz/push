<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Carbon;

use App\PopupsInformationSecurityPolicy;
use App\PopupsAttendancePolicy;
use App\PopupsRefreshPolicy;
use App\PopupsCSRNonPerformancePolicy;
use App\PopupsCodeOfConduct;
use App\PopupsZeroTolerancePolicy;
use App\AgentBreakSchedule;

use Session;
use Auth;

class PopupController extends Controller
{
    /**
    Information Security Policy
    **/
    public function information_security(Request $request)
    {
        //
        $ntlogin = Auth::user()->ntlogin;

        $check_ntlogin = PopupsInformationSecurityPolicy::where('ntlogin', $ntlogin)->count();

        if($check_ntlogin == 0)
        {
            //  Insert Records
            $data = new PopupsInformationSecurityPolicy;
            $data->ntlogin = $ntlogin;
            $data->created_at = date('Y-m-d H:i:s.000');
            $data->save();
            //  Remove from session
            $request->session()->forget('user.popup.information_security');
            #Session::forget('information_security');
            //  Redirect
            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    Attendance Policy
    **/
    public function attendance_policy(Request $request)
    {
        //
        $ntlogin = Auth::user()->ntlogin;

        $check_ntlogin = PopupsAttendancePolicy::where('ntlogin', $ntlogin)->count();

        if($check_ntlogin == 0)
        {
            //  Insert Records
            $data = new PopupsAttendancePolicy;
            $data->ntlogin = $ntlogin;
            $data->created_at = date('Y-m-d H:i:s.000');
            $data->save();
            //  Remove from session
            $request->session()->forget('user.popup.attendance_policy');

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    Refresh Policy
    **/
    public function refresh_policy(Request $request)
    {
        //
        $ntlogin = Auth::user()->ntlogin;

        $check_ntlogin = PopupsRefreshPolicy::where('ntlogin', $ntlogin)->count();

        if($check_ntlogin == 0)
        {
            //  Insert Records
            $data = new PopupsRefreshPolicy;
            $data->ntlogin = $ntlogin;
            $data->created_at = date('Y-m-d H:i:s.000');
            $data->save();
            //  Remove from session
            $request->session()->forget('user.popup.refresh_policy');

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    CSR Non Performance Policy
    **/
    public function csr_non_performance_policy(Request $request)
    {
        //
        $ntlogin = Auth::user()->ntlogin;

        $check_ntlogin = PopupsCSRNonPerformancePolicy::where('ntlogin', $ntlogin)->count();

        if($check_ntlogin == 0)
        {
            //  Insert Records
            $data = new PopupsCSRNonPerformancePolicy;
            $data->ntlogin = $ntlogin;
            $data->created_at = date('Y-m-d H:i:s.000');
            $data->save();
            //  Remove from session
            $request->session()->forget('user.popup.csr_non_performance_policy');

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    Code of Conduct
    **/
    public function code_of_conduct(Request $request)
    {
        //
        $ntlogin = Auth::user()->ntlogin;

        $check_ntlogin = PopupsCodeOfConduct::where('ntlogin', $ntlogin)->count();

        if($check_ntlogin == 0)
        {
            //  Insert Records
            $data = new PopupsCodeOfConduct;
            $data->ntlogin = $ntlogin;
            $data->created_at = date('Y-m-d H:i:s.000');
            $data->save();
            //  Remove from session
            $request->session()->forget('user.popup.code_of_conduct');

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    Zero Tolerance Policy
    **/
    public function zero_tolerance(Request $request)
    {
        //
        $ntlogin = Auth::user()->ntlogin;

        $check_ntlogin = PopupsZeroTolerancePolicy::where('ntlogin', $ntlogin)->count();

        if($check_ntlogin == 0)
        {
            //  Insert Records
            $data = new PopupsZeroTolerancePolicy;
            $data->ntlogin = $ntlogin;
            $data->created_at = date('Y-m-d H:i:s.000');
            $data->save();
            //  Remove from session
            $request->session()->forget('user.popup.zero_tolerance_policy');
            
            //  Redirect
            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    *
    My Week Ending Schedule
    *
    **/
    public function my_week_ending_schedule(Request $request)
    {
        $dst_list = array(1, 2, 3, 11, 12);
        $date     = \Carbon\Carbon::now();
        $date->tz = 'EST';
        $date_now = $date->toDateString();

        $datetime_now   = $date->toDateTimeString();
        $week_ending    = $date->endOfWeek()->toDateString();

        $my_week_ending_accept = AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
                                    ->where('week_ending', $week_ending)
                                    ->where('week_ending_accept', 0)
                                    ->count();

        if ($my_week_ending_accept != 0)
        {
            AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
            ->where('week_ending', $week_ending)
            ->update([
                    'week_ending_accept'    => 1, 
                    'week_ending_accepted_at' => $datetime_now
                ]);

            $request->session()->forget('user.schedule.week_ending');

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    *
    My Shift Schedule
    *
    **/
    public function my_shift_schedule(Request $request)
    {
        $dst_list = array(1, 2, 3, 11, 12);
        $date     = \Carbon\Carbon::now();
        $date->tz = 'EST';
        $date_now = $date->toDateString();

        $datetime_now   = $date->toDateTimeString();
        $week_ending    = $date->endOfWeek()->toDateString();

        $my_shift_date_accept = AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
                                    ->where('shift_date', $date_now)
                                    ->where('shift_date_accept', 0)
                                    ->count();

        if ($my_shift_date_accept != 0)
        {
            AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
            ->where('shift_date', $date_now)
            ->update([
                    'shift_date_accept' => 1
                ]);

            $request->session()->forget('user.schedule.shift_date');

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
    /**
    *
    My Shift Schedule Break
    *
    **/
    public function my_break_schedule_notification(Request $request)
    {
        $dst_list   = array(1, 2, 3, 11, 12);
        $date       = \Carbon\Carbon::now();
        $date->tz   = 'EST';

        $date_less_5_minutes     = \Carbon\Carbon::now();
        $date_less_5_minutes->tz = 'EST';
        $date_less_5_minutes->subMinutes(5);

        $date_now   = $date->toDateString();
        $time_now   = $date->toTimeString();
        $datetime_now = $date->toDateTimeString();
        #$request->session()->forget('user.schedule.break.0');
        if(Session::has('user.schedule.break') && count(session('user.schedule.break'))-1 > 0)
        {
            if(Session::has('user.schedule.break.0'))
            {
              $break = session('user.schedule.break.0');
            }
            elseif(Session::has('user.schedule.break.1'))
            {
              $break = session('user.schedule.break.1');
            }
            elseif(Session::has('user.schedule.break.2'))
            {
              $break = session('user.schedule.break.2');
            }

            $break_hour     = date('H', strtotime($break));
            $break_minute   = date('i', strtotime($break));

            $first  = \Carbon\Carbon::create($date_less_5_minutes->year, $date_less_5_minutes->month, $date_less_5_minutes->day, $date_less_5_minutes->hour, $date_less_5_minutes->minute);
            $second = \Carbon\Carbon::create($date->year, $date->month, $date->day, $date->hour, $date->minute);

            $check_time = \Carbon\Carbon::create($date->year, $date->month, $date->day, $break_hour, $break_minute)->between($first, $second);

            if($check_time)
            {
                echo "$('#myBreaScheduleModal').foundation('reveal', 'open', {animation_speed: 0, close_on_background_click: false});";
            }

            /*echo '<br />';
            echo $date_less_5_minutes->toDateTimeString();
            echo '<br />';
            echo $date->toDateTimeString();*/
        }
    }

    public function my_break_schedule_accept(Request $request, $break_index)
    {
        $dst_list = array(1, 2, 3, 11, 12);
        $date     = \Carbon\Carbon::now();
        $date->tz = 'EST';
        $date_now = $date->toDateString();
        $datetime_now = $date->toDateTimeString();
        $week_ending = $date->endOfWeek()->toDateString();

        $break = AgentBreakSchedule::where('employee_number', Auth::user()
                ->employee_number)
                ->where('shift_date', $date_now)
                ->first();
        #dd($break);
        #if($break->break_1_accept == 0)
        if($break_index == 0)
        {
            AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
            ->where('shift_date', $date_now)
            ->update([
                    'break_1_accept'        => 1,
                    'break_1_accepted_at'   => $datetime_now
                ]);

            $request->session()->forget('user.schedule.break.0');

            return redirect()->route('dashboard');
        }
        #elseif($break->break_2_accept == 0)
        elseif($break_index == 1)
        {
            AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
            ->where('shift_date', $date_now)
            ->update([
                    'break_2_accept'        => 1,
                    'break_2_accepted_at'   => $datetime_now
                ]);

            $request->session()->forget('user.schedule.break.1');

            return redirect()->route('dashboard');
        }
        #elseif($break->break_3_accept == 0)
        elseif($break_index == 2)
        {
            AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
            ->where('shift_date', $date_now)
            ->update([
                    'break_3_accept'        => 1,
                    'break_3_accepted_at'   => $datetime_now
                ]);
            //  remove break in session
            $request->session()->forget('user.schedule.break');

            return redirect()->route('dashboard');
        }
        else
        {
            return redirect()->route('dashboard');
        }
    }
}
