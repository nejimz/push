<script type="text/javascript">
	/**
		Agent Metrics
	**/
	$(function(){

		var d = new Date(),
		n = d.getMonth(),
		y = d.getFullYear();
		var metric = $("#metric").val();
		loadMetric(metric);

		$('#metric_mm option:eq('+n+')').prop('selected', true);
		$('#metric_yy option[value="'+y+'"]').prop('selected', true);

		$('#metricTabs').on('toggled', function (event, tab) {
			$(".metricTableBody").html('');
			loadMetric($("#metric").val());
		});

	});

	function loadMetric(metric){
		//$('#body_weekly').append(metric + ' on ' + 'weekly');
		//$('#body_monthly').append(metric + ' on ' + 'monthly');
		//$('#body_bimonthly').append(metric + ' on ' + 'bimonthly');
	}
</script>