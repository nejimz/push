<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupsAttendancePolicy extends Model
{
    //
     public $table = 'popups_attendance_policy';
     public $timestamps = false;
}
