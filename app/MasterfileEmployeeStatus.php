<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileEmployeeStatus extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'employementstatus';
    public $timestamps = false;
}
