@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
    <div class="large-12 columns">

      <h4><i class="fa fa-users"></i> PUSH Add User Distribution</h4>

      @include('push.messages.error_message_list')
      @include('push.messages.success_message')

      <form  action="{{ route('user_distribution_store') }}" method="post">

        <div class="row">
          <div class="large-6 columns">
            <input id="ntlogin_search" name="ntlogin" type="text" placeholder="Ntlogin" value="{{ old('ntlogin') }}" autocomplete="off" required autofocus />
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <select id="distribution_id" name="distribution_id">
                <option value="" selected="selected">-- Select --</option>
              @foreach($distributions as $distribution)
                <option value="{{ $distribution->id }}">{{ $distribution->distribution }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <button class="button">Add</button>
            <a href="{{ route('user_distribution_index') }}" class="button secondary">Back</a>
          </div>
        </div>

        {!! csrf_field() !!}
      </form>
    </div>
    <script type="text/javascript">
    <?php
    if(old('distribution_id') != '')
    {
      echo '$(\'#distribution_id\').val(\'' . old('distribution_id') . '\');';
    }
    ?>
    </script>
<!-- Stops -->
@stop