<!-- My Schedule Popup -->
<div id="myBreaScheduleModal" class="reveal-modal" data-reveal aria-labelledby="mySchedule" aria-hidden="true" role="dialog" style="width: 400px;">
<?php
if(Session::has('user.schedule.break.0'))
{
  $break = session('user.schedule.break.0');
}
elseif(Session::has('user.schedule.break.1'))
{
  $break = session('user.schedule.break.1');
}
elseif(Session::has('user.schedule.break.2'))
{
  $break = session('user.schedule.break.2');
}
else
{
  $break = '';
}
?>
  <a href="{{ route('my_break_schedule_accept', [$break]) }}" class="button large expand success">You may now take your break!<br />Your Break is <strong>{{ $break }}</strong></a>
</div>
<script type="text/javascript">
$('#myScheduleModal').foundation('reveal', 'close');
</script>
