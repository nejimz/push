<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferLogCallDetails extends Model
{
    //
     public $table = 'transfer_log_call_details';
     public $timestamps = false;
     
}
