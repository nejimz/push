<!DOCTYPE html>
<html>
    <head>
        <title>500 Error</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <script src="{{ asset('js/vendor/jquery.js') }}"></script>

        <style>
            @font-face {
              font-family: 'Lato';
              font-style: normal;
              font-weight: 100;
              src: local('Lato Hairline'), local('Lato-Hairline'), url(../../fonts/lato-hairline.woff) format('woff');
            }
            
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .content p{
                font-size: 20pt;
                margin:0px;
            }

            .title {
                font-size: 66px;
            }

            .title h1{
                color:#008cba;
                font-size: 250px;
                margin:0px;
            }

            a{
                text-decoration: none;
                color:#cf2a0e;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div id="logo"></div>
            <div class="content">
                <div class="title"><h1>500</h1> Something is technically wrong.</div>
                <p>We're experiencing an internal server problem.  <a href="{{ route('dashboard') }}">Please try back later</a></p>
            </div>
        </div>
    </body>
</html>

