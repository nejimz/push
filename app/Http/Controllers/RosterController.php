<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Roster;
use Validator;

class RosterController extends Controller
{

    public function fileUploadsValidationRules() 
    {
        return [
            #'file' => 'required|mimes:csv,text'
            'file' => 'required',
            'extension' => 'in:csv'
        ];
    }

    public function fileUploadsValidationMessages() 
    {
        return [
            'file.required' => 'File is required.',
            'extension.in' => 'File is not a Comma Delimited or CSV file extension.'
        ];
    }

    public function index()
    {
        //

        $data = [
            #'files'=>$files,
            #'types'=>$types,
            'title'=>"Upload Call Center Roster"
        ];

        return view('push.cpanel.uploads.upload_roster', $data);
    }

    public function store(Request $request)
    {
        //
        $ext = '';

        if ($request->hasFile('file'))
        {
            $ext = strtolower($request->file('file')->getClientOriginalExtension());

            if ($ext != 'csv')
            {
                $ext = 'others';
            }
        }

        $input = [
            'file' => $request->file('file'),
            'extension' => $ext
        ];

        $validator = Validator::make(
            $input,
            $this->fileUploadsValidationRules(),
            $this->fileUploadsValidationMessages()
        );

        if ($validator->fails())
        {
            return redirect() -> route('upload_roster') -> withInput() -> withErrors($validator);
        }
        else
        {
            $file_location = '';

            $fileName = 'Roster_'.date('Ymd@his').'_'.str_random(15).'.'.$request->file('file')->getClientOriginalExtension();
            $file_location = 'uploads/roster/';
            //  upload file
            $request->file('file')->move($file_location, $fileName);
            //  Include filename
            $file_location .= ''.$fileName;
            //  Open File
            $csv = fopen($file_location, 'r');

            while (($element = fgetcsv($csv, 1000, ',')) !== FALSE)
            {
                $agent_details_trimmed = array_map('trim',$element);
                #dd(date('D', strtotime($agent_details_trimmed[8])));
                if (date('D', strtotime($agent_details_trimmed[8])) !== 'Sun'){
                    return redirect() -> route('upload_roster') -> with('phpError', 'Your weekending is not a Sunday!');
                }
                else 
                {
                    $agent_details_trimmed[3] = date("Y-m-d",strtotime($agent_details_trimmed[3]));
                    $agent_details_trimmed[8] = date("Y-m-d",strtotime($agent_details_trimmed[8]));
                    $agent_details = Roster::create([
                        'employee_ID'   =>  $agent_details_trimmed[0],
                        'organization'  =>  $agent_details_trimmed[1],
                        'avaya'         =>  $agent_details_trimmed[2],
                        'hire_date'     =>  $agent_details_trimmed[3],
                        'supervisor_ID' =>  $agent_details_trimmed[4],
                        'work_pattern'  =>  $agent_details_trimmed[5],
                        'tier'          =>  $agent_details_trimmed[6],
                        'ac_ID'         =>  $agent_details_trimmed[7],
                        'week_ending'   =>  $agent_details_trimmed[8]
                        ]);
                }
            }
            
            return redirect() -> route('upload_roster') -> with('uploadRosterSuccess', 'Roster successfully updated!');
        }
    }

    public function show()
    {
        //
        $search = '';
        $agents = Roster::leftJoin('users', 'call_center_roster.employee_ID', '=', 'users.employee_number')
                    ->orderBy('employee_ID', 'ASC')
                    ->paginate(50);

        $data = compact('agents', 'search');
        
        return view('push.cpanel.roster.index', $data);
    }
}