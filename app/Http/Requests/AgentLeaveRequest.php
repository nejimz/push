<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgentLeaveRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'leave_type'    => 'required',
            'reason'        => 'required',
            'leave_dates'   => 'required'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'leave_type.required'   => 'Type of Leave is required.',
            'reason.required'       => 'Reason is required.',
            'leave_dates.required'  => 'Leave Date/s is required.'
        ];
    }
}
