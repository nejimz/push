<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MasterfileSeparationUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'empID'             => 'required|exists:mysql2.separationinfo,empID',
            'SeparationDate'    => 'required',
            'separation_type'   => 'required',
            'reason'            => 'required',
            'hero'              => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'empID'             => 'Employee',
            'SeparationDate'    => 'Separation Date',
            'separation_type'   => 'Separation Type',
            'reason'            => 'Reason',
            'hero'              => 'Hero'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'exists'    => ':attribute not exists.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
