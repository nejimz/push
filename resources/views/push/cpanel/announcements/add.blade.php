@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
    <div class="large-12 columns">

      <h4><i class="fa fa-users"></i> PUSH Add User Announcement</h4>

      @include('push.messages.error_message_list')
      @include('push.messages.success_message')

      <form  action="{{ route('announcement_store') }}" method="post">

        <div class="row">
          <div class="large-6 columns">
            <input id="ntlogin_search" name="ntlogin" type="text" placeholder="Ntlogin" value="{{ old('ntlogin') }}" autocomplete="off" required />
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <button class="button">Add</button>
            <a href="{{ route('announcement_index') }}" class="button secondary">Back</a>
          </div>
        </div>

        {!! csrf_field() !!}
      </form>
    </div>
<!-- Stops -->
@stop