@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
    <div class="medium-12 columns">

      <h4><i class="fa fa-users"></i> PUSH Users</h4>

      @include('push.messages.error_message_list')
      @include('push.messages.success_message')

      <form  action="{{ route('users_update', [$user->ntlogin]) }}" method="post">

        <div class="row">
          <div class="large-2 columns">
            <input name="id" type="text" value="{{ $user->id }}" readonly="readonly" />
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <input name="ntlogin" type="text" value="{{ $user->ntlogin }}"  readonly="readonly" />
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <input name="name" type="text" value="{{ $user->name }}"  readonly="readonly" />
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <input name="employee_number" type="text" placeholder="Employee Number" value="{{ $user->employee_number }}" autocomplete="off" required />
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <select name="ou">
              <option value="">-- Select --</option>
              @foreach($ous as $ou)
                <option value="{{ $ou->name }}" {{ ($ou->name == $user->ou)? 'selected="selected"' : '' }}>{{ $ou->name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <button class="button">Add</button>
            <a href="{{ route('users_index') }}" class="button secondary">Back</a>
          </div>
        </div>

        {!! csrf_field() !!}
      </form>

    </div>
<!-- Stops -->
@stop