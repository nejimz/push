<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupsCodeOfConduct extends Model
{
    //
    public $table = 'popups_code_of_conduct';
    public $timestamps = false;
}
