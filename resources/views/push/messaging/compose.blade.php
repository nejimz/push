@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
		
		<div class="row" >
        	<div class="medium-12 columns">
        		<h4><i class="fa fa-envelope"></i> Compose</h4>
        	</div>
        </div><!-- row  # -->

		<div class="row" >
        	<div class="medium-12 columns">
		     	<form action="{{ route('send_message') }}" id="message-send" method="POST">
					@if($errors->has())
					<div data-alert class="alert-box warning">
						@foreach($errors->all() as $error)
							{{ $error }}<br />
						@endforeach
						<a href="#" class="close">&times;</a>
			        </div>
			        @elseif(Session::has('messageSuccess'))
					<div data-alert class="alert-box success">
						{{ Session::get('messageSuccess') }}
						<a href="#" class="close">&times;</a>
			        </div>
			       	@endif

					<input name="message-to" id="message-to" type="text" placeholder="To">
					<input name="message-subject" id="message-subject" type="text" value="{{ old('message-subject') }}" placeholder="Subject">
				    <textarea name="message-body" id="messageBody" class="ckeditor" placeholder="Message" rows='15'>
				    	{{ old('message-body') }}
				    </textarea>

				    <button type="submit" href="#" class="button small right">Send</button>
				    <a href="{{ route('inbox') }}" class="button small secondary right close-modal-button">Cancel</a>
					
				    {!! csrf_field() !!}
				    
				</form>
					<input type="hidden" id="user_ldap_search" name="user_ldap_search" value="{{ route('user_ldap_search') }}">
			</div>
		</div>
<!-- Stops -->
@stop
