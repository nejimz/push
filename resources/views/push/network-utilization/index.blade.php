@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
	<div class="row">
       	<div class="medium-12 columns">
       		<h4><i class="fa fa-unlock"></i> Network Utilization</h4>
       	</div>
    </div><!-- row  # -->
    <div class="row">
    	<div class="medium-12 columns">
       		<?php 
       			$pldtTable = Lava::DataTable();

       			$pldtTable->addDateColumn('Day of Month')
				          ->addNumberColumn('Projected')
				          ->addNumberColumn('Official');

				for ($a = 1; $a < 30; $a++)
				{
				    $rowData = array(
				      "2014-8-$a", rand(800,1000), rand(800,1000)
				    );

				    $pldtTable->addRow($rowData);

				}
				$chart = Lava::LineChart('myFancyChart');  

				$chart->datatable($pldtTable);

				echo Lava::render('LineChart', 'myFancyChart', 'myStocks');
       		?>

       		<div id="myStocks"></div>
       	</div>
    </div>  
<!-- Stops -->
@stop