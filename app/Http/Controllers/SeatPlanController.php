<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//use App\SeatPlan;
use Validator;

class SeatPlanController extends Controller
{

    public function index()
    {
        //

        $data = [
            #'files'=>$files,
            #'types'=>$types
        ];
        
        return view('push.seatplan_control', $data);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
