<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class PayslipPasswordReset extends Model
{
	public function scopePayslipInfo($query, $employeeid){
		return DB::connection('payslip')
					->table('users')
					->where('empno',"=",$employeeid);
	}

	public function scopeUpdatePayslip($query, $userid){
		return DB::connection('payslip')
					->table('users')
					->where('userid','=',$userid);
	}
}