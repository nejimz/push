<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Auth;

use Validator;

class SupervisorActivityLoggerController extends Controller
{
    public function index()
    {
        
        //
        $employee_number = Auth::user()->employee_number;
        $employee = DB::connection('mysql2')
                ->table('employee')
                ->select([
                    'firstName','middleName','lastName','suffix','birthDate','gender',
                    'civil_status_name','contactNumber','contactNumber2','telNumber',
                    'oldAddress','street','subd','sitio','brgy','city','province',
                    'empNumber','waveNumber','branchName','empDate','TaxStatusName',
                    'departments.department','departments.position',
                    'tin','sss','philhealth','hdmf'
                ])
                ->leftJoin('employeeaccount', 'employeeaccount.empID', '=', 'employee.empID')
                ->leftJoin('employeeaddress', 'employeeaddress.empID', '=', 'employee.empID')
                ->leftJoin('employmentinfo', 'employmentinfo.empID', '=', 'employee.empID')
                ->leftJoin('employeecredentials', 'employeecredentials.empID', '=', 'employee.empID')
                ->leftJoin('employeepaydetails', 'employeepaydetails.empID', '=', 'employee.empID')

                ->leftJoin('civil_status', 'civil_status.civil_status_id', '=', 'employeepaydetails.civilStatus')
                ->leftJoin('taxstatus', 'taxstatus.TaxStatusID', '=', 'employeepaydetails.taxStatus')
                ->leftJoin('departments', 'departments.idDep', '=', 'employmentinfo.position')
                ->leftJoin('branch', 'branch.branchID', '=', 'employmentinfo.branchID')
                ->where('empNumber', $employee_number)
                ->first();

        if(is_null($employee))
        {
           abort(404);
        }
    
        $data = [
            'title'=>"Supervisor Activity Log",
            'emp_id'=>$employee_number,
            'employee'=>$employee
        ];

        #dd($data);

        return view('push.activity-logger.supervisor_logger', $data);
    }
}

?>