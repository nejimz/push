<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileTaxStatus extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'taxstatus';
    public $timestamps = false;
}
