<?php

namespace App;

use DB;
use Auth;

use Illuminate\Database\Eloquent\Model;

class ReleaseCallLog extends Model
{
    // 
    public $table = 'release_call_log';

    public function setUpdatedAtAttribute($value)
	{
	// Do nothing.
	}

	public function reason()
	{
		return $this->belongsTo('App\ReleaseCallValidReasons','reason_ID','reason_ID');
	}

	public function scopeTimeReport($query, $start, $end)
	{
		//
		$query->whereBetween('created_at', array($start, $end));

		return $query;
	}

	public function scopeTeamMembers($query)
	{
		$query->whereIn('employee_ID', Roster::MyAgents(Auth::user()->employee_number)->lists('employee_ID'))
			->orderBy('employee_ID', 'reason_ID');

		return $query;
	}

	public function scopeCountReasons($query)
	{
		$query->select(DB::raw('employee_ID, reason_ID, count(reason_ID) as reason_count, created_at'))
			->whereIn('employee_ID', Roster::MyAgents(Auth::user()->employee_number)->lists('employee_ID'))
			->groupBy('employee_ID', 'reason_ID', 'created_at')
			->orderBy('employee_ID');

		return $query;
	}

}
