<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

class MetricsController extends Controller
{

    public function showMetric($id = "")
    {
        //
        
        return view('push.metrics/metrics', ['specMetric' => $id]);
    }
}
