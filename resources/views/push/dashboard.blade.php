@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

        @if($allowed_post >= 1)
    
        <script>
          //CKEDITOR.instances;
          $(function(){
            //CKEDITOR.disableAutoInline = true;
            // CKEDITOR.replace( 'editor1',{
            //   skin : 'office2013',
            //   removePlugins: 'resize,sharedspace',
            //   toolbarGroups: [
            //     { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            //     { "name" : "basicstyles", "groups" : [ "basicstyles" ] },
            //     { "name" : "paragraph", "groups" : [ "list", "align" ] },
            //     { "name" : "styles" },
            //     { "name" : "insert" },
            //   ],
            //   removeButtons: 'Save,Source,Templates,Print,Strike,Subscript,Superscript,Styles,Format,Iframe,Flash,Image,PageBreak'
            // } );
            //$('#editor1').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
            //$('#editable').ckeditor(); // Use CKEDITOR.inline().
          });

        </script>

        <div class="row">
          <div class="large-12 columns">
            <h4><i class="fa fa-pencil-square-o"></i> News Update</h4>
          </div>
        </div>

        <form action="{{ route('dashboard_store') }}" method="post">
          <div class="row">
            <div class="large-12 columns">
            @if($errors->has())
              <div data-alert class="alert-box warning">
                @foreach($errors->all() as $error)
                    {{ $error }}<br />
                @endforeach
                <a href="#" class="close">&times;</a>
              </div>
            @elseif(Session::has('announcementSuccess'))
              <div data-alert class="alert-box success">
                    {{ Session::get('announcementSuccess') }}
                <a href="#" class="close">&times;</a>
              </div>
            @endif
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns">
              <textarea class="ckeditor" id="editor1" name="editor1"></textarea>
              <br />
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns">
              {!! csrf_field() !!}
              <button type="submit" class="button success right"><i class="fa fa-check"></i> Post Announcement</button>
            </div>
          </div>

        </form>
        
        @endif

        <div class="row">
          <div class="large-12 columns">
            <h4><i class="fa fa-book"></i> Announcements</h4>
          </div>
        </div>
        <div class="row">
          @if($count_announcement != 0)
            @foreach($announcements as $announcement)
            <div class="large-12 columns">
              <div class="panel callout">
                <div class="row">
                  <div class="large-12 columns text-right">
                    <span class="postDateFormat">{{ date('F d, Y h:i a', strtotime($announcement->created_at)) }}</span>
                  </div>
                </div>
                  {!! $announcement->message !!}

                <div class="large-6 columns">
                  <em style="color: #9c9c9c;">Posted by <a href="#"> {{ $announcement->name }}</a></em>
                </div>
              </div><!-- panel columnss -->
            </div><!-- large-12 columns -->
            @endforeach
          @else
            <div class="large-12 columns">
              <div class="panel callout">
                <div class="row">
                  <center><h6>No Announcements posted yet.</h6></center>
              </div><!-- panel columns -->
            </div><!-- large-12 columns -->
          @endif

        </div><!-- row -->

<!-- Stops -->
@stop