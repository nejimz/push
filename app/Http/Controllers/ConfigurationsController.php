<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Users;
use App\UsersAdmin;

class ConfigurationsController extends Controller
{
	/**
    Authenticate Admin Account
    **/
    public function __construct()
    {
        //  Only Allowed OU's can use the messaging 
        if(UsersAdmin::where('ntlogin', Auth::user()->ntlogin)->count() == 0)
        {
            abort(404);
        }
    }

    public function index(){
        return view('push.cpanel.index');
    }
}