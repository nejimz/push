
	<br /> <!-- Managers -->
	<ul id="pushMainNavLeft" class="side-nav">
		<li class="{{ \App\Helpers\Menu::activeMenu(['release_call_log', 'release_call_show']) }}">
            <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropReleaseCall" aria-controls="dropReleaseCall" aria-expanded="false" href="#">Release Call »</a>
            <ul id="dropReleaseCall" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="{{ route('reports_CMS') }}">Release Call Log Reports</a></li>
            </ul>
          </li>
    </ul>