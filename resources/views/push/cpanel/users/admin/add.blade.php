@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
    <div class="large-12 columns">

      <h4><i class="fa fa-users"></i> PUSH Admin Add</h4>

      @include('push.messages.error_message_list')
      @include('push.messages.success_message')

      <form  action="{{ route('users_admin_store') }}" method="post">
        <div class="row">
          <div class="large-6 columns">
            <div class="row collapse">
              <div class="small-8 columns">
                <input id="ntlogin_search" name="ntlogin" type="text" placeholder="Ntlogin" value="{{ old('ntlogin') }}" autocomplete="off" required />
              </div>
              <div class="small-2 columns">
                <button class="button postfix">Add</button>
              </div>
              <div class="small-2 columns">
                <a href="{{ route('users_admin') }}" class="button secondary postfix">Back</a>
              </div>
            </div>
          </div>
        </div>
        {!! csrf_field() !!}
      </form>
    </div>
<!-- Stops -->
@stop