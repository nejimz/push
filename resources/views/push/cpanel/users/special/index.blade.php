@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
   	<div class="large-12 columns">

   		<h4><i class="fa fa-users"></i> PUSH Special Permission </h4>

   		<div class="row">
   			<a href="{{ route('users_special_add') }}" class="button button-small"><i class="fa fa-plus"></i> Add</a>
   		</div>

		@include('push.messages.success_message')

		{!! str_replace('/?', '?', $admins->render()) !!}
		<table width="100%">
			<thead>
				<tr>
					<th width="5%"></th>
					<th width="20%">Employee No.</th>
					<th width="20%">NT-Login</th>
					<th width="30%">Name</th>
					<th width="25%">OU</th>
				</tr>
			</thead>
			<tbody>
				@foreach($admins as $admin)
				<tr>
					<td><a href="{{ route('users_special_destroy', [$admin->ntlogin]) }}"><i class="fa fa-times"></i></a></td>
					<td>{{ $admin->employee_number }}</td>
					<td>{{ $admin->ntlogin }}</td>
					<td>{{ $admin->name }}</td>
					<td>{{ $admin->ou }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! str_replace('/?', '?', $admins->render()) !!}
   	</div>
<!-- Stops -->
@stop