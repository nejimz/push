@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

<div class="row">
  <div class="large-12 columns">
    <h4><i class="fa fa-unlink"></i> Separation</h4>
  </div>
</div><!-- row -->

<div class="row">
  <div class="large-6 columns">

    <form action="{{ $action }}" method="post">

      <div class="row">
        <div class="large-12 columns">
          <label for="Employee">Employee</label>
          <input type="text" name="Employee" id="Employee" value="{{ $Employee }}" readonly />
        </div>
      </div>

      <div class="row">
        <div class="large-12 columns">
          <label for="SeparationDate">Separation Date</label>
          <input type="text" name="SeparationDate" id="SeparationDate" value="{{ $SeparationDate }}" data-date-format="%m/%d/%Y" data-date />
        </div>
      </div>

      <div class="row">
        <div class="large-12 columns">
          <label for="separation_type">Separation Type</label>
          <select name="separation_type" id="separation_type">
            <option value=""></option>
            <option value="3">Resignation</option>
            <option value="4">Termination</option>
            <option value="5">Resignation.Preferred</option>
            <option value="6">Deceased</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="large-12 columns">
          <label for="first_name">Reason</label>
          <textarea rows="10" name="reason" id="reason">{{ $reason }}</textarea>
        </div>
      </div>

      <div class="row">
        <div class="large-12 columns">
          <label for="hero">Hero</label>
          <select name="hero" id="hero">
            <option value=""></option>
            @foreach($heroes as $row)
            <option value="{{ $row->empID }}">{{ $row->lastName }}, {{ $row->firstName }}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="row">
        <div class="large-4 columns">
          {!! csrf_field() !!}
          <input type="hidden" name="manager" value="{{ $manager }}">
          <input type="hidden" name="supervisor" value="{{ $supervisor }}">
          <input type="hidden" name="ac" value="{{ $ac }}">
          <input type="hidden" name="empID" value="{{ $id }}">
          <button type="submit" class="button expand">Submit</button>
        </div>
        <div class="large-4 columns">&nbsp;</div>
        <div class="large-4 columns">&nbsp;</div>
      </div>

    </form>

  </div>
  <div class="large-6 columns">
    &nbsp;
    @include('push.messages.error_message_list')
    @include('push.messages.success_message')
  </div>
</div>

<input type="hidden" id="position_url" value="{{ route('masterfile_poistions_url') }}">

<script type="text/javascript">
<?php
if($separation_type != '')
{
  echo '$(\'#separation_type\').val(\'' . $separation_type . '\');';
}

if($hero != '')
{
  echo '$(\'#hero\').val(\'' . $hero . '\');';
}
?>
</script>
<!-- Stops -->
@stop