<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MasterfileEmployeeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if(is_null($this->id))
        {
            return [
                'employee_number'   => 'required|unique:mysql2.employeeaccount,empNumber',
                'first_name'        => 'required',
                'last_name'         => 'required',
                'branch'            => 'required',
                'hired_date'        => 'required',
                'employment_status' => 'required',
                'department'        => 'required',
                'position'          => 'required'
            ];
        }
        else
        {
            return [
                'employee_number'   => 'required|unique:mysql2.employeeaccount,empNumber,' . $this->id.',empID',
                'first_name'        => 'required',
                'last_name'         => 'required',
                'branch'            => 'required',
                'hired_date'        => 'required',
                'employment_status' => 'required',
                'department'        => 'required',
                'position'          => 'required'
            ];
        }
    }

    public function messages()
    {
        return [
            'employee_number.required'  => 'Employee Number is required.',
            'employee_number.unique'    => 'Employee Number already exists.',
            'first_name.required'       => 'First Name is required.',
            'last_name.required'        => 'Last Name is required.',
            'branch.required'           => 'Branch is required.',
            'hired_date.required'       => 'Hired Date is required.',
            'employment_status.required'=> 'Employment Status is required.',
            'department.required'       => 'Department is required.',
            'position.required'         => 'Position is required.'
        ];
    }
}
