<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ous', function (Blueprint $table) {
            $table->string('name')->unique();
        });

        DB::table('ous')->insert( [ 'name' => 'Accounting' ] );
        DB::table('ous')->insert( [ 'name' => 'Administration' ] );
        DB::table('ous')->insert( [ 'name' => 'CA' ] );
        DB::table('ous')->insert( [ 'name' => 'CMSGroup' ] );
        DB::table('ous')->insert( [ 'name' => 'ERD' ] );
        DB::table('ous')->insert( [ 'name' => 'HRD' ] );
        DB::table('ous')->insert( [ 'name' => 'IT Systems' ] );
        DB::table('ous')->insert( [ 'name' => 'Legacy' ] );
        DB::table('ous')->insert( [ 'name' => 'Managers' ] );
        DB::table('ous')->insert( [ 'name' => 'PagePlus' ] );
        DB::table('ous')->insert( [ 'name' => 'Quality' ] );
        DB::table('ous')->insert( [ 'name' => 'RAPID' ] );
        DB::table('ous')->insert( [ 'name' => 'Receptionist' ] );
        DB::table('ous')->insert( [ 'name' => 'Recruitment' ] );
        DB::table('ous')->insert( [ 'name' => 'SimpleMobile' ] );
        DB::table('ous')->insert( [ 'name' => 'SME' ] );
        DB::table('ous')->insert( [ 'name' => 'StraightTalk' ] );
        DB::table('ous')->insert( [ 'name' => 'Supervisors' ] );
        DB::table('ous')->insert( [ 'name' => 'SupGroup' ] );
        DB::table('ous')->insert( [ 'name' => 'Training' ] );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ous');
    }
}
