

        <br /> <!-- CMS Group -->
        <ul id="pushMainNavLeft" class="side-nav">
          <li class="{{ \App\Helpers\Menu::activeMenu(['file_uploads', 'upload_agent_schedules']) }}">
            <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropFileUploads" aria-controls="dropFileUploads" aria-expanded="false" href="#">File Uploads »</a>
            <ul id="dropFileUploads" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="{{ route('upload_roster') }}">Call Center Roster</a></li>
            </ul>
          </li>
          <li class="{{ \App\Helpers\Menu::activeMenu(['release_call_log', 'release_call_show']) }}">
            <a data-options="is_hover:true; hover_timeout:100; align:right;" data-dropdown="dropReleaseCall" aria-controls="dropReleaseCall" aria-expanded="false" href="#">Release Call »</a>
            <ul id="dropReleaseCall" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
              <li><a href="{{ route('reports_CMS') }}">CMS Reports</a></li>
            </ul>
          </li>
        </ul>