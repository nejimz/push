<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileEmployeeCredentials extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'employeecredentials';
    public $timestamps = false;
}
