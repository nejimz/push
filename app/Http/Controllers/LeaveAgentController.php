<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LeaveAgent;
use Auth;
use Response;

class LeaveAgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $leaves = LeaveAgent::where('ntlogin', Auth::user()->ntlogin)->orderBy('leave_date', 'DESC')->paginate(50);

        $data = compact( 'leaves' );

        return view('push.leave.agent.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $action      = route('leave_agent_store');
        
        $leave_types = [
            'Vacation Leave',
            'Sick Leave',
            'Birthday Leave',
            'Emergency Leave',
            'Maternity Leave',
            'Single Parent Leave',
            'Paternity Leave',
            'Other'
        ];

        $leave_type  = old('leave_type');
        $reason      = old('reason');
        $leave_dates = old('leave_dates');

        $data = compact( 'action', 'leave_types', 'leave_type', 'reason', 'leave_dates' );

        return view('push.leave.agent.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\AgentLeaveRequest $request)
    {
        foreach ($request->input('leave_dates') as $date)
        {
            $row = new LeaveAgent;

            $row->ntlogin       = Auth::user()->ntlogin;
            $row->leave_type    = $request->input('leave_type');
            $row->leave_date    = $date;
            $row->reason        = $request->input('reason');

            $row->save();
        }

        return redirect()->route('leave_agent_create')->with('success', 'Leave Application successfully submitted!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function filed_leaves()
    {
        $action      = route('leave_agent_download');

        $data = compact( 'action' );

        return view('push.leave.agent.agent_filed_leaves', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function download(Request $request)
    {
        $date = trim($request->input('leave_date'));
        $leaves = LeaveAgent::where('leave_date', $date)->orderBy('ntlogin', 'ASC')->get();
        
        $content = "<table border=\"1\">" . //
                  "<thead>" . //
                     "<th>Employee Number</th>" . //
                     "<th>Name</th>" . //
                     "<th>Leave Type</th>" . //
                     "<th>Leave Date</th>" . //
                     "<th>Reason</th>" . //
                  "</thead>";
        
        foreach ($leaves as $row) {
            $content .= "<tr>" . 
                      "<td>" . $row->employee_number .
                      "</td><td>" . $row->employee_name .
                      "</td><td>" . $row->leave_type .
                      "</td><td>" . $row->leave_date .
                      "</td><td>" . $row->reason .
                      "</td>" . 
                      "</tr>";
        }
        $content .= "</table>";
        
        $headers    = array('content-type'=>'application/vnd.ms-excel');
        
        return Response::make($content, 200, $headers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function add_leave_dates(Request $request)
    {
        /*$date = $request->input('date');

        //  Check if already have a session
        if($request->session()->has('user.leave'))
        {
            //  Check if date is already exists
            if(!in_array($date, session('user.leave')))
            {
                //  Count the number of records in session
                $count = count(session('user.leave')) + 1;
                //  Add to Current Leave Session
                $request->session()->put('user.leave.' . $count, $date);
            }
        }
        else
        {
            //  Add Leave to Session
            $request->session()->push('user.leave', $date);
        }*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function show_leave_dates($id)
    {
        //
    }
}
