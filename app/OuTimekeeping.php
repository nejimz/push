<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UsersAdmin;
use App\Users;
use Auth;

class OuTimekeeping extends Model
{
    public function scopeDepartmentOptions($query)
    {
    	

    	if(Auth::user()->ou == 'IT Systems')
        {
            $list =  [['id'=>'6','value'=>'IT'],
            			['id'=>'159','value'=>'Manual LILO']];

            if(in_array(Auth::user()->ntlogin, ['jvmacalalag', 'bjanono', 'amdelossantos', 'jsjacob', 'whcañete']))
            {
            	$list =  [['id'=>'1','value'=>'Accounting'],
                            ['id'=>'2','value'=>'CMS'],
							['id'=>'3','value'=>'Coordinators'],							
							['id'=>'170','value'=>'Corporate audit'],
                            ['id'=>'4','value'=>'Facilities'],
                            ['id'=>'5','value'=>'HR'],
                            ['id'=>'6','value'=>'IT'],  
							['id'=>'15','value'=>'Operations'],
							['id'=>'8','value'=>'Quality'],
							['id'=>'9','value'=>'Rapid'],
							['id'=>'10','value'=>'Recruitment'],
							['id'=>'11','value'=>'SME'],
							['id'=>'13','value'=>'Training'],
							['id'=>'159','value'=>'Manual LILO']];
            }
        }

        #For Admin Access
        if(UsersAdmin::where('ntlogin',Auth::user()->ntlogin)->count() != 0)
        {
            $list =  [['id'=>'1','value'=>'Accounting'],
                            ['id'=>'2','value'=>'CMS'],
							['id'=>'3','value'=>'Coordinators'],							
							['id'=>'170','value'=>'Corporate audit'],
                            ['id'=>'4','value'=>'Facilities'],
                            ['id'=>'5','value'=>'HR'],
                            ['id'=>'6','value'=>'IT'],  
							['id'=>'15','value'=>'Operations'],
							['id'=>'8','value'=>'Quality'],
							['id'=>'9','value'=>'Rapid'],
							['id'=>'10','value'=>'Recruitment'],
							['id'=>'11','value'=>'SME'],
							['id'=>'13','value'=>'Training'],
							['id'=>'159','value'=>'Manual LILO']];
        }

		#For HR Access
    	if(Auth::user()->ou == 'HRD')
    	{
	   		$list =  [['id'=>'1','value'=>'Accounting'],
	   						['id'=>'4','value'=>'Facilities'],
	   						['id'=>'5','value'=>'HR'],
	   						['id'=>'6','value'=>'IT'],
	   						['id'=>'10','value'=>'Recruitment'],
	   						['id'=>'159','value'=>'Manual LILO']];
	   	}

	   	#For DMG Access
    	if(Auth::user()->ou == 'RAPID')
    	{
			$list =  [['id'=>'2','value'=>'CMS'],
							['id'=>'3','value'=>'Coordinators'],							
							['id'=>'170','value'=>'Corporate audit'],
							['id'=>'15','value'=>'Operations'],
							['id'=>'8','value'=>'Quality'],
							['id'=>'9','value'=>'Rapid'],
							['id'=>'11','value'=>'SME'],
							['id'=>'13','value'=>'Training'],
							['id'=>'159','value'=>'Manual LILO']];
    	}
	   	return $list;
    }
}
