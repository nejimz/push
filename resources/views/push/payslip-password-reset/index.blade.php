@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
		
		<div class="row">
        	<div class="medium-12 columns">
        		<h4><i class="fa fa-unlock"></i> Payslip Password Reset</h4>
        	</div>
        </div><!-- row  # -->
        <div class="row">
			<div class="large-4 large-centered columns">
				<a href="{{ route('payslip_generate') }}" class="button small">Click here to generate payslip password</a>
			</div>
		</div>
		<div class=="row">
			<div class="large-7 large-centered columns">
				@if(isset($payslip_info))
					<div data-alert class="alert-box info"> 
						Payslip password has been reset. 
						@if($payslip_info->username != Auth::user()->ntlogin)
							<div style="color:red;"><strong> Payslip username has been changed. Please see details for your login: </strong></div>
						@endif
					</div>

					@if($payslip_info->username != Auth::user()->ntlogin)
						<div class="row">
							<div class="large-6 columns text-right">
								<label><strong>Old Username: </strong></label>
							</div>
							<div class="large-6 columns">
								<span style="font-family: monospace;"> {{ $payslip_info->username }} </span>
							</div>
						</div>
					@endif
					
					<div class="row">					
						<div class="large-6 columns text-right">
							<label><strong>{{ ($payslip_info->username == Auth::user()->ntlogin) ? "Username:" : "New Username:" }} </strong></label>
						</div>
						<div class="large-6 columns">
							<span style="font-family: monospace;">{{ ($payslip_info->username == Auth::user()->ntlogin) ? $payslip_info->username : Auth::user()->ntlogin }}</span>
						</div>					
					</div>
					<div class="row">
						<div class="large-6 columns text-right">
							<label><strong>Password: </strong></label>
						</div>
						<div class="large-6 columns">
							<span style="font-family: monospace;"><a href="#" onclick=' copyToClipboard("{{ $payslip_pass or "" }}"); '>{{ $payslip_pass or "" }}</a></span>	
						</div>			
					</div>
				@endif
				
			</div>
		</div>
<!-- Stops -->
@stop

@section('scripts')
  @include('push.scripts.payslip_reset_js')
@stop