<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

#Route::get('info', function() { phpinfo(); });
#Route::get('wincache', function() {	return view('vendor.wincache'); });

# Notification
Route::get('my-break-schedule-notification', ['as' => 'my_break_schedule_notification', 'uses' => 'PopupController@my_break_schedule_notification']);
# End Notification

# Authentication
Route::get('/', ['as' => 'welcome', 'uses' => 'AuthenticationController@welcome']);
Route::post('authentication', ['as' => 'authentication', 'uses' => 'AuthenticationController@authentication']);

# Authentication Backdoor
Route::get('/authentication-backdoor/{ntlogin}', ['as' => 'authentication_backdoor', 'uses' => 'AuthenticationController@authentication_backdoor']);

#	Portal
Route::group(['prefix' => 'portal', 'middleware' => 'auth'], function(){

	# Logout Backdoor
	Route::get('logout-backdoor', ['as' => 'logout', 'uses' => 'AuthenticationController@logout']);

	# Popup Activation
	Route::get('information-security', ['as' => 'information_security', 'uses' => 'PopupController@information_security']);
	Route::get('attendance-policy', ['as' => 'attendance_policy', 'uses' => 'PopupController@attendance_policy']);
	Route::get('csr-non-performance-policy', ['as' => 'csr_non_performance_policy', 'uses' => 'PopupController@csr_non_performance_policy']);
	Route::get('code-of-conduct', ['as' => 'code_of_conduct', 'uses' => 'PopupController@code_of_conduct']);
	Route::get('zero-tolerance', ['as' => 'zero_tolerance', 'uses' => 'PopupController@zero_tolerance']);
	Route::get('refresh-policy', ['as' => 'refresh_policy', 'uses' => 'PopupController@refresh_policy']);
	Route::get('my-week-ending-schedule', ['as' => 'my_week_ending_schedule', 'uses' => 'PopupController@my_week_ending_schedule']);
	Route::get('my-shift-schedule', ['as' => 'my_shift_schedule', 'uses' => 'PopupController@my_shift_schedule']);
	Route::get('my-break-schedule-accept/{break_index}', ['as' => 'my_break_schedule_accept', 'uses' => 'PopupController@my_break_schedule_accept']);

	# Dashboard
	Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
	Route::post('dashboard_store', ['as' => 'dashboard_store', 'uses' => 'DashboardController@store']);

	# Profile
	Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);

	# Messaging
	Route::group(['prefix' => 'message'], function(){
		# Message Views
		Route::get('inbox', ['as' => 'inbox', 'uses' => 'ConversationController@index']);
		Route::get('messages/{conversation_id?}/{conversation_member_id?}/{_?}', ['as' => 'showmessages', 'uses' => 'ConversationController@messages']);
		Route::get('my-messages', ['as' => 'my_messages', 'uses' => 'ConversationController@my_messages']);
		Route::get('notification', ['as' => 'notification_message', 'uses' => 'ConversationController@notification']);
		
		# Message Forms
		Route::get('compose', ['as' => 'compose_message', 'uses' => 'ConversationController@compose']);
		Route::get('reply/{conversation_id}/{conversation_member_id?}/{_}', ['as' => 'reply_message', 'uses' => 'ConversationController@reply']);
		
		# Message Submits
		Route::post('send-reply', ['as' => 'send_reply_message', 'uses' => 'ConversationController@send_reply']);
		Route::post('send', ['as' => 'send_message', 'uses' => 'ConversationController@send']);
		Route::get('delete/{id}', ['as' => 'delete_message', 'uses' => 'ConversationController@delete']);

		Route::get('user-ldap-search', ['as' => 'user_ldap_search', 'uses' => 'ConversationController@searchldap']);
		Route::get('check-recipient', ['uses' => 'ConversationController@checkreceipient']);
	});
	
	# Payslip Password Reset
	Route::group(['prefix' => 'reset-payslip-password'], function(){
		Route::get('/', ['as' => 'payslip_index', 'uses' => 'PayslipPasswordResetController@index']);
		Route::get('generate', ['as' => 'payslip_generate', 'uses' => 'PayslipPasswordResetController@generate']);
	});

	#	View
	Route::get('avaya-one-x-guide', ['as' => 'avaya_one_x_guide', 'uses' => 'FileUploadsController@avaya_one_x_guide']);
	Route::get('paid-leaves', ['as' => 'paid_leaves', 'uses' => 'FileUploadsController@paid_leaves']);
	Route::get('it-security-policy', ['as' => 'it_security_policy', 'uses' => 'FileUploadsController@it_security_policy']);
	Route::get('qa-newsletter', ['as' => 'qa_newsletter', 'uses' => 'FileUploadsController@qa_newsletter']);

	# Transfer Log
	Route::group(['prefix' => 'transfer-log'], function(){
		Route::get('logger/{account_id?}/{skill_id?}', ['as' => 'transfer_log', 'uses' => 'TransferLogController@index']);
		Route::get('populate-calls', ['as' => 'populate_calls', 'uses' => 'TransferLogController@callsreceiveddropdown']);
		Route::get('populate-extDept', ['as' => 'populate_extension', 'uses' => 'TransferLogController@extdept']);
		Route::post('save-transfer-log', ['as' => 'save_transfer_log', 'uses' => 'TransferLogController@store']);
		Route::get('transfer-log-report', ['as' => 'transfer_log_report', 'uses' => 'TransferLogController@show']);
	});
	
	# Leave
	Route::group(['prefix' => 'leave'], function(){
		# Agent
		Route::group(['prefix' => 'agent'], function(){
			Route::get('/', ['as' => 'leave_agent_index', 'uses' => 'LeaveAgentController@index']);
			Route::get('/filed-leaves', ['as' => 'leave_agent_filed', 'uses' => 'LeaveAgentController@filed_leaves']);
			Route::post('/download-filed-leaves', ['as' => 'leave_agent_download', 'uses' => 'LeaveAgentController@download']);

			Route::get('/create', ['as' => 'leave_agent_create', 'uses' => 'LeaveAgentController@create']);
			Route::post('/store', ['as' => 'leave_agent_store', 'uses' => 'LeaveAgentController@store']);

			/*Route::get('/add-leave-date', ['as' => 'leave_agent_add_date', 'uses' => 'LeaveAgentController@add_leave_dates']);
			Route::get('/show-leave-dates', ['as' => 'leave_agent_show_dates', 'uses' => 'LeaveAgentController@show_leave_dates']);*/
		});# Agent
	});# Leave
	
	# Materfile
	Route::group(['prefix' => 'masterfile'], function(){
		Route::get('/', ['as' => 'masterfile_index', 'uses' => 'MasterfileController@index']);
		Route::get('/profile/{id}', ['as' => 'masterfile_profile', 'uses' => 'MasterfileController@profile']);

		Route::get('/create', ['as' => 'masterfile_create', 'uses' => 'MasterfileController@create']);
		Route::post('/store', ['as' => 'masterfile_store', 'uses' => 'MasterfileController@store']);

		Route::get('/edit/{id}', ['as' => 'masterfile_edit', 'uses' => 'MasterfileController@edit']);
		Route::post('/update/{id}', ['as' => 'masterfile_update', 'uses' => 'MasterfileController@update']);

		Route::get('/separation/{id}', ['as' => 'masterfile_separation_create', 'uses' => 'MasterfileController@separation_create']);
		Route::post('/separation-store/{id}', ['as' => 'masterfile_separation_store', 'uses' => 'MasterfileController@separation_store']);

		Route::get('/separation-edit/{id}', ['as' => 'masterfile_separation_edit', 'uses' => 'MasterfileController@separation_edit']);
		Route::post('/separation-update/{id}', ['as' => 'masterfile_separation_update', 'uses' => 'MasterfileController@separation_update']);

		Route::get('/positions/{depID?}', ['as' => 'masterfile_poistions_url', 'uses' => 'MasterfileController@positions']);

	});# Materfile
	
	# Ezwatcher
	Route::group(['prefix' => 'timekeeping'], function(){
		Route::get('ezwatch', ['as' => 'ezwatch', 'uses' => 'TimeKeepingController@ezwatch']);
		Route::get('doorlog', ['as' => 'doorlog', 'uses' => 'TimeKeepingController@doorlog']);

		Route::get('search/employee', ['as' => 'ez_search_employee', 'uses' => 'TimeKeepingController@ezsearchemployee']);
		
		Route::get('ezwatch/generate', ['as' => 'ez_search_lilo_generate', 'uses' => 'TimeKeepingController@ezsearchlilogenerate']);
		Route::get('ezwatch/download', ['as' => 'ez_search_lilo_download', 'uses' => 'TimeKeepingController@ezsearchlilodownload']);
		Route::get('ezwatch/preview', ['as' => 'ez_search_lilo_preview', 'uses' => 'TimeKeepingController@ezsearchlilopreview']);
		Route::get('ezwatch/preview/archive', ['as' => 'ez_preview_archive', 'uses' => 'TimeKeepingController@archivepreview']);
		Route::get('ezwatch/preview/transaction', ['as' => 'ez_preview_transaction', 'uses' => 'TimeKeepingController@transactionpreview']);
	});

	# Network Utilization
	Route::group(['prefix' => 'network-utilization'], function(){
		Route::get('/', ['as' => '/', 'uses' => 'NetworkUtilizationController@index']);
		Route::get('upload', ['as' => 'upload', 'uses' => 'NetworkUtilizationController@upload']);
	});

	# Seat Plan Project
	Route::group(['prefix' => 'seatplan'], function(){
		Route::get('control', ['as' => 'seatplan_control', 'uses' => 'SeatPlanController@index']);
		Route::get('bu', ['as' => 'seatplan_bu', 'uses' => 'SeatPlanController@index']);
		Route::get('team', ['as' => 'seatplan_team', 'uses' => 'SeatPlanController@index']);
	});
	
	#	Agent Metrics
	Route::group(['prefix' => 'metrics'], function(){
		Route::get('metrics/{id?}', ['as' => 'metrics', 'uses' => 'MetricsController@showMetric']);
	});

	#	Agent Release Call Log
	Route::group(['prefix' => 'release-call-log'], function(){
		Route::get('/', ['as' => 'release_call_log', 'uses' => 'ReleaseCallLogController@index']);
		Route::post('release-call-store', ['as' => 'release_call_store', 'uses' => 'ReleaseCallLogController@store']);
		Route::get('reports-CMS', ['as' => 'reports_CMS', 'uses' => 'ReleaseCallLogController@rptCMS']);
		Route::get('download-CMS', ['as' => 'download_CMS', 'uses' => 'ReleaseCallLogController@dlcms']);
		Route::get('reports-supervisor', ['as' => 'reports_supervisor', 'uses' => 'ReleaseCallLogController@rptsupervisor']);
		Route::get('generate-report', ['as' => 'generate_report', 'uses' => 'ReleaseCallLogController@generatereport']);
		Route::get('download-supervisor', ['as' => 'download_supervisor', 'uses' => 'ReleaseCallLogController@dlsupervisor']);
	});
	
	# Activity Log
	Route::group(['prefix' => 'activity-logger'], function(){
		Route::get('/', ['as' => 'supervisor_logger', 'uses' => 'SupervisorActivityLoggerController@index']);
	});

	# Admin Configuration
	Route::group(['prefix' => 'cpanel'], function(){
		# main cpanel
		Route::get('/', ['as' => 'cpanel_index', 'uses' => 'ConfigurationsController@index']);
		#	Users
		Route::group(['prefix' => 'users'], function(){
			Route::get('/', ['as' => 'users_index', 'uses' => 'UsersController@index']);
			Route::get('/edit/{ntlogin}', ['as' => 'users_edit', 'uses' => 'UsersController@edit']);
			Route::post('/update/{ntlogin}', ['as' => 'users_update', 'uses' => 'UsersController@update']);
			Route::get('/search', ['as' => 'users_search_json', 'uses' => 'UsersController@search_json']);
			#	Special Permission
			Route::group(['prefix' => 'special'], function(){
				Route::get('', ['as' => 'users_special', 'uses' => 'UsersSpecialPermissionController@index']);
				Route::get('add', ['as' => 'users_special_add', 'uses' => 'UsersSpecialPermissionController@add']);
				Route::post('store', ['as' => 'users_special_store', 'uses' => 'UsersSpecialPermissionController@store']);
				Route::get('destroy/{ntlogin}', ['as' => 'users_special_destroy', 'uses' => 'UsersSpecialPermissionController@destroy']);
			});
			#	Admin
			Route::group(['prefix' => 'admin'], function(){
				Route::get('', ['as' => 'users_admin', 'uses' => 'UsersAdminController@index']);
				Route::get('add', ['as' => 'users_admin_add', 'uses' => 'UsersAdminController@add']);
				Route::post('store', ['as' => 'users_admin_store', 'uses' => 'UsersAdminController@store']);
				Route::get('destroy/{ntlogin}', ['as' => 'users_admin_destroy', 'uses' => 'UsersAdminController@destroy']);
			});
			# Conversation
			Route::group(['prefix' => 'conversation'], function(){
				Route::get('/', ['as' => 'conversation_index', 'uses' => 'UserConversationController@index']);
				Route::get('create', ['as' => 'conversation_create', 'uses' => 'UserConversationController@create']);
				Route::post('store', ['as' => 'conversation_store', 'uses' => 'UserConversationController@store']);
				Route::get('destroy/{ntlogin}', ['as' => 'conversation_destroy', 'uses' => 'UserConversationController@destroy']);
			});
			# Distribution
			Route::group(['prefix' => 'distribution'], function(){
				Route::get('/', ['as' => 'user_distribution_index', 'uses' => 'UserDistributionController@index']);
				Route::get('create', ['as' => 'user_distribution_create', 'uses' => 'UserDistributionController@create']);
				Route::post('store', ['as' => 'user_distribution_store', 'uses' => 'UserDistributionController@store']);
				Route::get('destroy/{ntlogin}/{distribution_id}', ['as' => 'user_distribution_destroy', 'uses' => 'UserDistributionController@destroy']);
			});
			# Announcement
			Route::group(['prefix' => 'announcement'], function(){
				Route::get('/', ['as' => 'announcement_index', 'uses' => 'UserAnnouncementController@index']);
				Route::get('create', ['as' => 'announcement_create', 'uses' => 'UserAnnouncementController@create']);
				Route::post('store', ['as' => 'announcement_store', 'uses' => 'UserAnnouncementController@store']);
				Route::get('destroy/{ntlogin}', ['as' => 'announcement_destroy', 'uses' => 'UserAnnouncementController@destroy']);
				Route::get('show', ['as' => 'announcement_show', 'uses' => 'UserAnnouncementController@show']);
				Route::get('update/{id}/{active}', ['as' => 'announcement_update', 'uses' => 'UserAnnouncementController@update']);
			});
			# Roster
			Route::group(['prefix' => 'roster'], function(){
				Route::get('/', ['as' => 'roster_index', 'uses' => 'RosterController@show']);
			});
		});

		# Uploads
		Route::group(['prefix' => 'uploads'], function(){
			# File Uploads
			Route::get('file-uploads', ['as' => 'file_uploads', 'uses' => 'FileUploadsController@index']);
			Route::post('file-uploads-store', ['as' => 'file_uploads_store', 'uses' => 'FileUploadsController@store']);
			Route::get('file-uploads-delete/{id}', ['as' => 'file_uploads_delete', 'uses' => 'FileUploadsController@delete']);
			# Agent Break Schedule
			Route::get('upload-agent-break-schedules', ['as' => 'upload_agent_break_schedules', 'uses' => 'AgentBreakSchedulesController@index']);
			Route::post('upload-agent-break-schedules-store', ['as' => 'upload_agent_schedules_break_store', 'uses' => 'AgentBreakSchedulesController@store']);
			# Call Center Roster
			Route::get('upload-roster', ['as' => 'upload_roster', 'uses' => 'RosterController@index']);
			Route::post('upload-roster-store', ['as' => 'upload_roster_store', 'uses' => 'RosterController@store']);
		});

		# distributions
		Route::group(['prefix' => 'distribution'], function(){
			Route::get('/', ['as' => 'distribution_index', 'uses' => 'DistributionsController@index']);
			Route::get('create', ['as' => 'distribution_create', 'uses' => 'DistributionsController@create']);
			Route::get('edit/{id}', ['as' => 'distribution_edit', 'uses' => 'DistributionsController@edit']);
			Route::post('store', ['as' => 'distribution_store', 'uses' => 'DistributionsController@store']);
			Route::post('update', ['as' => 'distribution_update', 'uses' => 'DistributionsController@update']);

		});

		# Configuration
		Route::group(['prefix' => 'cpanel', 'middleware' => 'auth'], function(){
			# main cpanel
			Route::get('/', ['as' => 'cpanel_index', 'uses' => 'ConfigurationsController@index']);

			# distributions
			Route::group(['prefix' => 'distribution'], function(){
				Route::get('/', ['as' => 'distribution_index', 'uses' => 'DistributionsController@index']);
				Route::get('create', ['as' => 'distribution_create', 'uses' => 'DistributionsController@create']);
				Route::get('edit/{id}', ['as' => 'distribution_edit', 'uses' => 'DistributionsController@edit']);
				Route::post('store', ['as' => 'distribution_store', 'uses' => 'DistributionsController@store']);
				Route::post('update', ['as' => 'distribution_update', 'uses' => 'DistributionsController@update']);
			});

			# users
			Route::group(['prefix' => 'user'], function(){
				Route::get('/', ['as' => 'user_index', 'uses' => 'UsersController@index']);
				Route::get('create', ['as' => 'user_create', 'uses' => 'UsersController@create']);
				Route::get('edit/{ntlogin}', ['as' => 'user_edit', 'uses' => 'UsersController@edit']);
				Route::post('store', ['as' => 'user_store', 'uses' => 'UsersController@store']);
				Route::post('update', ['as' => 'user_update', 'uses' => 'UsersController@update']);
			});
		});
	});

});
