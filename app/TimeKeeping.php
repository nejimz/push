<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class TimeKeeping extends Model
{
	public function scopeEmployeeList($query, $department)
    {
        return DB::connection('ezpayroll')
					->table('EmployeeMaster')
					->where('OrganizationId','=',$department)
					->where('ActiveFlag','=','1')
					->select('EmpId', DB::raw("[EmpLastName] + ', ' + [EmpFirstName] EmpName"))
					->orderBy('EmpName','asc');
    }

    public function scopeliloResult($query, $department, $start, $end){
    	return DB::connection('ezwatch')
                    ->table('TKTimekeepingTransaction')
                    ->join('EzPayroll.dbo.EmployeeMaster','EzPayroll.dbo.EmployeeMaster.EmpId','=','EzWatcher.dbo.TKTimekeepingTransaction.EmployeeId')
                    ->join('EzPayroll.dbo.SectionMaster','EzPayroll.dbo.EmployeeMaster.OrganizationId','=','EzPayroll.dbo.SectionMaster.SectionId')
                    ->join('EzPayroll.dbo.TerminalMaster','EzWatcher.dbo.TKTimekeepingTransaction.TerminalCode','=','EzPayroll.dbo.TerminalMaster.TerminalId')
                    ->join('EzWatcher.dbo.TKTransactionTypeMaster','EzWatcher.dbo.TKTransactionTypeMaster.TransactionTypeId','=','EzWatcher.dbo.TKTimekeepingTransaction.IdentificationMode')
                    ->where('TransactionId','>','1100000')
                    ->where('TransactionDateTime','>=',$start)
                    ->where('TransactionDateTime','<=',$end)
                    ->where('OrganizationId','=',$department)
                    ->select('TransactionId','EmpId', DB::raw("[EmpLastName] + ', ' + [EmpFirstName] EmpName"), 'EmpCode', DB::raw('CONVERT(VARCHAR(20), [TransactionDateTime], 120) TransactionDateTime'), 'TerminalName', 'SectionName', 'TerminalName', DB::raw('UPPER([TransactionTypeName]) TransactionTypeName'))
                    ->orderBy('TransactionDateTime','asc');

    }
}