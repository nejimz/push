<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('transfer_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_ID', 10);
            $table->string('avaya', 6);
            $table->integer('acct_id')->references('id')->on('call_center_accounts');
            $table->integer('skill_id')->references('id')->on('call_center_skills');
            $table->integer('transaction_id')->references('id')->on('transfer_log_transactions');
            $table->integer('calldetails_id')->references('id')->on('transfer_log_call_details');
            $table->text('transfer_log_notes');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('transfer_log');
    }
}
