<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Distributions;
use App\UsersDistribution;
use App\UsersAdmin;
use Input;
use Auth;

class UserDistributionController extends Controller
{

    public function __construct()
    {
        if(UsersAdmin::where('ntlogin', Auth::user()->ntlogin)->count() == 0)
        {
            abort(404);
        }
    }

    public function index(Request $request)
    {
        //
        $search = '';
        $query = UsersDistribution::leftJoin('users', 'users_distribution.ntlogin', '=', 'users.ntlogin')
                    ->leftJoin('distributions', 'distributions.id', '=', 'users_distribution.distribution_id')
                    ->whereNotNull('users.id');
        
        if($request->has('search'))
        {
            $search = trim($request->get('search'));
            $query = $query->where('name', 'LIKE', "%$search%")->orWhere('distributions.distribution', 'LIKE', "%$search%");
        }

        $users = $query->orderBy('name', 'ASC')
                    ->orderBy('distribution', 'ASC')
                    ->paginate(50);

        $data = compact('users', 'search');
        
        return view('push.cpanel.user_distribution.index', $data);
    }

    public function create()
    {
        //
        $distributions = Distributions::all();
        $data = compact('distributions');

        return view('push.cpanel.user_distribution.add', $data);
    }

    public function store(Request $request)
    {
        //
        $rules = [
                #'ntlogin'=>'required|unique:users_distribution,ntlogin|exists:users,ntlogin,distribution_id',
                'ntlogin'=>'required|exists:users,ntlogin',
                'distribution_id'=>'required'
            ];
        $message = [
                'ntlogin.required'          => 'Ntlogin is required.',
                #'ntlogin.unique'            => 'Ntlogin already exists.',
                'ntlogin.exists'            => 'Ntlogin not exists.',
                'distribution_id.required'  => 'Distribution is required.'
            ];

        $this->validate($request, $rules, $message);
        //
        $admin = new UsersDistribution;
        $admin->ntlogin         = trim($request->input('ntlogin'));
        $admin->distribution_id = $request->input('distribution_id');
        $admin->save();

        $input = Input::except('ntlogin');

        return redirect()->route('user_distribution_create')->withInput($input)->with('success', 'Ntlogin successfully added!');
    }

    public function destroy(Request $request, $ntlogin, $distribution_id)
    {
        UsersDistribution::where('ntlogin', $ntlogin)->where('distribution_id', $distribution_id)->delete();

        return redirect()->route('user_distribution_index', ['search'=>$request->search])->with('success', 'Ntlogin successfully deleted!');
    }
}
