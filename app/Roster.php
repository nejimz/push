<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{
    //
     public $table = 'call_center_roster';
     public $timestamps = false;

     protected $fillable = array('employee_ID', 'organization', 'avaya', 'hire_date', 'supervisor_ID', 'work_pattern', 'tier', 'ac_ID', 'week_ending');
     
     public function scopeRetrieveProfile($query, $empID)
     {

     	return $query->where('employee_ID', $empID);

     }

     public function scopeMyAgents($query, $empID)
     {
     	return $query->where('supervisor_ID', $empID);
     }
}
