<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileEmployeeSeparation extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'separationinfo';

    protected $fillable = [
        'empID', 'SeparationDate', 'reason', 'manager', 'supervisor', 'ac', 'hero'
    ];


    public $timestamps = false;
}
