@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

        <div class="row">
          <div class="large-12 columns">

            <div class="row">
              <div class="large-12 columns">
                <h4><i class="fa fa-user"></i> Personal Information</h4>
              </div>
            </div>

            <div class="panel">

              <div class="row">
                <div class="large-3 columns"><strong class="right">Name</strong></div>
                <div class="large-9 columns">{{ $employee->firstName . ' ' . $employee->middleName . ' ' . $employee->lastName . ' ' . $employee->suffix }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Birth Date</strong></div>
                <div class="large-9 columns">{{ date('F d, Y', strtotime($employee->birthDate)) }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Gender</strong></div>
                <div class="large-9 columns">
                  @if($employee->gender == 1)
                    Female
                  @else
                    Male
                  @endif
                </div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Civil Status</strong></div>
                <div class="large-9 columns">{{ $employee->civil_status_name }}</div>
              </div>
<!-- 
              <div class="row">
                <div class="large-3 columns"><strong class="right">Cellphone No. 1</strong></div>
                <div class="large-9 columns">{{ $employee->contactNumber }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Cellphone No. 2</strong></div>
                <div class="large-9 columns">{{ $employee->contactNumber2 }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Telephone No.</strong></div>
                <div class="large-9 columns">{{ $employee->telNumber }}</div>
              </div>
 -->
              <div class="row">
                <div class="large-3 columns"><strong class="right">Old Address</strong></div>
                <div class="large-9 columns">{{ $employee->oldAddress }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Street</strong></div>
                <div class="large-9 columns">{{ $employee->street }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Subdivision</strong></div>
                <div class="large-9 columns">{{ $employee->subd }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Sitio</strong></div>
                <div class="large-9 columns">{{ $employee->sitio }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Barangay</strong></div>
                <div class="large-9 columns">{{ $employee->brgy }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">City</strong></div>
                <div class="large-9 columns">{{ $employee->city }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Province</strong></div>
                <div class="large-9 columns">{{ $employee->province }}</div>
              </div>

            </div>

            <!--  Employment Information -->

            <div class="row">
              <div class="large-12 columns">
                <h4><i class="fa fa-list-alt"></i> Employment Information</h4>
              </div>
            </div>

            <div class="panel">
            
              <div class="row">
                <div class="large-3 columns"><strong class="right">Employee Number</strong></div>
                <div class="large-9 columns">{{ $employee->empNumber }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Wave Number</strong></div>
                <div class="large-9 columns">{{ $employee->waveNumber }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Branch</strong></div>
                <div class="large-9 columns">{{ $employee->branchName }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Department</strong></div>
                <div class="large-9 columns">{{ $employee->department }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Position</strong></div>
                <div class="large-9 columns">{{ $employee->position }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Date Hired</strong></div>
                <div class="large-9 columns">{{ date('F d, Y', strtotime($employee->empDate)) }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Tax Status</strong></div>
                <div class="large-9 columns">{{ $employee->TaxStatusName }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">TIN</strong></div>
                <div class="large-9 columns">{{ $employee->tin }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">SSS</strong></div>
                <div class="large-9 columns">{{ $employee->sss }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">PHILHEALTH</strong></div>
                <div class="large-9 columns">{{ $employee->philhealth }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">PAG-IBIG/HDMF</strong></div>
                <div class="large-9 columns">{{ $employee->hdmf }}</div>
              </div>

            </div>

            @if(!is_null($roster))
            <!--  Roster Information -->

            <div class="row">
              <div class="large-12 columns">
                <h4><i class="fa fa-list-alt"></i> Roster Information</h4>
              </div>
            </div>

            <div class="panel">
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Organization</strong></div>
                <div class="large-9 columns">{{ $roster->organization }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Avaya Number</strong></div>
                <div class="large-9 columns">{{ $roster->avaya }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Production Date</strong></div>
                <div class="large-9 columns">{{ date('F d, Y', strtotime($roster->hire_date)) }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Supervisor</strong></div>
                <div class="large-9 columns">{{ $roster->supervisor }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Work Pattern</strong></div>
                <div class="large-9 columns">{{ $roster->work_pattern }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Tier</strong></div>
                <div class="large-9 columns">{{ $roster->tier }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Account Coordinator</strong></div>
                <div class="large-9 columns">{{ $roster->ac }}</div>
              </div>
              
              <div class="row">
                <div class="large-3 columns"><strong class="right">Week Ending</strong></div>
                <div class="large-9 columns">{{ date('F d, Y', strtotime($roster->week_ending)) }}</div>
              </div>

            </div>

          @endif

          </div>
        </div><!-- row -->

<!-- Stops -->
@stop