<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOuAlias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ou_alias', function (Blueprint $table) {
            $table->string('ou');
            $table->string('alias');
        });

        DB::table('ou_alias')->insert( [ 'ou' => 'Accounting' , 'alias' => 'PayrollAccounting'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Administration' ,'alias' => 'Executive' ] );
        DB::table('ou_alias')->insert( [ 'ou' => 'CA' , 'alias' => 'CorporateAudit'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'CMSGroup' , 'alias' => 'CMSGroup'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'ERD' , 'alias' => 'ERD'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'HRD' , 'alias' => 'HR'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'IT Systems' , 'alias' => 'ITSystems'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Legacy' , 'alias' => 'Legacy'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Managers' , 'alias' => 'Managers'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'PagePlus' , 'alias' => 'PagePlus'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Quality' , 'alias' => 'Quality'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'RAPID' , 'alias' => 'DMG'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Receptionist' , 'alias' => 'Receptionist'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Recruitment' , 'alias' => 'Recruitment'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'SimpleMobile' , 'alias' => 'SimpleMobile'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'SME' , 'alias' => 'SME'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'StraightTalk' , 'alias' => 'StraightTalk'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Supervisors' , 'alias' => 'OpsSupervisors'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'SupGroup' , 'alias' => 'SupGroup'] );
        DB::table('ou_alias')->insert( [ 'ou' => 'Training' , 'alias' => 'Training'] );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('ou_alias');
    }
}
