<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupsZeroTolerancePolicy extends Model
{
    //
     public $table = 'popups_zero_tolerance_policy';
     public $timestamps = false;
}
