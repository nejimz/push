@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
<?php

	$to = '';
	$group = '';
	$to_hidden = '';
	$recipients = App\ConversationMembers::recipients($conversation_id,Auth::user()->ntlogin)
				->orderBy('group')
				->get();
	foreach ($recipients as $user)
	{

		if(is_null($user->name))
		{
			if($user->group == "")
			{
				$to .= '<span class="label" title="'.$user->ntlogin.'">'.$user->ntlogin.'</span>; ';
			}
		}
		elseif($user->group != "")
		{
			$user->group." => ".$user->ntlogin."</br>";
			if($user->group != $group)
			{
				$group = $user->group;
				$groupMembers = App\Distributions::leftJoin('users_distribution', 'users_distribution.distribution_id', '=' ,'distributions.id')
					    				->leftJoin('users', 'users.ntlogin', '=' ,'users_distribution.ntlogin')    				
					    				->where('distributions.distribution', '=', $group)
					    				->select(['users.name'])
					    				->orderBy('name', 'asc')
					    				->get();
				$grpMember = "";
				$grpMemberCnt = 1;
				foreach ($groupMembers as $groupMember) {
					if($groupMember->name != "")
					{
						$grpMember .= (count($groupMembers)==$grpMemberCnt) ? $groupMember->name." " : $groupMember->name.", ";
					}
					$grpMemberCnt++;
				}

				$to .= '<span class="label" data-tooltip aria-haspopup="true" data-options="show_on:large" title="'.$grpMember.'">'.$group.'</span> ';
			}
		}else{
			$to .= '<span class="label" title="'.$user->ntlogin.'">'.$user->name.'</span> ';
		}
	}
	$to_hidden = substr($to_hidden, 0, -1);
	$recipients_reply = json_encode($recipients);

	#echo $to_hidden;
	#print_r($recipients_reply);exit;
?>

     	<form action="{{ route('send_reply_message') }}" id="message-send" method="POST">
			<div class="row" >
	        	<div class="large-6 columns">
	        		<h3>Reply</h3>
	        	</div>
	        	<div class="large-6 columns">
				    <button type="submit" href="#" class="button small right">Send</button>
				    <a href="{{ route('inbox') }}" class="button small secondary right close-modal-button">Cancel</a>
	        	</div>
	        </div><!-- row  # -->

			@if($errors->has())
			<div data-alert class="alert-box warning">
				@foreach($errors->all() as $error)
					{{ $error }}<br />
				@endforeach
				<a href="#" class="close">&times;</a>
	        </div>
	        @elseif(Session::has('messageSuccess'))
			<div data-alert class="alert-box success">
				{{ Session::get('messageSuccess') }}
				<a href="#" class="close">&times;</a>
	        </div>
	       	@endif
	       	
	       	<div class="panel secondary" id="previous-recepients">
				{!! $to !!}
			</div>

			<input name="message_reply_to" id="message-to" type="text" placeholder="To">
       		<input type="hidden" name="recipients" id="recipients" value="{{ $to_hidden }}" />
       		<input type="hidden" name="conversation_id" id="conversation_id" value="{{ $conversation_id }}" />
       		<input type="hidden" name="conversation_member_id" id="conversation_member_id" value="{{ $conversation_member_id }}" />
			<input value="RE: {{ $lastMessage->subject }}" name="message-subject" id="message-subject" type="text" placeholder="Subject" readonly="readonly">
		    <textarea name="message_body" id="messageBody" class="ckeditor" placeholder="Message" rows='15'>{{ old('message_body') }}</textarea>

	       	<input type="hidden" name="conversation_id" id="conversation_id" value="{{ $conversation_id }}" />
		    {!! csrf_field() !!}
		    
		</form>
		
		<input type="hidden" name="recipients_reply" id="recipients_reply" value="{{ $recipients_reply }}" />
		<input type="hidden" id="user_ldap_search" name="user_ldap_search" value="{{ route('user_ldap_search') }}">
	    	@foreach($my_message as $row)
		<?php
   				$recipients = App\ConversationMembers::recipients($conversation_id,$row->ntlogin)
		            					->orderBy('group')
		            					->get();
				$to = '';
				
				foreach ($recipients as $user)
				{

					if(is_null($user->name))
					{
						if($user->group == "")
						{
							$to .= '<u title="'.$user->ntlogin.'">'.$user->ntlogin.'</u>; ';
						}
					}
					elseif($user->group != "")
					{
						$user->group." => ".$user->ntlogin."</br>";
						if($user->group != $group)
						{
							$group = $user->group;
							$groupMembers = App\Distributions::leftJoin('users_distribution', 'users_distribution.distribution_id', '=' ,'distributions.id')
								    				->leftJoin('users', 'users.ntlogin', '=' ,'users_distribution.ntlogin')    				
								    				->where('distributions.distribution', '=', $group)
								    				->select(['users.name'])
								    				->orderBy('name', 'asc')
								    				->get();
							$grpMember = "";
							$grpMemberCnt = 1;
							foreach ($groupMembers as $groupMember) {
								if($groupMember->name != "")
								{
									$grpMember .= (count($groupMembers)==$grpMemberCnt) ? $groupMember->name." " : $groupMember->name.", ";
								}
								$grpMemberCnt++;
							}

							$to .= '<u data-tooltip aria-haspopup="true" data-options="show_on:large" title="'.$grpMember.'">'.$group.'</u>; ';
						}
					}else{
						$to .= '<u title="'.$user->ntlogin.'">'.$user->name.'</u>; ';
					}
				}
				$to = substr($to, 0, -1);
		?>
			    <div class="panel secondary">
			    	<div class="row">
				    	<div class="large-12 columns">
				    		<strong>Sent: </strong>{{ date('D, m/d/Y h:i:s', strtotime($row->created_at)) }}<br />
				    		<strong>From: </strong> 
							@if(is_null($row->name))
								<u>{{ $row->ntlogin }}</u>
							@else
								<u>{{ $row->name }}</u>
							@endif
				    		<br />
				    		<strong>To: </strong>{!! $to !!}
				    	</div>
				    	<div class="large-12 columns">
				    		<strong>Subject: </strong>{!! $row->subject !!}
				    	</div>
				    	<div class="large-12 columns">
							<br />
				    		{!! $row->message !!}
				    	</div>
			    	</div>
			    </div>
	    	@endforeach
<!-- Stops -->
@stop