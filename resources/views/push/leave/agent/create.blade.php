@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

  <div class="row">
    <div class="large-12 columns">
      <h4><i class="fa fa-sign-in"></i> My Leave</h4>
    </div>
  </div><!-- row -->

  <form action="{{ $action }}" method="post">
    @include('push.messages.error_message_list')
    @include('push.messages.success_message')
    <div class="row">

      <div class="large-6 columns">
          <div class="row">
            <div class="large-12 columns">
              <label for="leave_type" class="">Type of Leave:</label>
              <select name="leave_type" id="leave_type">
                <option value="">-- Select --</option>
                @foreach($leave_types as $row)
                  <option value="{{ $row }}">{{ $row }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns">
              <label for="reason" class="">Reason:</label>
              <textarea rows="5" name="reason" id="reason">{{ $reason }}</textarea>
            </div>
          </div>

          <div class="row">
            <div class="large-9 columns">
              <button type="submit" class="button">Submit</button>
              <a class="button secondary" href="{{ route('leave_agent_index') }}">Back</a>
            </div>
            <div class="large-3 columns">&nbsp;</div>
          </div>
      </div><!-- large-6 columns -->

      <div class="large-6 columns">
        <label for="leave_date" class="">Date (<i>EST</i>):</label>
        <input type="hidden" name="leave_agent_add_date_url" id="leave_agent_add_date_url" value="{{ route('leave_agent_add_date') }}" />
        <input type="hidden" name="leave_agent_show_dates_url" id="leave_agent_show_dates_url" value="{{ route('leave_agent_show_dates') }}" />
        <input type="text" name="leave_date" id="leave_date" placeholder="Date" data-date-format="%Y-%m-%d" data-date />
        <input type="hidden" name="click_count" id="click_count" value="0" />
        {!! csrf_field() !!}
        <br />
        <table class="leave_dates" width="100%">
          <thead>
            <tr>
              <td width="10%"></td>
              <td width="90%">Date/s</td>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div><!-- large-6 columns -->

    </div><!-- row -->
  </form>
<script type="text/javascript">
<?php

echo '$(\'#leave_type\').val(\'' . $leave_type . '\');';

if(count($leave_dates) - 1 > 0)
{
  echo '$(\'#click_count\').val(\'' . (count($leave_dates) - 1) . '\');';

  $count = 0;
  foreach ($leave_dates as $date)
  {
    $button = '<a href="#" onclick="remove_date(\\\'row_' . $count . '\\\')"><i class="fa fa-times fa-lg"></i></a>';
    $input  = '<input type="hidden" class="leave_dates_list" name="leave_dates[' . $count . ']"  value="' . $date . '" />';
    $row    = '<tr class="row_' . $count . '"><td>' . $button . '</td><td>' . $date . '' . $input . '</td></tr>';

    echo '$(\'table.leave_dates tbody\').append(\'' . $row . '\');';

    $count++;
  }
}
/*if(Session::has('user.leave'))
{
  echo 'leave_dates();';
}*/
?>
var xhr_leave_agent_add_date = null;

$(function(){
  $('.date-picker').click(function(e){
    var date_count = parseInt($('#click_count').val());
    var add_cond = true;
    var date = $('.calendar .selector ,value').text();
    var input_count = $('.leave_dates_list').size();
    var button = '<a href="#" onclick="remove_date(\'row_' + date_count + '\')"><i class="fa fa-times fa-lg"></i></a>';
    var date_now = new Date('<?php echo date('Y-m-d'); ?>');
    var selected_date = new Date(date);

    //console.log(selected_date.getTime() >= date_now.getTime());
    //console.log(date_now.getTime() >= selected_date.getTime());
    //console.log(date_now.getTime() + ' ' + selected_date.getTime());

    if(input_count != 0)
    {
      $('.leave_dates_list').each(function(){
        //console.log($(this).val() + ' = ' + date);
        if($(this).val() == date)
        {
          input_count++;
          add_cond = false;
          return false;
        }
      });
    }

    var input = '<input type="hidden" class="leave_dates_list" name="leave_dates[' + date_count + ']"  value="' + date + '" />';
    var row = '<tr class="row_' + date_count + '"><td>' + button + '</td><td>' + date + '' + input + '</td></tr>';

    if(add_cond)
    {
      $('table.leave_dates tbody').append(row);
    }
    
    $('#click_count').val((date_count+1));
    
    e.preventDefault();
    //console.log(date);
    /*xhr_leave_agent_add_date = $.ajax({
        type : 'get',
        url : $("#leave_agent_add_date_url").val(),
        data : 'date=' + date,
        cache : false,
        dataType : "script",
        beforeSend: function(xhr){
            if (xhr_leave_agent_add_date != null)
            {
                xhr_leave_agent_add_date.abort();
            }
        }
    }).done(function(data){
      //
    }).fail(function(jqXHR, textStatus){
      //console.log('Request failed: ' + textStatus);
    });*/
  });
});

var xhr_leave_agent_show_dates = null;
function leave_dates()
{
  xhr_leave_agent_show_dates = $.ajax({
      type : 'get',
      url : $("#leave_agent_show_dates_url").val(),
      cache : false,
      dataType : "json",
      beforeSend: function(xhr){
          if (xhr_leave_agent_show_dates != null)
          {
              xhr_leave_agent_show_dates.abort();
          }
      }
  }).done(function(data){
    //
    console.log(data);
  }).fail(function(jqXHR, textStatus){
    //console.log('Request failed: ' + textStatus);
  });
}

function remove_date(row)
{
  $('.' + row).remove();
  return false;
}
</script>
<!-- Stops -->
@stop