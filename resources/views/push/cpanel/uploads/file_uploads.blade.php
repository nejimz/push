@extends('push.layout.master')
@section('container')
@parent
<!-- Starts Container -->
        
        <!-- Container Title -->
        <div class="row" >
          <div class="medium-12 columns">
            <h4>{{ $title or '' }}</h4>
          </div>
        </div>
        <!-- Container Title -->

        <!-- Container Errors -->
        <div class="row">
          <div class="large-12 columns">
          @if($errors->has())
            <div data-alert class="alert-box warning">
              @foreach($errors->all() as $error)
                  {{ $error }}<br />
              @endforeach
              <a href="#" class="close">&times;</a>
            </div>
          @elseif(Session::has('fileUploadSuccess'))
            <div data-alert class="alert-box success">
                  {{ Session::get('fileUploadSuccess') }}
              <a href="#" class="close">&times;</a>
            </div>
          @elseif(Session::has('fileUploadSuccessDelete'))
            <div data-alert class="alert-box success">
                  {{ Session::get('fileUploadSuccessDelete') }}
              <a href="#" class="close">&times;</a>
            </div>
          @endif
          </div>
        </div>
         <!-- Container Errors -->

        <!-- Container Content -->
        <form action="{{ route('file_uploads_store') }}" method="post" enctype="multipart/form-data">

          <div class="row">
            <div class="large-12 columns">
              <div class="row">
                <div class="large-2 columns">
                  <label for="file_name" class="right inline">File Name</label>
                </div>
                <div class="large-8 columns left">
                  <input type="text" id="file_name" name="file_name" value="{{ old('file_name') }}" />
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns">
              <div class="row">
                <div class="large-2 columns">
                  <label for="file_type" class="right inline">Type</label>
                </div>
                <div class="large-8 columns left">
                  <select id="file_type" name="file_type">
                    <option value="">-- Select --</option>
                    {{ old('file_type') }}
                    @foreach($types as $type)
                    <?php $selected = (old('file_type') == $type->id)? 'selected="selected"' : ''; ?>
                    <option value="{{ $type->id }}" {{ $selected }}>{{ $type->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns">
              <div class="row">
                <div class="large-2 columns">
                  <label for="CompanyName" class="right inline">File</label>
                </div>
                <div class="large-8 columns left">
                  <input id="uploadFile" class="left" placeholder="Choose File" disabled="disabled" />
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="large-2 columns">&nbsp;</div>
            <div class="large-8 columns left">
              <div class="button file-upload left" id="file-upload">
                Browse <input type="file" id="file" name="file" />
              </div>

              <button type="submit" class="button success left">Save</button>
              {!! csrf_field() !!}
            </div>
          </div>

        </form>

        <div class="row">    
          <div class="large-12 columns">      
            <div class="pagination-centered">
              {!! str_replace('/?', '?', $files->render()) !!}
            </div>

            <table width="100%">
              <thead>
                <th width="10%">ID</th>
                <th width="30%">Type</th>
                <th width="55%">File Name</th>
                <th width="5%"></th>
              </thead>
              <tbody>
                @foreach($files as $file)
                  <tr>
                    <td>{{ $file->id }}</td>
                    <td>{{ $file->type->name }}</td>
                    <td><a href="{{ secure_asset($file->file_location) }}" target="_blank">{{ $file->file_name }}</a></td>
                    <td><a href="{{ route('file_uploads_delete', [$file->id]) }}"><span class="fa fa-times"></span></a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>

            <div class="pagination-centered">
              {!! str_replace('/?', '?', $files->render()) !!}
            </div>
          </div>
        </div>
        <!-- Container Content -->
        <script type="text/javascript">
          document.getElementById("file").onchange = function () {
              document.getElementById("uploadFile").value = this.value;
          };
        </script>
<!-- Stops -->
@stop