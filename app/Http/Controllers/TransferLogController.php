<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use App\Http\Controllers\Controller;

use DB;
use Auth;

use App\Roster;
use App\Accounts;
use App\Skills;
use App\TransferLogTransactions;
use App\TransferLogCallDetails;
use App\TransferLog;

class TransferLogController extends Controller
{
    public function index(Request $request)
    {
        //MAIN PAGE
        $employee_number = Auth::user()->employee_number;
        $agentData = Roster::RetrieveProfile($employee_number)->orderBy('week_ending', 'DESC')->first();
        if(is_null($agentData))
        {
           $data = [
                    'title'=>"Transfer Log",
                    'avaya'=>"",
                    'account'=>"",
                    'accountId'=>"",
                    'skill'=>"",
                    'skillId'=>"",
                    'transaction'=>""
            ];
        }
        else 
        {
            $skillId = Skills::where('skill', '=', $agentData->organization)->first();
            if(is_null($skillId))
            {
                $data = [
                    'title'=>"Transfer Log",
                    'avaya'=>$agentData->avaya,
                    'account'=>[["id" => "0", "name"=>"Select Account"],["id" => "5", "name"=>"Tracfone Wireless"],["id" => "6", "name"=>"SafeLink"]],
                    'skill'=>$agentData->organization,
                    'skillId'=>"",
                    'transaction'=>[["id" => "0", "transaction"=>"Please select your Account"]]
                ];
            }
            else {
                $transactions = $skillId->transactions()->get();
                $data = [
                    'title'=>"Transfer Log",
                    'avaya'=>$agentData->avaya,
                    'account'=>$skillId->account->account,
                    'accountId'=>$skillId->account->id,
                    'skill'=>$agentData->organization,
                    'skillId'=>$skillId->id,
                    'transaction'=>$transactions
                ];
            }
        }

        //LOAD ACCOUNT FOR TF/SL BAC
        if($request->acctID){
            $skillCollection = Skills::where('account_id', '=', $request->acctID)->get();
            $data['skill'] = [["id" => "0", "skill"=>"Select your Skill"]];
            foreach ($skillCollection as $skills) {
                array_push($data['skill'], $skills);
            }
            $data['transaction'] = [["id" => "0", "transaction"=>"Please select your Skill"]];
        }

        //LOAD SKILL FOR TF/SL BAC
        if($request->skillID){
            $transactionCollection = TransferLogTransactions::where('skill_id', "=", $request->skillID)->get();
            $data['transaction'] = [["id" => "0", "transaction"=>"-- SELECT --"]];
            foreach ($transactionCollection as $transactions) {
                array_push($data['transaction'], $transactions);
            }
        }
        $data['chosenAcct'] = $request->acctID;
        $data['chosenSkill'] = $request->skillID;
        
        return view('push.transfer-log.transfer_log', $data);
    }


    public function callsreceiveddropdown(Request $request)
    {
       //LOAD DROPDOWN FOR SELECTED TRANSACTION
        $options = '<option value="0">-- SELECT --</option>';
        $calls = TransferLogCallDetails::where('transaction_id', '=', $request['transID'])->get();
        foreach ($calls as $callDets) {
            $options .= '<option value="'.$callDets['id'].'">'.$callDets['call_details'].'</option>';
        }

        return $options;
    }

    public function extdept(Request $request)
    {
       //LOAD EXTENSTION NUMBER AND DEPARTMENT FOR SELECTED CALL RCVD
        $calls = TransferLogCallDetails::where('id', '=', $request['callId'])->first();
        $extensionDepartment = $calls->department." - ".$calls->extension;

        return $extensionDepartment;
    }

    public function store(Requests\TransferLogStore $request)
    {
       //SAVE TO DATABASE
        $employee_number = Auth::user()->employee_number;
        $tflog = new TransferLog;
        $tflog->employee_ID = $employee_number;
        $tflog->avaya = $request->input('avayainput');
        if(is_null($request->input('accountID'))){
            $tflog->acct_id = $request->input('accountinput');
        } else {
            $tflog->acct_id = $request->input('accountID');
        }
        if(is_null($request->input('skillId'))){
            $tflog->skill_id = $request->input('agentskillinput');
        } else {
            $tflog->skill_id = $request->input('skillId');
        }
        $tflog->transaction_id = $request->input('transactionType');
        $tflog->calldetails_id = $request->input('callsreceived');
        $tflog->transfer_log_notes = $request->input('notesTransferLog');
        $tflog->save();
        return redirect()->route('transfer_log')->with('success', 'Successfully logged.');
    }

    public function show()
    {
        //DOWNLOAD REPORT
        $data = [
            'title'=>"Transfer Log Reports"
        ];
        return view('push.transfer-log.reports', $data);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
