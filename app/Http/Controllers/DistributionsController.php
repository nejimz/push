<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Distributions;

class DistributionsController extends Controller
{
	public function index(){
		$distributions = Distributions::orderBy('distribution','asc')->paginate(15);
		$data = [ 'distributions' => $distributions ];
		return view('push.cpanel.distributions.index', $data);
	}

	public function edit($id)
	{
		$info = Distributions::distributionInformation($id);
		if(is_null($info))
		{
			abort(404);
		}	
		else
		{
			$members = Distributions::distributionMembers($id);
			$data = [	'id' => $id, 
						'distribution_info' => $info,
						'distribution_members' => $members
					 ];
			return view('push.cpanel.distributions.edit', $data);
		}
		
	}
}