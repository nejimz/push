@if (Session::has('success'))
    <div class="alert-box success ">
        {{ Session::get('success') }}
    </div>
@endif