<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OusConversation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ous_conversation', function (Blueprint $table) {
            $table->string('ou')->unique();
        });

        DB::table('ous_conversation')->insert( [ 'ou' => 'Accounting' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'Administration' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'CA' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'CMSGroup' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'HRD' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'IT Systems' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'Managers' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'Quality' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'RAPID' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'Receptionist' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'Recruitment' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'Supervisors' ] );
        DB::table('ous_conversation')->insert( [ 'ou' => 'Training' ] );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ous_conversation');
    }
}
