<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Masterfile;
use App\MasterfileEmployeeAccount;
use App\MasterfileEmployeeInformation;
use App\MasterfileEmployeeAddress;
use App\MasterfileEmployeeCredentials;
use App\MasterfileEmployeePayDetails;
use App\MasterfileDepartments;
use App\MasterfileEmployeeSeparation;

class MasterfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $search = '';

        $status = ($request->has('status')) ? $request->status : 1;
        $list   = ($status == 1)? [1, 2] : [3, 4, 5, 6, 7, 8];

        $query = Masterfile::leftJoin('employmentinfo', 'employee.empID', '=', 'employmentinfo.empID');

        if($request->has('search'))
        {
            $search = trim($request->input('search'));

            $query->whereIn('empStatus', $list)
                ->where(function($query) use ($search){
                    $query->where('firstName', 'LIKE', '%' . $search . '%')
                            ->orWhere('lastName', 'LIKE', '%' . $search . '%');
                            #->orWhere('middleName', 'LIKE', '%' . $search . '%');
                });
        }
        else
        {
            $query->whereIn('empStatus', $list);
        }

        $employees = $query->orderBy('lastName', 'ASC')->paginate(50);

        $data = compact( 'employees', 'search', 'status' );

        return view('push.masterfile.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $departments = MasterfileDepartments::groupBy('depID')->get();

        $action = route('masterfile_store');

        $first_name     = old('first_name');
        $middle_name    = old('middle_name');
        $last_name      = old('last_name');
        $suffix         = old('suffix');

        $old_address    = old('old_address');
        $street         = old('street');
        $subdivision    = old('subdivision');
        $sitio          = old('sitio');
        $brgy           = old('brgy');
        $city           = old('city');
        $province       = old('province');

        $cellphone_1    = old('cellphone_1');
        $cellphone_2    = old('cellphone_2');
        $telephone      = old('telephone');

        $birth_date     = old('birth_date');
        $civil_status   = old('civil_status');
        $gender         = old('gender');

        $employee_number    = old('employee_number');
        $wave               = old('wave');
        $building           = old('building');
        $branch             = old('branch');
        $hired_date         = old('hired_date');
        $employment_status  = old('employment_status');
        $department         = old('department');
        $position           = old('position');
        $tax_status         = old('tax_status');
        $rehire             = old('rehire');
        $sss                = old('sss');
        $tin                = old('tin');
        $philhealth         = old('philhealth');
        $account_number     = old('account_number');
        $hdmf               = old('hdmf');

        $data = compact( 
                'action', 'departments',
                'first_name', 'middle_name', 'last_name', 'suffix', 
                'old_address', 'street', 'subdivision', 'sitio', 'city', 'brgy', 'province', 
                'cellphone_1', 'cellphone_2', 'telephone', 
                'birth_date', 'civil_status', 'gender', 

                'employee_number', 'wave', 'building', 'branch', 
                'hired_date', 'employment_status', 'department', 'position', 'tax_status', 'rehire', 
                'sss', 'tin', 'philhealth', 
                'account_number', 'hdmf' 
            );

        return view('push.masterfile.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Requests\MasterfileEmployeeRequest $request)
    {
        //
        $employee    = Masterfile::create([
                            'firstName'     => $request->input('first_name'),
                            'middleName'    => $request->input('middle_name'),
                            'lastName'      => $request->input('last_name'),
                            'suffix'        => $request->input('suffix'),
                            'birthDate'     => date('Y-m-d', strtotime($request->input('birth_date'))),
                            'gender'        => $request->input('gender'),
                            'contactNumber' => $request->input('cellphone_1'),
                            'contactNumber2'=> $request->input('cellphone_2'),
                            'telNumber'     => $request->input('telephone')
                        ]);

        $account     = MasterfileEmployeeAccount::create([
                            'empNumber' => $request->input('employee_number'),
                            'building'  => $request->input('building')
                        ]);

        $address     = MasterfileEmployeeAddress::create([
                            'province'      => $request->input('province'),
                            'city'          => $request->input('city'),
                            'brgy'          => $request->input('brgy'),
                            'sitio'         => $request->input('sitio'),
                            'subd'          => $request->input('subdivision'),
                            'street'        => $request->input('street'),
                            'oldAddress'   => $request->input('old_address')
                        ]);

        $information = MasterfileEmployeeInformation::create([
                            'empDate'       => date('Y-m-d', strtotime($request->input('hired_date'))),
                            'department'    => $request->input('department'),
                            'position'      => $request->input('position'),
                            'waveNumber'    => $request->input('waveNumber'),
                            'reHire'        => $request->input('reHire'),
                            'empStatus'     => $request->input('employment_status'),
                            'branchID'      => $request->input('branch')
                        ]);

        $pay_details = MasterfileEmployeePayDetails::create([
                            'civilStatus'  => $request->input('civilStatus'),
                            'taxStatus'     => $request->input('tax_status')
                        ]);

        $credentials = MasterfileEmployeeCredentials::create([
                            'accountNumber' => $request->input('accountNumber'),
                            'sss'           => $request->input('sss'),
                            'tin'           => $request->input('tin'),
                            'philhealth'    => $request->input('philhealth'),
                            'hdmf'          => $request->input('hdmf')
                        ]);

        return redirect()->route('masterfile_create')->with('success', 'Employee Record successfully added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $employee    = Masterfile::where('empID', $id)->first();
        $account     = MasterfileEmployeeAccount::where('empID', $id)->first();
        $address     = MasterfileEmployeeAddress::where('empID', $id)->first();
        $information = MasterfileEmployeeInformation::where('empID', $id)->first();
        $pay_details = MasterfileEmployeePayDetails::where('empID', $id)->first();
        $credentials = MasterfileEmployeeCredentials::where('empID', $id)->first();
        $departments = MasterfileDepartments::groupBy('depID')->get();

        $action     = route('masterfile_update', $id);

        $first_name     = $employee->firstName;
        $middle_name    = $employee->middleName;
        $last_name      = $employee->lastName;
        $suffix         = $employee->suffix;

        $old_address    = $address->oldAddress;
        $street         = $address->street;
        $subdivision    = $address->subd;
        $sitio          = $address->sitio;
        $brgy           = $address->brgy;
        $city           = $address->city;
        $province       = $address->province;

        $cellphone_1    = $employee->contactNumber;
        $cellphone_2    = $employee->contactNumber2;
        $telephone      = $employee->telNumber;

        $birth_date     =  $employee->birthDate;
        $civil_status   = $pay_details->civilStatus;
        $gender         =  $employee->gender;

        $employee_number    = $account->empNumber;
        $wave               = $information->waveNumber;
        $building           = $account->building;
        $branch             = $information->branchID;
        $hired_date         = $information->empDate;
        $employment_status  = $information->empStatus;
        $department         = $information->department;
        $position           = $information->position;
        $tax_status         = $pay_details->taxStatus;
        $rehire             = $information->reHire;
        $sss                = $credentials->sss;
        $tin                = $credentials->tin;
        $philhealth         = $credentials->philhealth;
        $account_number     = $credentials->accountNumber;
        $hdmf               = $credentials->hdmf;

        $data = compact( 
                'action', 'departments',
                'first_name', 'middle_name', 'last_name', 'suffix', 
                'old_address', 'street', 'subdivision', 'sitio', 'city', 'brgy', 'province', 
                'cellphone_1', 'cellphone_2', 'telephone', 
                'birth_date', 'civil_status', 'gender', 

                'employee_number', 'wave', 'building', 'branch', 
                'hired_date', 'employment_status', 'department', 'position', 'tax_status', 'rehire', 
                'sss', 'tin', 'philhealth', 
                'account_number', 'hdmf' 
            );

        return view('push.masterfile.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Requests\MasterfileEmployeeRequest $request, $id)
    {
        //
        $employee    = Masterfile::where('empID', $id)
                        ->update([
                            'firstName'     => $request->input('first_name'),
                            'middleName'    => $request->input('middle_name'),
                            'lastName'      => $request->input('last_name'),
                            'suffix'        => $request->input('suffix'),
                            'birthDate'     => date('Y-m-d', strtotime($request->input('birth_date'))),
                            'gender'        => $request->input('gender'),
                            'contactNumber' => $request->input('cellphone_1'),
                            'contactNumber2'=> $request->input('cellphone_2'),
                            'telNumber'     => $request->input('telephone')
                        ]);

        $account     = MasterfileEmployeeAccount::where('empID', $id)
                        ->update([
                            'empNumber' => $request->input('employee_number'),
                            'building'  => $request->input('building')
                        ]);

        $address     = MasterfileEmployeeAddress::where('empID', $id)
                        ->update([
                            'province'      => $request->input('province'),
                            'city'          => $request->input('city'),
                            'brgy'          => $request->input('brgy'),
                            'sitio'         => $request->input('sitio'),
                            'subd'          => $request->input('subdivision'),
                            'street'        => $request->input('street'),
                            'oldAddress'   => $request->input('old_address')
                        ]);

        $information = MasterfileEmployeeInformation::where('empID', $id)
                        ->update([
                            'empDate'       => date('Y-m-d', strtotime($request->input('hired_date'))),
                            'department'    => $request->input('department'),
                            'position'      => $request->input('position'),
                            'waveNumber'    => $request->input('wave'),
                            'reHire'        => $request->input('reHire'),
                            'empStatus'     => $request->input('employment_status'),
                            'branchID'      => $request->input('branch')
                        ]);

        $pay_details = MasterfileEmployeePayDetails::where('empID', $id)
                        ->update([
                            'civilStatus'  => $request->input('civilStatus'),
                            'taxStatus'     => $request->input('tax_status')
                        ]);

        $credentials = MasterfileEmployeeCredentials::where('empID', $id)
                        ->update([
                            'accountNumber' => $request->input('accountNumber'),
                            'sss'           => $request->input('sss'),
                            'tin'           => $request->input('tin'),
                            'philhealth'    => $request->input('philhealth'),
                            'hdmf'          => $request->input('hdmf')
                        ]);

        return redirect()->route('masterfile_edit', $id)->with('success', 'Employee Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function positions($depID)
    {
        #$depID = $request->input('');

        $rows = MasterfileDepartments::where('depID', $depID)->orderBy('position', 'ASC')->lists('position', 'idDep');

        return response()->json($rows);
    }

    public function profile($id)
    {
        $employee    = Masterfile::where('empID', $id)->first();
        $account     = MasterfileEmployeeAccount::where('empID', $id)->first();
        $address     = MasterfileEmployeeAddress::where('empID', $id)->first();
        $information = MasterfileEmployeeInformation::where('empID', $id)->first();
        $pay_details = MasterfileEmployeePayDetails::where('empID', $id)->first();
        $credentials = MasterfileEmployeeCredentials::where('empID', $id)->first();

        $data = compact('employee', 'account', 'address', 'information', 'pay_details', 'credentials');

        return view('push.masterfile.profile', $data);
    }

    public function separation_create($id)
    {
        $action = route('masterfile_separation_store', $id);
        $employee   = Masterfile::where('empID', $id)->first();
        $heroes     = Masterfile::select('employee.empID', 'firstName', 'lastName')
                    ->leftJoin('employmentinfo', 'employmentinfo.empID', '=', 'employee.empID')
                    ->whereIn('empStatus', [1, 2])
                    ->where('department', 4)
                    ->orderBy('lastName', 'ASC')
                    ->get();

        $Employee        = $employee->firstName . ' ' . $employee->middleName . ' ' . $employee->lastName . ' ' . $employee->suffix;
        $SeparationDate  = old('SeparationDate');
        $separation_type = old('separation_type');
        $reason          = old('reason');
        $manager         = old('manager');
        $supervisor      = old('supervisor');
        $ac              = old('ac');
        $hero            = old('hero');

        $data = compact(
                'action', 'heroes', 'id', 'Employee',
                'SeparationDate', 'separation_type', 'reason', 
                'manager', 'supervisor', 'ac', 'hero'
            );

        return view('push.masterfile.separation', $data);
    }

    public function separation_store(Requests\MasterfileSeparationSaveRequest $request, $id)
    {
        #dd($request->all());
        $inputs = $request->only(['SeparationDate', 'reason', 'manager', 'supervisor', 'ac', 'hero']);
        $inputs['empID'] = $id;
        $inputs['SeparationDate'] = date('Y-m-d', strtotime($request->SeparationDate));

        MasterfileEmployeeSeparation::firstOrCreate( $inputs );
        MasterfileEmployeeInformation::where('empID', $id)->update( ['empStatus' => $request->separation_type] );

        return redirect()->route('masterfile_separation_create', $id)->with('success', 'Employee Record successfully updated!');
    }



    public function separation_edit($id)
    {
        $action = route('masterfile_separation_update', $id);
        $heroes     = Masterfile::select('employee.empID', 'firstName', 'lastName')
                    ->leftJoin('employmentinfo', 'employmentinfo.empID', '=', 'employee.empID')
                    ->whereIn('empStatus', [1, 2])
                    ->where('department', 4)
                    ->orderBy('lastName', 'ASC')
                    ->get();
        $employee   = Masterfile::select('firstName', 'middleName', 'lastName', 'suffix', 'SeparationDate', 'empStatus', 'reason', 'hero', 'manager', 'supervisor', 'ac')
                    ->leftJoin('separationinfo', 'employee.empID', '=', 'separationinfo.empID')
                    ->leftJoin('employmentinfo', 'employee.empID', '=', 'employmentinfo.empID')
                    ->where('employee.empID', $id)
                    ->first();
                    
        $Employee        = $employee->firstName . ' ' . $employee->middleName . ' ' . $employee->lastName . ' ' . $employee->suffix;
        $SeparationDate  = $employee->SeparationDate;
        $separation_type = $employee->empStatus;
        $reason          = $employee->reason;
        $manager         = $employee->manager;
        $supervisor      = $employee->supervisor;
        $ac              = $employee->ac;
        $hero            = $employee->hero;

        $data = compact(
                'action', 'heroes', 'id', 'Employee',
                'SeparationDate', 'separation_type', 'reason', 
                'manager', 'supervisor', 'ac', 'hero'
            );

        return view('push.masterfile.separation', $data);
    }

    public function separation_update(Requests\MasterfileSeparationUpdateRequest $request, $id)
    {
        $inputs = $request->only(['SeparationDate', 'reason', 'manager', 'supervisor', 'ac', 'hero']);
        $inputs['empID'] = $id;
        $inputs['SeparationDate'] = date('Y-m-d', strtotime($request->SeparationDate));

        MasterfileEmployeeSeparation::where('empID', $id)->update( $inputs );
        MasterfileEmployeeInformation::where('empID', $id)->update( ['empStatus' => $request->separation_type] );

        return redirect()->route('masterfile_separation_edit', $id)->with('success', 'Employee Record successfully updated!');
    }
}
