<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopupsCsrNonPerformancePolicy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('popups_csr_non_performance_policy', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ntlogin', 100)->unique()->index();
            $table->datetime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('popups_csr_non_performance_policy');
    }
}
