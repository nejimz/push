@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
    
        <script>
          //CKEDITOR.instances;
          $(function(){
            //CKEDITOR.disableAutoInline = true;
            // CKEDITOR.replace( 'editor1',{
            //   skin : 'office2013',
            //   removePlugins: 'resize,sharedspace',
            //   toolbarGroups: [
            //     { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            //     { "name" : "basicstyles", "groups" : [ "basicstyles" ] },
            //     { "name" : "paragraph", "groups" : [ "list", "align" ] },
            //     { "name" : "styles" },
            //     { "name" : "insert" },
            //   ],
            //   removeButtons: 'Save,Source,Templates,Print,Strike,Subscript,Superscript,Styles,Format,Iframe,Flash,Image,PageBreak'
            // } );
            //$('#editor1').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
            //$('#editable').ckeditor(); // Use CKEDITOR.inline().
          });

        </script>

        <div class="row">
          <div class="large-12 columns">
            <h4><i class="fa fa-pencil-square-o"></i> Announcements</h4>
          </div>
        </div>

        <form action="{{ route('dashboard_store') }}" method="post">
          <div class="row">
            <div class="large-12 columns">
              @include('push.messages.success_message')
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns">
              <textarea class="ckeditor" id="editor1" name="editor1"></textarea>
              <br />
            </div>
          </div>

          <div class="row">
            <div class="large-12 columns">
              {!! csrf_field() !!}
              <button type="submit" class="button success right"><i class="fa fa-check"></i> Post Announcement</button>
            </div>
          </div>

        </form>

        <div class="row">
          @if($count_announcement != 0)
            @foreach($announcements as $announcement)
              @if($announcement->active == 1)
                <div class="large-12 columns">
                  <div class="panel callout">
                    <div class="row">
                      <div class="large-12 columns">
                        <a href="{{ route('announcement_update', [$announcement->id, 0]) }}" class="button button-small success expand">Active</a>
                      </div>
                      <div class="large-12 columns text-right">
                        <span class="postDateFormat">{{ date('F d, Y h:i a', strtotime($announcement->created_at)) }}</span>
                      </div>
                    </div>

                      {!! $announcement->message !!}

                    <div class="large-6 columns">
                      <em style="color: #9c9c9c;">Posted by <a href="#"> {{ $announcement->name }}</a></em>
                    </div>
                  </div><!-- panel columnss -->
                </div><!-- large-12 columns -->
              @else
                <div class="large-12 columns">
                  <div class="panel">
                    <div class="row">
                      <div class="large-12 columns">
                        <a href="{{ route('announcement_update', [$announcement->id, 1]) }}" class="button button-small alert expand">Inactive</a>
                      </div>
                      <div class="large-12 columns text-right">
                        <span class="postDateFormat">{{ date('F d, Y h:i a', strtotime($announcement->created_at)) }}</span>
                      </div>
                    </div>

                      {!! $announcement->message !!}

                    <div class="large-6 columns">
                      <em style="color: #9c9c9c;">Posted by <a href="#"> {{ $announcement->name }}</a></em>
                    </div>
                  </div><!-- panel columnss -->
                </div><!-- large-12 columns -->
              @endif
            @endforeach
          @else
            <div class="large-12 columns">
              <div class="panel callout">
                <div class="row">
                  <center><h6>No Announcements posted yet.</h6></center>
              </div><!-- panel columns -->
            </div><!-- large-12 columns -->
          @endif

        </div><!-- row -->

<!-- Stops -->
@stop