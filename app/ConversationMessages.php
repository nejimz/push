<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationMessages extends Model
{
    //
    public $table = 'conversation_messages';
    public $timestamps = false;
    
    public function messages()
    {
    	return $this->hasMany('App\Conversations', 'id', 'conversation_id');
    }

    public function scopeMyMessages($query,$conversation_id)
    {
        return $query->leftJoin('conversations','conversation_messages.conversation_id','=','conversations.id')
                    ->leftJoin('users','conversation_messages.ntlogin','=','users.ntlogin')
                    ->where('conversation_id', $conversation_id)
                    ->orderBy('created_at', 'DESC');
    }
}
