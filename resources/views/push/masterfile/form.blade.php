@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

  <div class="row">
    <div class="large-12 columns">
      <h4><i class="fa fa-user-plus"></i> Form</h4>
    </div>
  </div><!-- row -->

  <form action="{{ $action }}" method="post">
    @include('push.messages.error_message_list')
    @include('push.messages.success_message')
    <div class="panel">
      <fieldset>
        <legend>Employee Information</legend>

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="first_name">First Name:</label>
                <input type="text" name="first_name" id="first_name" value="{{ $first_name }}" />
              </div>
            </div>
          </div>

          <div class="large-3 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="middle_name">Middle Name:</label>
                <input type="text" name="middle_name" id="middle_name" value="{{ $middle_name }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="last_name">Last Name:</label>
                <input type="text" name="last_name" id="last_name" value="{{ $last_name }}" />
              </div>
            </div>
          </div>

          <div class="large-1 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="suffix">Suffix:</label>
                <input type="text" name="suffix" id="suffix" value="{{ $suffix }}" />
              </div>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
          <div class="large-12 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="old_address">Old Address:</label>
                <input type="text" name="old_address" id="old_address" value="{{ $old_address }}" />
              </div>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="street">Street:</label>
                <input type="text" name="street" id="street" value="{{ $street }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="subdivision">Subdivision:</label>
                <input type="text" name="subdivision" id="subdivision" value="{{ $subdivision }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="sitio">Sitio:</label>
                <input type="text" name="sitio" id="sitio" value="{{ $sitio }}" />
              </div>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="brgy">Brgy:</label>
                <input type="text" name="brgy" id="brgy" value="{{ $brgy }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="city">City:</label>
                <input type="text" name="city" id="city" value="{{ $city }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="province">Province:</label>
                <input type="text" name="province" id="province" value="{{ $province }}" />
              </div>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="cellphone_1">Cellphone 1:</label>
                <input type="text" name="cellphone_1" id="cellphone_1" value="{{ $cellphone_1 }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="cellphone_2">Cellphone 2:</label>
                <input type="text" name="cellphone_2" id="cellphone_2" value="{{ $cellphone_2 }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="telephone">Telephone:</label>
                <input type="text" name="telephone" id="telephone" value="{{ $telephone }}" />
              </div>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="birth_date">Birth Date:</label>
                <input type="text" name="birth_date" id="birth_date" value="{{ $birth_date }}" data-date-format="%m/%d/%Y" data-date />
              </div>
            </div>
          </div>
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="civil_status">Civil Status:</label>
                <select name="civil_status" id="civil_status">
                  <option value=""></option>
                  <option value="1">Single</option>
                  <option value="2">Married</option>
                  <option value="3">Widowed</option>
                  <option value="4">Divorced</option>
                </select>
              </div>
            </div>
          </div>
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="gender">Gender:</label>
                <input type="radio" name="gender" id="male" value="1" /> <label for="male">Male</label>
                <input type="radio" name="gender" id="female" value="0" /> <label for="female">Female</label>
              </div>
            </div>
          </div>
        </div><!-- row -->

      </fieldset>

      <fieldset>
        <legend>Employement Information</legend>
        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="employee_number">Employee Number:</label>
                <input type="text" name="employee_number" id="employee_number" value="{{ $employee_number }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="branch">Wave:</label>
                <input type="text" name="wave" id="wave" value="{{ $wave }}" />
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="branch">Branch:</label>
                <select name="branch" id="branch">
                  <option value=""></option>
                  <option value="1">Bacolod</option>
                  <option value="2">Cebu</option>
                </select>
              </div>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="building">Building:</label>
                <select name="building" id="building">
                  <option value=""></option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select>
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="department">Department:</label>
                <select name="department" id="department">
                  <option value=""></option>
                  @foreach($departments as $row)
                    <option value="{{ $row->depID }}">{{ $row->department }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="position">Position:</label>
                <select name="position" id="position">
                  <option value=""></option>
                </select>
              </div>
            </div>
          </div>

        </div><!-- row -->

        <div class="row">

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="hired_date">Hired Date:</label>
                <input type="text" name="hired_date" id="hired_date" value="{{ $hired_date }}" data-date-format="%m/%d/%Y" data-date />
              </div>
            </div>
          </div>
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="tax_status">Tax Status:</label>
                <select name="tax_status" id="tax_status">
                  <option value=""></option>
                  <option value="1">S</option>
                  <option value="2">S1</option>
                  <option value="3">S2</option>
                  <option value="4">S3</option>
                  <option value="5">S4</option>
                  <option value="6">M</option>
                  <option value="7">M1</option>
                  <option value="8">M2</option>
                  <option value="9">M3</option>
                  <option value="10">M4</option>
                </select>
              </div>
            </div>
          </div>

          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="employment_status">Employment Status:</label>
                <select name="employment_status" id="employment_status">
                  <option value=""></option>
                  <option value="1">Probationary</option>
                  <option value="2">Regular</option>
                  <option value="3">Resignation</option>
                  <option value="4">Termination</option>
                  <option value="5">Verification</option>
                  <option value="6">Not For Rehire</option>
                  <option value="7">Trainee</option>
                  <option value="8">Decease</option>
                </select>
              </div>
            </div>
          </div>

        </div><!-- row -->

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="rehire">Rehire:</label>
                <select name="rehire" id="rehire">
                  <option value=""></option>
                  <option value="1">No</option>
                  <option value="2">Regular</option>
                  <option value="3">Yes</option>
                  <option value="4">Valid Substitute</option>
                  <option value="5">N/A</option>
                </select>
              </div>
            </div>
          </div>
          <div class="large-4 columns">&nbsp;</div>
          <div class="large-4 columns">&nbsp;</div>
        </div><!-- row -->

      </fieldset>

      <fieldset>
        <legend>Employee Credentials</legend>

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="sss">SSS:</label>
                <input type="text" name="sss" id="sss" value="{{ $sss }}" />
              </div>
            </div>
          </div>
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="tin">TIN:</label>
                <input type="text" name="tin" id="tin" value="{{ $tin }}" />
              </div>
            </div>
          </div>
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="philhealth">Philhealth:</label>
                <input type="text" name="philhealth" id="philhealth" value="{{ $philhealth }}" />
              </div>
            </div>
          </div>
        </div><!-- row -->

        <div class="row">
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="account_number">Account Number:</label>
                <input type="text" name="account_number" id="account_number" value="{{ $account_number }}" />
              </div>
            </div>
          </div>
          <div class="large-4 columns">
            <div class="row">
              <div class="large-12 columns">
                <label for="hdmf">HDMF:</label>
                <input type="text" name="hdmf" id="hdmf" value="{{ $hdmf }}" />
              </div>
            </div>
          </div>
          <div class="large-4 columns">&nbsp;</div>
        </div><!-- row -->

      </fieldset>

      <div class="row">
        <div class="large-4 columns">&nbsp;</div>
        <div class="large-1 columns">&nbsp;</div>
        <div class="large-2 columns">
          {!! csrf_field() !!}
          <button class="button alert expand"><strong>Save</strong></button>
        </div>
        <div class="large-1 columns">&nbsp;</div>
        <div class="large-4 columns">&nbsp;</div>
      </div>

    </div>
  </form>
  <input type="hidden" id="position_url" value="{{ route('masterfile_poistions_url') }}">

<script type="text/javascript">

<?php

  if($civil_status != '')
  {
    echo '$(\'#civil_status\').val(\'' . $civil_status . '\');';
  }

  if($gender == 0)
  {
    echo '$(\'#male\').prop(\'checked\', true);';
  }
  elseif($gender == 1)
  {
    echo '$(\'#female\').prop(\'checked\', true);';
  }

  if($tax_status != '')
  {
    echo '$(\'#tax_status\').val(\'' . $tax_status . '\');';
  }
  
  if($employment_status != '')
  {
    echo '$(\'#employment_status\').val(\'' . $employment_status . '\');';
  }
  
  if($building != '')
  {
    echo '$(\'#building\').val(\'' . $building . '\');';
  }
  
  if($branch != '')
  {
    echo '$(\'#branch\').val(\'' . $branch . '\');';
  }
  
  if($department != '')
  {
    echo '$(\'#department\').val(\'' . $department . '\');';
  }
  
  if($position != '')
  {
    echo 'position(\'' . $department . '\', ' . $position . ');';
    #echo '$(\'#position\').val(\'' . $position . '\');';
  }
  
  if($rehire != '')
  {
    echo '$(\'#rehire\').val(\'' . $rehire . '\');';
  }
?>

$(function(){
  $('#department').change(function(e){
    position($(this).val(), null);
    e.preventDefault();
  });
});

var xhr_leave_agent_show_dates = null;
function leave_dates()
{
  xhr_leave_agent_show_dates = $.ajax({
      type : 'get',
      url : $("#leave_agent_show_dates_url").val(),
      cache : false,
      dataType : "json",
      beforeSend: function(xhr){
          if (xhr_leave_agent_show_dates != null)
          {
              xhr_leave_agent_show_dates.abort();
          }
      }
  }).done(function(data){
    //
    console.log(data);
  }).fail(function(jqXHR, textStatus){
    //console.log('Request failed: ' + textStatus);
  });
}

var xhr_position = null;
function position(department, position)
{
  xhr_position = $.ajax({
      type : 'get',
      url : $("#position_url").val() + '/' + department,
      cache : false,
      dataType : "json",
      beforeSend: function(xhr){
          if (xhr_position != null)
          {
              xhr_position.abort();
          }
      }
  }).done(function(data){
    
    var str_position    = '<option value=""></option>';

    $.each(data, function(index, value){

      if(position == index)
      {
        str_position += '<option value="' + index + '" selected="selected">' + value + '</option>';
      }
      else
      {
        str_position += '<option value="' + index + '">' + value + '</option>';
      }
    });

    $('#position').html(str_position);
    
  }).fail(function(jqXHR, textStatus){
    //console.log('Request failed: ' + textStatus);
  });
}
</script>
<!-- Stops -->
@stop