@extends('push.layout.master')

@section('container')
@parent
<!-- Starts Container -->
        
        <!-- Container Title -->
        <div class="row" >
          <div class="medium-12 columns">
            <h4>{{ $title or '' }}</h4>
          </div>
        </div>
        <!-- Container Title -->

        <!-- Container Content -->
        <div class="row">
          <div class="medium-12 columns">
            <table width="100%">
              <thead>
                <!--th width="20%">ID</th-->
                <th width="80%">FIle Name</th>
              </thead>
              <tbody>
              @foreach($files as $file)
                <tr>
                  <!--td>{{ $file->id }}</td-->
                  <td><a href="{{ asset($file->file_location) }}" target="_blank">{{ $file->file_name }}</a></td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <!-- Container Content -->
        
<!-- Stops Container -->
@stop