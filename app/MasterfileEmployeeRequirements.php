<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileEmployeeRequirements extends Model
{
    //
    public $timestamps = false;
}
