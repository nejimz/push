<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MasterfileBranch;
use App\MasterfileDepartments;
use App\MasterfileEmployeeStatus;

class MasterfileEmployeeInformation extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'employmentinfo';
    public $timestamps = false;

    public function getBranchFormatAttribute()
    {
    	$row = MasterfileBranch::where('branchID', $this->branchID)->first();

        return $row->branchName;
    }

    public function getEmployementDateFormatAttribute()
    {
        return date('F d, Y', strtotime($this->empDate));
    }

    public function getEmployeeStatusFormatAttribute()
    {
    	$row = MasterfileEmployeeStatus::where('EmployementStatusID', $this->empStatus)->first();

        return $row->EmploymentStatusName;
    }

    public function getRehireFormatAttribute()
    {
        return ($this->reHire == 1)? 'Yes' : 'No';
    }

    public function getDepartmentFormatAttribute()
    {
    	$row = MasterfileDepartments::where('idDep', $this->position)->first();

        return $row;
    }

}
