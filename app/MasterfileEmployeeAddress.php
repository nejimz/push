<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileEmployeeAddress extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'employeeaddress';
    public $timestamps = false;
}
