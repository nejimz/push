@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
        
        <!-- Container Title -->
        <div class="row" >
          <div class="medium-12 columns">
            <h4><i class="fa fa-calendar-o"></i> Timekeeping - Ezwatch</h4>
          </div>
        </div>
        <!-- Container Title -->

        <!-- Container Content -->

        {{-- Controls --}}
        <div class="row">
          <div class="medium-6 columns">
            {{-- Form --}}
            <form id="ez-form">
              {{-- Department --}}
              <div class="row">
                <div class="medium-2 columns"><label class="inline">Department</label></div>
                <div class="medium-10 columns">
                  <select id="ez-dept" name="ez_dept">
                    <option value='0'>-- Select Department --</option>
                    @foreach($departments as $department)
                      <option value="{{ $department['id'] }}">{{ $department['value'] }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              {{-- Department --}}

              {{-- Name --}}
              <div class="row">
                <div class="medium-2 columns"><label class="inline">Name</label></div>
                <div class="medium-10 columns">
                  <select id="ez-emp" name="ez_emp">
                    <option value='0'>-- Select Name --</option>
                  </select>
                </div>
              </div>
              {{-- Name --}}

              <div class="row">
                {{-- Start --}}
                <div class="medium-2 columns"><label class="inline">Start</label></div>
                <div class="medium-10 columns">
                  <input type="text" id="ez-start" name="ez_start" value="{{ date('Y-m-d') }} 00:00:00" data-date-format="%m/%d/%Y" data-time-format="%H:%M:%S" data-date-time readonly="readonly"/>
                </div>
                {{-- Start --}}
              </div>

              <div class="row">
                {{-- End --}}
                <div class="medium-2 columns"><label class="inline">End</label></div>
                <div class="medium-10 columns">
                   <input type="text" id="ez-end" name="ez_end" value="{{ date('Y-m-d') }} 23:59:59" data-date-format="%m/%d/%Y" data-time-format="%H:%M:%S"  data-date-time readonly="readonly"/>
                </div>
                {{-- End --}}
              </div>

               {{-- Triggers --}}
               <div class="row">
                  <div class="medium-2 columns">&nbsp;</div>
                  <div class="medium-10 columns">
                    <ul class="button-group even-3" id="ez-button">
                      <li><button type="submit" class="button"><i class="fa fa-refresh"></i> Generate</button></li>
                      <li><button type="button" onclick="ezdownload('{{ route('ez_search_lilo_download') }}')" class="button success"><i class="fa fa-download"></i> Download</button></li>
                      <li><button type="reset" class="button alert"><i class="fa fa-undo"></i> Reset</button></li>
                    </ul>
                  </div>
                </div>
               {{-- Triggers --}} <!--  Amo ni sya ahahah-->
            </form>
            {{-- Form --}}
          </div>

          {{-- Image Viewer --}}
          <div class="medium-6 columns">
            <ul class="large-block-grid-2">
              <li><a class="th" href="#"><img src="#" data-src="holder.js/200x200?text=Archive Photo&theme=sky" id="ez-archive" class="ez-photo"></a></li>
              <li><a class="th" href="#"><img src="#" data-src="holder.js/200x200?text=Capture Photo&theme=sky" id="ez-capture" class="ez-photo"></a></li>
            </ul>
          </div>
          {{-- Image Viewer --}}
        </div>

        <input type="hidden" id="ez-employee-url" value="{{ route('ez_search_employee') }}">
        <input type="hidden" id="ez-lilo-url" value="{{ route('ez_search_lilo_generate') }}">
        <input type="hidden" id="ez-preview-url" value="{{ route('ez_search_lilo_preview') }}">
        {{-- Controls --}}
      
        {{-- Table view --}}
        <div class="row">
          <div class="medium-12">
            <table width="100%" id='ez-table'>
              <thead>
                <th>Emp. No.</th>
                <th>Section</th>
                <th>Name</th>
                <th>Attendance Date Time</th>
                <th>Device</th>
                <th>Transaction</th>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
        {{-- Table view --}}

        <!-- Container Content -->
        
<!-- Stops Container -->
@stop

@section('scripts')
  @include('push.scripts.timekeeping_js')
@stop