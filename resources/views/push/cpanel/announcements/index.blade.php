@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
   	<div class="large-12 columns">

   		<h4><i class="fa fa-users"></i> PUSH Announcement</h4>

   		<div class="row">
   			<a href="{{ route('announcement_create') }}" class="button button-small"><i class="fa fa-plus"></i> Add</a>
   		</div>

		@include('push.messages.success_message')

		{!! str_replace('/?', '?', $users->render()) !!}
		<table width="100%">
			<thead>
				<tr>
					<th width="5%"></th>
					<th width="20%">Employee No.</th>
					<th width="20%">NT-Login</th>
					<th width="30%">Name</th>
					<th width="25%">Distribution</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td><a href="{{ route('announcement_destroy', [$user->ntlogin]) }}"><i class="fa fa-times"></i></a></td>
					<td>{{ $user->employee_number }}</td>
					<td>{{ $user->ntlogin }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->ou }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! str_replace('/?', '?', $users->render()) !!}
   	</div>
<!-- Stops -->
@stop