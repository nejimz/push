<!-- My Schedule Popup -->
<div id="myScheduleModal" class="reveal-modal" data-reveal aria-labelledby="mySchedule" aria-hidden="true" role="dialog">
<?php
$date_now = date('Y-m-d H:i:s', strtotime('EST'));
$dst_list = [1, 2, 3, 11, 12];
$week_ending = date('Y-m-d', strtotime('next sunday', strtotime(date('Y-m-d'))));
$schedule = App\AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
            ->where('week_ending', $week_ending)
            ->get();

/*

echo $week_ending_accept;
*/
if(in_array(date('n', strtotime('Y-m-d EST')), $dst_list))
{
  $date_now = date('Y-m-d H:i:s', strtotime($date_now) - 3600);
}
?>
  <table width="100%">
    <caption><h3 class="left">My Schedule</h3></caption>
    <thead>
      <tr>
        <th width="40%">Shift Date</th>
        <th width="20%">Break 1</th>
        <th width="20%">Break 2</th>
        <th width="20%">Break 3</th>
      </tr>
    </thead>
    <tbody>
    @foreach($schedule as $row)
      <tr>
        <td>
          {{ date('F d, Y', strtotime($row->shift_date)) }} 
          @if(is_null($row->break_1_start))
            <span class="alert label">Not Scheduled</span>
          @else
            <span class="label">Scheduled</span>
          @endif
        </td>
          @if(is_null($row->break_1_start))
          <td></td>
          <td></td>
          <td></td>
          @else
          <td>{{ date('H:i', strtotime($row->break_1_start)) . ' - ' . date('H:i', strtotime($row->break_1_end)) }} <i>EST</i></td>
          <td>{{ date('H:i', strtotime($row->break_2_start)) . ' - ' . date('H:i', strtotime($row->break_2_end)) }} <i>EST</i></td>
          <td>{{ date('H:i', strtotime($row->break_3_start)) . ' - ' . date('H:i', strtotime($row->break_3_end)) }} <i>EST</i></td>
          @endif
      </tr>
    @endforeach
    </tbody>
  </table>

  <div data-alert class="alert-box info">
    <strong>Note : </strong> Agents shouldn’t have shift schedules for more than 5 consecutive days. Should the system assign you a week schedule with more than 5 consecutive shift dates, please report this incident to your supervisor.
    <br /> <br /> 
    <em> To acknowledge that you have read and understood and therefore agree to comply to your daily schedule, click "I Accept." </em>
  </div>
    <a href="{{ route('my_week_ending_schedule') }}" class="button success">I Accept</button>
</div>

<script type="text/javascript">
$('#myScheduleModal').foundation('reveal', 'open', {
  animation_speed: 0,
  close_on_background_click: false
});
</script>