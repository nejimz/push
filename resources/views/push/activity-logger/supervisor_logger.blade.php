@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
  <!-- Container Content -->

<div class="row">
  <div class="medium-10 medium-centered columns">
    <form action="#" method="post" id="activity-form">
    <fieldset>

      <legend><h4>{{ $title or '' }}</h4></legend>

      <div class="row">
        <div class="small-2 columns">
            <label class="left inline">Employee ID:</label>
        </div>
        <div class="small-3 columns left">
            <input type="text" id="empid" name="empid" class="small-4 columns" readonly="readonly" value="{{ $emp_id }}" />
        </div>

        <div class="small-2 columns">
            <label class="left inline">Name:</label>
        </div>
        <div class="small-5 columns left">
            <input type="text" id="empname" name="empname" class="small-4 columns" readonly="readonly" value="{{ $employee->firstName . ' ' . $employee->middleName . ' ' . $employee->lastName . ' ' . $employee->suffix }}" />
        </div>
      </div>

      <div class="row">
            <div class="small-2 columns">
                <label class="left inline">Activity :</label>
            </div>
            <div class="small-10 columns">
                  <select>
                    <option value="0">- - S E L E C T - -</option>
                    <option value="1">Client Reports/Feedback</option>
                    <option value="2">QM Uploads</option>
                    <option value="3">Coaching Logs</option>
                    <option value="4">Adherence to Supervisor Break/Lunch Procedure</option>
                    <option value="5">Daily Pre-Shift /Post Shift Huddle</option>
                    <option value="6">CMS Report/Feedback</option>
                    <option value="7">ORI/IAK Compliance</option>
                    <option value="8">3rd and 5th Month Review Compliance</option>
                    <option value="9">CTI Compliance Audit</option>     
                    <option value="10">Agent Performance Feedback</option>
                    <option value="11">Communication and Program Updates</option>
                    <option value="12">Offshore Monitoring Logs</option>
                  </select>
            </div>
      </div>

      <div class="row">
        <div class="small-2 columns">
          <label class="left inline">Notes:</label>
          </div>
          <div class="small-10 columns right">
            <textarea rows="10"></textarea>
        </div>
      </div>

      <div class="row">
        <div class="small-5 small-offset-2 columns">
          <button type="button" id="startTime" class="button round expand success">START</button>
        </div><div class="small-5 columns">
          <button type="button" id="stopTime" class="button round expand secondary">STOP</button>
        </div>
      </div>

        {!! csrf_field() !!}

    </fieldset>
    </form>
  </div>
</div>

<div class="row">
  <div class="large-10 large-offset-1 columns">
  <table width="100%">
    <thead>
      <tr>
        <th width="50%">Activity for October 22, 2015</th>
        <th width="25%" style="text-align: center;">Start Time</th>
        <th width="25%" style="text-align: center;">Stop Time</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Daily Pre-Shift /Post Shift Huddle</td>
        <td style="text-align: center;"><em>07:55 AM</em></td>
        <td style="text-align: center;"><em>08:00 AM</em></td>
      </tr>
      <tr>
        <td>Adherence to Supervisor Break/Lunch Procedure</td>
        <td style="text-align: center;"><em>08:15 AM</em></td>
        <td style="text-align: center;"><em>08:30 AM</em></td>
      </tr>
      <tr>
        <td>Agent Performance Feedback</td>
        <td style="text-align: center;"><em>09:30 AM</em></td>
        <td style="text-align: center;"><em>09:35 AM</em></td>
      </tr>
      <tr>
        <td>Agent Performance Feedback</td>
        <td style="text-align: center;"><em>10:30 AM</em></td>
        <td style="text-align: center;"><em>10:35 AM</em></td>
      </tr>
      <tr>
        <td>Coaching Logs</td>
        <td style="text-align: center;"><em>10:45 AM</em></td>
        <td style="text-align: center;"><em>11:15 AM</em></td>
      </tr>
      <tr>
        <td>Coaching Logs</td>
        <td style="text-align: center;"><em>12:30 PM</em></td>
        <td style="text-align: center;"><em>01:35 PM</em></td>
      </tr>
      <tr>
        <td>Adherence to Supervisor Break/Lunch Procedure</td>
        <td style="text-align: center;"><em>01:35 PM</em></td>
        <td style="text-align: center;"><em>02:30 PM</em></td>
      </tr>
    </tbody>
  </table>
  </div>
</div>
  <!-- Container Content -->
<!-- Stops Container -->
@stop

@section('scripts')
  @include('push.scripts.activity_log_js')
@stop