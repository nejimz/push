<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Users;

class LeaveAgent extends Model
{
    public $table = 'leave_agent';
    public $timestamps = false;

    /**
    Setters
    **/
    public function setLeaveDateAttribute($value)
	{
     	$this->attributes['leave_date'] = date('Y-m-d', strtotime($value));
	}


    /**
    Getters
    **/
    public function getLeaveDateFormatAttribute()
	{
     	return date('D, F d, Y', strtotime($this->leave_date));
	}

    public function getEmployeeNumberAttribute()
	{
		$ntlogin 	= $this->ntlogin;
		$employee 	= Users::where('ntlogin', $ntlogin)->first();

     	return $employee->employee_number;
	}

    public function getEmployeeNameAttribute()
	{
		$ntlogin 	= $this->ntlogin;
		$employee 	= Users::where('ntlogin', $ntlogin)->first();

     	return $employee->name;
	}
}
