@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
	<div class="row">
       	<div class="medium-12 columns">
       		<h4><i class="fa fa-cog"></i> Application Configuration </h4>
       	</div>
    </div><!-- row  # -->
    <div class="row" id="cpanel">
    	<div class="large-12 columns">
    		<!-- Users Accordion -->
			<dl class="accordion" data-accordion>
				<dd class="accordion-navigation">
					<a href="#panel-users" class="accordion-title">
						<label><i class="fa fa-users"></i> Users <span class="fa accordion-icon right"></span></label>
					</a>
					<div id="panel-users" class="content active">
						<div class="row">
							<!-- Column1 -->
							<div class="large-4 columns">
								<a href="{{ route('users_index') }}"><i class="fa fa-users"></i> NT Logins </a>
							</div>
							<!-- Column2 -->
							<div class="large-4 columns">
								<a href="{{ route('users_admin') }}"><i class="fa fa-user"></i> Admins </a>
							</div>
							<!-- Column3 -->
							<div class="large-4 columns">
								<a href="{{ route('users_special') }}"><i class="fa fa-user"></i> Special Permission </a>
							</div>

							<!-- Column4 -->
							<div class="large-4 columns">
								<a href="{{ route('conversation_index') }}"><i class="fa fa-envelope"></i> Conversation </a>
							</div>
							<!-- Column5 -->
							<div class="large-4 columns">
								<a href="{{ route('user_distribution_index') }}"><i class="fa fa-dropbox"></i> Distribution </a>
							</div>
							<!-- Column6 -->
							<div class="large-4 columns">
								<a href="{{ route('announcement_index') }}"><i class="fa fa-microphone"></i> Announcements </a>
							</div>

							<!-- Column7 -->
							<div class="large-4 columns">
								<a href="{{ route('roster_index') }}"><i class="fa fa-users"></i> Roster </a>
							</div>
							<!-- Column8 -->
							<div class="large-4 columns">
								&nbsp;
							</div>
							<!-- Column9 -->
							<div class="large-4 columns">
								&nbsp;
							</div>
						</div>
					</div>
				</dd>
			</dl>
			<!-- Users Accordion -->

			<!-- Distribution Accordion -->
			<dl class="accordion" data-accordion>
				
				<dd class="accordion-navigation">
					<a href="#panel-distribution" class="accordion-title">
						<label><i class="fa fa-dropbox"></i> Organization/Distribution <span class="fa accordion-icon right"></span></label>
					</a>
					<div id="panel-distribution" class="content active">
						<div class="row">
							<!-- Column1 -->
							<div class="large-4 columns">
								<a href="{{ route('distribution_index') }}"><i class="fa fa-group"></i> Distributions </a>						
							</div>
							<!-- Column2 -->
							<div class="large-4 columns">
								<a href="{{ route('distribution_create') }}"><i class="fa fa-plus"></i> Add Distribution </a>
							</div>
							<!-- Column3 -->
							<div class="large-4 columns">
								<a href="{{ route('distribution_edit') }}"><i class="fa fa-pencil"></i> Edit Distribution </a>
							</div>
						</div>
					</div>
				</dd>
			</dl>

			<!-- Upload Accordion -->
			<dl class="accordion" data-accordion>
				
				<dd class="accordion-navigation">
					<a href="#panel-file-uploads" class="accordion-title">
						<label><i class="fa fa-files-o"></i> File Uploads/Announcements <span class="fa accordion-icon right"></span></label>
					</a>
					<div id="panel-file-uploads" class="content active">
						<div class="row">
							<!-- Column1 -->
							<div class="large-4 columns">
								<a href="{{ route('file_uploads') }}"><i class="fa fa-file"></i> PDF's</a>						
							</div>
							<!-- Column2 -->
							<div class="large-4 columns">
								<a href="{{ route('upload_agent_break_schedules') }}"><i class="fa fa-calendar"></i> Agent Break Schedule</a>
							</div>
							<!-- Column3 -->
							<div class="large-4 columns">
								<a href="{{ route('upload_roster') }}"><i class="fa fa-group"></i> Call Center Roster</a>
							</div>

							<!-- Column4 -->
							<div class="large-4 columns">
								<a href="{{ route('announcement_show') }}"><i class="fa fa-group"></i> Announcements</a>
							</div>
							<!-- Column5 -->
							<div class="large-4 columns">
								<a href="{{ route('upload_roster') }}">&nbsp;</a>
							</div>
							<!-- Column6 -->
							<div class="large-4 columns">
								<a href="{{ route('upload_roster') }}">&nbsp;</a>
							</div>
						</div>
					</div>
				</dd>

			</dl>
			<!-- Distribution Accordion -->
       	</div>
    </div>  
<!-- Stops -->
@stop