<aside class="large-3 columns">
  <ul id="pushMainNavLeft" class="side-nav">
    <li class="{{ \App\Helpers\Menu::activeMenu(['dashboard']) }}"><a href="{{ route('dashboard') }}">Announcement</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['profile']) }}"><a href="{{ route('profile') }}">Profile</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['inbox']) }}"><a href="{{ route('inbox') }}">Inbox&nbsp;<span id="message_notification"></span></a></li>

    <li class="{{ \App\Helpers\Menu::activeMenu(['avaya_one_x_guide']) }}"><a href="{{ route('avaya_one_x_guide') }}">AVAYA one-X Guide</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['paid_leaves']) }}"><a href="{{ route('paid_leaves') }}">Paid Leaves</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['it_security_policy']) }}"><a href="{{ route('it_security_policy') }}">IT Security Policy</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['qa_newsletter']) }}"><a href="{{ route('qa_newsletter') }}">QA Newsletter</a></li>
    <li class="{{ \App\Helpers\Menu::activeMenu(['payslip_index']) }}"><a href="{{ route('payslip_index') }}">Payslip Password Reset</a></li>
  </ul>
  
  @if(Auth::user()->ou == 'IT Systems')
    @if(App\UsersAdmin::where('ntlogin',Auth::user()->ntlogin)->count() != 0)
      @include('push.menus.programmer')
    @else
      @include('push.menus.it')
    @endif
  @elseif(Auth::user()->ou == 'Quality')
      @include('push.menus.quality')
  @elseif(Auth::user()->ou == 'Supervisors')
      @include('push.menus.supervisors')
  @elseif(Auth::user()->ou == 'CA')
      @include('push.menus.ca')
  @elseif(Auth::user()->ou == 'HRD')
      @include('push.menus.hrd')
  @elseif(Auth::user()->ou == 'Managers')
      @include('push.menus.managers')
  @elseif(Auth::user()->ou == 'RAPID')
      @include('push.menus.rapid')
  @elseif(Auth::user()->ou == 'Receptionist')
      @include('push.menus.receptionist')
  @elseif(Auth::user()->ou == 'Recruitment')
      @include('push.menus.recruitment')
  @elseif(in_array(Auth::user()->ou,['Training','SME']))
      @include('push.menus.sme_and_training')
  @elseif(in_array(Auth::user()->ou, ['ERD','Legacy','PagePlus','SafeLink','SimpleMobile','StraightTalk','SupGroup']))
      @include('push.menus.agent')
  @elseif(Auth::user()->ou == 'Trainee')
      @include('push.menus.trainee')
  @elseif(Auth::user()->ou == 'CMSGroup')
      @include('push.menus.cms')
  @elseif(Auth::user()->ou == 'Coordinators')
      @include('push.menus.coordinators')
  <!-- Special Permission -->
  @elseif(Auth::user()->ou == 'Coordinators')
      @include('push.menus.nurse')
  @endif
</aside>