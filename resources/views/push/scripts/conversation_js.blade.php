<script type="text/javascript">
	var xhr_messages = null;
	var xhr_delete_messages = null;

	$(function(){	
		//initialize message function
		messages();
		//scroll to top of thread-list on refresh
		$('#thread-list').scrollTop(0);
	});

	//initialize infinitescroll to thread-list
    $('#items').infinitescroll({
    	loading 		: {
		        finishedMsg	: "<div class='end-msg'>Nothing to load.</div>",
		        msgText		: "",
		        img			: "{{ asset('img/ajax-loader.gif') }}",
		        speed		:'slow',
		        animate		: true
    	},
	    navSelector     : "ul.pager",
	    nextSelector    : "ul.pager li:last a",
	    itemSelector    : "ul#items li.item",
	    debug           : false,
	    binder			: $('#thread-list'),
	    behavior		: 'local'
	}, function(){
		//call message function
		messages();
		//refresh tooltip for thread subject
		$(document).foundation('tooltip', 'reflow');
	});

	function messages()
	{
		var delete_count = 0;

		//onclick show message
		$(".show-message").off('click');
		$(".show-message").on('click',function(e){
			var thisVal = $(this);
			xhr_messages = $.ajax({
				type  : 'get',
				url   : thisVal.attr('href'),
				dataType : "html",
				beforeSend: function(xhr){
					if (xhr_messages != null){
						xhr_messages.abort();
					}
				//show loading image	
				$("#message-list").html("<div class='center-loader'><img src='{{ asset('img/ajax-loader.gif') }}' /></div>");				
				//call notification count
				call_message_notification();
			}
			}).done(function(response){
				//load messages to message-list
				$("#message-list").html(response);

				//set thread to active
				$(".show-message").removeClass('active-thread');
				thisVal.addClass('active-thread');

				//set thread to read if thread is unread
				thisVal.removeClass('unread_item');
				thisVal.addClass('read_item');

				//refresh tooltip inside message-list
				$(document).foundation('tooltip', 'reflow');

				//show reply button
				$('a#reply-button').show();
				$('a#reply-button').attr('href', thisVal.attr('href').replace("messages", "reply"));
			});

			return false;
		});

		//on click delete message
		$(".delete-message").off('click');
		$(".delete-message").on('click',function(e){			
			var delVal = $(this);
			//confirm if user wants to delete the message
			if(confirm ("Are you sure you want to delete this thread?"))
			{
				$.ajax({
				 	type  : 'get',
				 	url   : delVal.attr('href'),
				 	cache 	: false,
				 	dataType : "html",
				 	beforeSend: function(xhr){
				 		if (xhr_delete_messages != null){
				 			xhr_delete_messages.abort();
				 		}
				 		var items = $('.item').length;
				 		console.log(items);
				 		if(items===8){	$('#items').infinitescroll('scroll'); }
				 		$(".delete-message").hide();
				 		call_message_notification();
					}
				}).done(function(response){
					$(".delete-message").show();
					delVal.parent().remove();
				 	$("#message-list").html("");
				 	$('a#reply-button').hide();
					$('a#reply-button').attr('href', "#");
				});
			}
			return false;
		});
	}
</script>