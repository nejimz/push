<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersAnnouncement extends Model
{
    //
     public $table = 'users_announcement';
    public $timestamps = false;
}
