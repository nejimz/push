<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeaveAgent extends Migration
{
    public function up()
    {
        Schema::create('leave_agent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ntlogin', 100);
            $table->string('leave_type', 50);
            $table->date('leave_date');
            $table->string('reason');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    public function down()
    {
        Schema::drop('leave_agent');
    }
}
