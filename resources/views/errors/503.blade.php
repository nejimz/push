<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <style>
            @font-face {
              font-family: 'Lato';
              font-style: normal;
              font-weight: 100;
              src: local('Lato Hairline'), local('Lato-Hairline'), url(../../fonts/lato-hairline.woff) format('woff');
            }
            
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Be right back.</div>
            </div>
        </div>
    </body>
</html>
