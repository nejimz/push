<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Users;

use Auth;
use DB;
use Validator;

class NetworkUtilizationController extends Controller
{

    public function index(Request $request)
    {
		return view('push.network-utilization.index');
    }

    public function upload(Request $request)
    {
		return view('push.network-utilization.upload');
    }
}
