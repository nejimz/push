<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleaseCallValidReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('release_call_valid_reasons', function (Blueprint $table) {
            $table->increments('reason_ID');
            $table->text('valid_reason');
        });

        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'TEST CALL: After placing a test call and the CSR needs to go back to the other line.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'Non-business related issues.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'Customer verbalizes that they don’t know how to end the call.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'Customers who are verbally abusive, after warning is given the agent will have to end the call.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'Customers who does not hang up at the end of the call.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'Customer places agent on hold but was not able to return to the line after three minutes.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'Dead air calls.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'Making a call back and the agent is routed to voicemail.'] );
        DB::table('release_call_valid_reasons')->insert( [ 'valid_reason' => 'EMERGENCY: In cases of natural disasters and agent needs to exit the building.'] );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('release_call_valid_reasons');
    }
}
