@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
        
        <!-- Container Title -->
        <div class="row" >
          <div class="medium-12 columns">
            <h4></h4>
          </div>
        </div>
        <!-- Container Title -->

        <!-- Container Content -->
        <div class="row">
          <div class="medium-12 columns">
            Doorlog
          </div>
        </div>
        <!-- Container Content -->
        
<!-- Stops Container -->
@stop

@section('scripts')
  @include('includes.timekeeping_js');
@stop