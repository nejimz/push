<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersDistribution extends Model
{
    //
    public $table = 'users_distribution';
    public $timestamps = false;

    public function users()
    {
    	return $this->hasMany('App\Distributions', 'id', 'distribution_id');
    }
}