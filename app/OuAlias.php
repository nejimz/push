<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ous;

class OuAlias extends Model
{
     public $table = 'ou_alias';
     public $timestamps = false;

    public function scopeAlias()
    {
    	return $this->leftjoin('Ous', 'name','=', 'ou');
    }
}
