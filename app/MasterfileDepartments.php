<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileDepartments extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'departments';
    public $timestamps = false;
}
