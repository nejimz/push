/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
$(function(){

    CKEDITOR.editorConfig = function( config ) {
        config.skin = 'office2013';
        config.removePlugins = 'sharedspace,dragdrop,contextmenu,tabletools,liststyle';
        config.toolbarGroups = [
            { "name": 'document', "groups": [ 'mode', 'document', 'doctools' ] },
            { "name" : "basicstyles", "groups" : [ "basicstyles" ] },
            { "name" : "paragraph", "groups" : [ "list", "align" ] },
            { "name" : "styles" },
            { "name" : "insert" },
        ];
        config.forcePasteAsPlainText = true;
        config.removeButtons= 'Save,Source,Templates,Print,Strike,Subscript,Superscript,Styles,Format,Iframe,Flash,Image,PageBreak';
    };


    /*CKEDITOR.instances.messageBody.on('key', function(e){
        //console.log(e.data.keyCode);
        if(e.data.keyCode == '1114179')
        {
            return false;
        }

        if(e.data.keyCode == '1114198')
        {
            return false;
        }

        if(e.data.keyCode == '1114200')
        {
            return false;
        }

        if(e.data.keyCode == '1114177')
        {
            return false;
        }
    });*/

    CKEDITOR.on('instanceReady', function(event){
         event.editor.document.on('contextmenu', function(ev){
            ev.data.$.preventDefault();
         });
    });

});