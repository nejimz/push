<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationMembers extends Model
{
    //
    public $table = 'conversation_members';
    public $timestamps = false;
    
    public function members()
    {
    	return $this->hasMany('App\Conversations', 'id', 'conversation_id');
    }

    public function scopeCheckRecipients($query,$conversation_id,$ntlogin)
    {
        return $query->where('conversation_id',$conversation_id)
                    ->where('conversation_members.ntlogin',$ntlogin);
    }

    public function scopeRecipients($query,$conversation_id,$ntlogin)
    {
    	return $query->leftJoin('users', 'conversation_members.ntlogin', '=', 'users.ntlogin')
                    ->where('conversation_id', $conversation_id)
                    ->where('conversation_members.ntlogin','!=',$ntlogin)
                    ->select(['conversation_members.ntlogin', 'users.name', 'conversation_members.group']);
    }

    public function scopeMessageLastView($query, $conversation_member_id)
    {
        return $query->where('id', $conversation_member_id)
                     ->select(['conversation_members.last_view']);
    }

    public function scopeMessageItemClick($query, $conversation_member_id)
    {
        return $query->where('id', $conversation_member_id)
                    ->update(['last_view'=>date('Y-m-d H:i:s.000',time()+1)]);
    }

    public function scopeMessageDelete($query, $conversation_member_id)
    {
        return $query->where('id', $conversation_member_id)
                    ->update(['deleted_at'=>1]);
    }
}
