<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferLogCallDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('transfer_log_call_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->references('id')->on('transfer_log_transactions');
            $table->string('call_details');
            $table->text('department')->nullable();
            $table->string('extension')->nullable();
            $table->integer('validity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('transfer_log_call_details');
    }
}
