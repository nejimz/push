<!-- Code of Conduct Popup -->
<div id="codeOfConductModal" class="reveal-modal medium" data-reveal aria-labelledby="codeOfConduct" aria-hidden="true" role="dialog">
  <img src="{{ asset('img/code-of-conduct/page_1.jpg') }}">
  <br />
  <br />
  <br />
  <div data-alert class="alert-box info">
    By clicking I Accept, you confirm that you have read the terms and conditions, that you understand them and that you agree to be bound by them.
  </div>
  <a href="{{ route('code_of_conduct') }}" class="button success" id="iAcceptCodeOfConductBtn">I Accept</a>
</div>

<script type="text/javascript">
$('#codeOfConductModal').foundation('reveal', 'open', {
  animation_speed: 0,
  close_on_background_click: false
});
</script>