<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileCivilStatus extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'civil_status';
    public $timestamps = false;
}
