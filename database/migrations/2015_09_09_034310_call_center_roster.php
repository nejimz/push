<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CallCenterRoster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('call_center_roster', function (Blueprint $table) {
            $table->increments('roster_ID');
            $table->string('employee_ID', 10);
            $table->string('organization', 45);
            $table->string('avaya', 10);
            $table->date('hire_date');
            $table->string('supervisor_ID', 10);
            $table->string('work_pattern', 25);
            $table->integer('tier');
            $table->string('ac_ID', 10);
            $table->date('week_ending');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('call_center_roster');
    }
}
