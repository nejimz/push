@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

<div class="row">
  <div class="large-12 columns">
    <h4><i class="fa fa-user"></i> Profile</h4>
  </div>
</div><!-- row -->

<div class="row">
  <div class="large-3 column">
    <a class="th" href="#"><img src="http://placehold.it/150x150"></a>
  </div>
  <div class="large-9 column">
    <div class="panel">

      <div class="row">
        <div class="large-3 column">
          <strong>Name</strong>
        </div>
        <div class="large-9 column">
          {{ $employee->employee_full_name}}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Birth Date</strong>
        </div>
        <div class="large-9 column">
          {{ $employee->birth_date_format}}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Gender</strong>
        </div>
        <div class="large-9 column">
          {{ $employee->gender_format }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Civil Status</strong>
        </div>
        <div class="large-9 column">
          {{ $pay_details->civil_status_format }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Contact Number 1</strong>
        </div>
        <div class="large-9 column">
          {{ $employee->contactNumber }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Contact Number 2</strong>
        </div>
        <div class="large-9 column">
          {{ $employee->contactNumber2 }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Telephone Number</strong>
        </div>
        <div class="large-9 column">
          {{ $employee->telNumber }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Old Address</strong>
        </div>
        <div class="large-9 column">
          {{ $address->oldAddress }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Street</strong>
        </div>
        <div class="large-9 column">
          {{ $address->street }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Subdivision</strong>
        </div>
        <div class="large-9 column">
          {{ $address->subd }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Sitio</strong>
        </div>
        <div class="large-9 column">
          {{ $address->sitio }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Barangay</strong>
        </div>
        <div class="large-9 column">
          {{ $address->brgy }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>City</strong>
        </div>
        <div class="large-9 column">
          {{ $address->city }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Province</strong>
        </div>
        <div class="large-9 column">
          {{ $address->province }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">&nbsp;</div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Employee Number</strong>
        </div>
        <div class="large-9 column">
          {{ $account->empNumber }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Wave Number</strong>
        </div>
        <div class="large-9 column">
          {{ $information->waveNumber }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Branch</strong>
        </div>
        <div class="large-9 column">
          {{ $information->branch_format }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Department</strong>
        </div>
        <div class="large-9 column">
          {{ $information->department_format->department }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Position</strong>
        </div>
        <div class="large-9 column">
          {{ $information->department_format->position }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Employee Status</strong>
        </div>
        <div class="large-9 column">
          {{ $information->employee_status_format }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Date Hired</strong>
        </div>
        <div class="large-9 column">
          {{ $information->employement_date_format }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Tax Status</strong>
        </div>
        <div class="large-9 column">
          {{ $pay_details->tax_status_format }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Rehire</strong>
        </div>
        <div class="large-9 column">
          {{ $information->rehire_format }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>SSS</strong>
        </div>
        <div class="large-9 column">
          {{ $credentials->sss }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>TIN</strong>
        </div>
        <div class="large-9 column">
          {{ $credentials->tin }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Philhealth</strong>
        </div>
        <div class="large-9 column">
          {{ $credentials->philhealth }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>Account Number</strong>
        </div>
        <div class="large-9 column">
          {{ $credentials->accountNumber }}
        </div>
      </div>

      <div class="row">
        <div class="large-3 column">
          <strong>HDMF</strong>
        </div>
        <div class="large-9 column">
          {{ $credentials->hdmf }}
        </div>
      </div>

    </div>
  </div>
</div>

<!-- Stops -->
@stop