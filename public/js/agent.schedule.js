/**
Break Notification
**/
var xhr_break_notification = null;

$(function(){
	//Call message notification function on first load of page
	setInterval(break_notification, 1000);
});

function break_notification()
{
	xhr_break_notification =	$.ajax({
		type : 'get',
		url : $("#break_schedule_notification").val(),
		cache : false,
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		dataType : "script",
		beforeSend: function(xhr){
			if (xhr_break_notification != null)
			{
				xhr_break_notification.abort();
			}
		}
	}).done(function(response){
		//console.log(response);
	});
}