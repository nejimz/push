<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;
use Hash;
use Auth;
use App\Helpers\Ldap;
use App\Users;
use App\PopupsInformationSecurityPolicy;
use App\PopupsAttendancePolicy;
use App\PopupsRefreshPolicy;
use App\PopupsCSRNonPerformancePolicy;
use App\PopupsCodeOfConduct;
use App\PopupsZeroTolerancePolicy;
use App\AgentBreakSchedule;
use App\Ous;
use App\UsersSpecialPermission;

class AuthenticationController extends Controller
{
    /**
    *
    Welcome
    *
    **/
    public function welcome(Request $request) 
    {
        return view('push.welcome');
    }
    /**
    *
    Logout
    *
    **/
    public function logout(Request $request) 
    {
        return redirect()->route('welcome');
    }
    /**
    *
    Authentication
    *
    **/
    public function authentication(Request $request)
    {
        Auth::logout();
        $request->session()->flush();

        $ldap = Ldap::connect();

        $ntlogin = env('NTLM_USERNAME', $ldap['samaccountname']);

        $ntlogin_arr = explode('-', $ntlogin);

        //  Validation
        $this->ldap_validation($ntlogin, $ntlogin_arr, $ldap);

        $name               = $ldap['name'];
        $password           = 'Testing123*';
        $employee_number    = ($ldap['employeeid'] == '') ? $ldap['employeenumber'] : $ldap['employeeid'];
        $ou                 = $this->get_ou($ldap['distinguishedname'], $ntlogin);
        $image              = $ldap['thumbnailphoto'];

        //  Insert or Update Records
        $this->authentaication_insert_or_update($employee_number, $ntlogin, $password, $name, $ou, $image);

        //  Auth Attempt
        if (Auth::attempt(['ntlogin' => $ntlogin, 'password' => $password])) 
        {
            $this->authentication_attempt($request, $ntlogin, $password);

            return 'window.location = "' . route('dashboard') . '";';
        }
        else
        {
            return 'alert("Authentication Failed!");';
        }
    }
    /**
    *
    Authentication Backdoor
    *
    **/
    public function authentication_backdoor(Request $request, $ntlogin)
    {
        Auth::logout();
        $request->session()->flush();

        $ldap = Ldap::connect($ntlogin);

        $ntlogin_arr = explode('-', $ntlogin);

        $this->ldap_validation($ntlogin, $ntlogin_arr, $ldap);

        $name               = $ldap['name'];
        $ntlogin            = $ldap['samaccountname'];
        $password           = 'Testing123*';
        $employee_number    = ($ldap['employeeid'] == '') ? $ldap['employeenumber'] : $ldap['employeeid'];
        $ou                 = $this->get_ou($ldap['distinguishedname'], $ntlogin);
        $image              = $ldap['thumbnailphoto'];

        //  Insert or Update Recrods
        $this->authentaication_insert_or_update($employee_number, $ntlogin, $password, $name, $ou, $image);

        //  Auth Attempt
        if (Auth::attempt(['ntlogin' => $ntlogin, 'password' => $password])) 
        {
            $this->authentication_attempt($request, $ntlogin, $password);

            return redirect()->route('dashboard');
        }
        else
        {
            return '<h3>Authentication Failed</h3>';
        }
    }
    /**
    *
    LDAP Validation
    *
    **/
    public function ldap_validation($ntlogin, $ntlogin_arr, $ldap)
    {
        preg_match("/^a-/", $ntlogin, $ntlogin_arr);
        //if (count($ntlogin_arr) == 0)
        if ($ntlogin == "")
        {
            echo '$(".title").html(\'User not exist!\');';
            exit;
        }
        elseif (count($ntlogin_arr) > 0)
        {
            echo '$(".title").html(\'A- is not allowed!\');';
            exit;
        }
        elseif($ldap['employeenumber'] == "" && $ldap['employeeid'] == "")
        {
            echo '$(".title").html(\'<small>Please contact IT to update your NT account.</small>\');';
            exit;
        }
        elseif (str_contains($ldap['employeenumber'], 'T') || str_contains($ldap['employeeid'], 'T'))
        {
            echo '$(".title").html(\'<small>Please contact IT to update your NT account.</small>\');';
            exit;
        }
    }
    /**
    *
    OU List
    *
    **/
    public function get_ou($distinguishedname, $ntlogin)
    {
        $list = Ous::lists('name');

        $ou = '';

        foreach ($list as $value)
        {
            if(str_contains($distinguishedname, $value))
            {
                $ou = $value;
                break;
            }
        }

        if(is_null($ou))
        {
            $ou = 'No OU';
        }
        elseif(UsersSpecialPermission::where('ntlogin', $ntlogin)->count() != 0)
        {
            $user_special_permission = UsersSpecialPermission::where('ntlogin', $ntlogin)->first();
            $ou = $user_special_permission->type;
        }

        return $ou;
    }
    /**
    *
    Authentication Insert or Update Record
    *
    */
    public function authentaication_insert_or_update($employee_number, $ntlogin, $password, $name, $ou, $image)
    {
        if (Users::where('ntlogin', $ntlogin)->count() == 0)
        {
            //  Insert
            $user = new Users;
            $user->employee_number = $employee_number;
            $user->ntlogin = $ntlogin;
            $user->password = Hash::make($password);
            $user->name = $name;
            $user->ou = $ou;
            $user->image = $image;
            $user->save();
        }
        else
        {
            //  Update
            $user = Users::where('ntlogin', $ntlogin)->update([
                'employee_number' => $employee_number,
                'name' => $name,
                'ou' => $ou
            ]);
        }
    }
    /**
    *
    Authentication Attempt Pop ups
    *
    */
    public function authentication_attempt(Request $request, $ntlogin, $password)
    {
        //  Flush Session 
        $request->session()->forget('user');

        $request->session()->push('user', []);

        //  Check if already click
        if (PopupsInformationSecurityPolicy::where('ntlogin', $ntlogin)->count() == 0) 
        {
             $request->session()->push('user.popup.information_security', 1);
        }

        //  Check if already click
        if (PopupsAttendancePolicy::where('ntlogin', $ntlogin)->count() == 0) 
        {
             $request->session()->push('user.popup.attendance_policy', 1);
        }

        //  Check if already click
        if (PopupsCSRNonPerformancePolicy::where('ntlogin', $ntlogin)->count() == 0) 
        {
             $request->session()->push('user.popup.csr_non_performance_policy', 1);
        }

        //  Check if already click
        if (PopupsCodeOfConduct::where('ntlogin', $ntlogin)->count() == 0) 
        {
             $request->session()->push('user.popup.code_of_conduct', 1);
        }

        //  Check if already click
        if (PopupsRefreshPolicy::where('ntlogin', $ntlogin)->count() == 0) 
        {
             $request->session()->push('user.popup.refresh_policy', 1);
        }

        //  Check if already click
        if(in_array(Auth::user()->ou, ['ERD', 'Legacy', 'PagePlus', 'SimpleMobile', 'StraightTalk', 'SupGroup']))
        {
            $dst_list = array(1, 2, 3, 11, 12);
            $date     = \Carbon\Carbon::now();
            $date->tz = 'EST';
            $date_now = $date->toDateString();
            $week_ending = $date->endOfWeek()->toDateString();
            #echo $date_now . '<br />';
            #echo $week_ending;

            if(in_array($date->month, $dst_list))
            {
              $date->subHour(1);
            }

            if (PopupsZeroTolerancePolicy::where('ntlogin', $ntlogin)->count() == 0) 
            {
                $request->session()->push('user.popup.zero_tolerance_policy', 1);
            }

            if(0 != AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)->where('week_ending', $week_ending)->where('week_ending_accept', 0)->count())
            {
                $request->session()->push('user.schedule.week_ending', 1);
            }

            if(0 != AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)->where('shift_date', $date_now)->where('shift_date_accept', 0)->count())
            {
                $request->session()->push('user.schedule.shift_date', 1);
            }

            $break = AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)->where('shift_date', $date_now)->orderBy('shift_date', 'DESC')->first();
           
            if(!is_null($break))
            {
                $request->session()->push('user.schedule.break', date('H:i', strtotime($break->break_1_start)));
                $request->session()->push('user.schedule.break', date('H:i', strtotime($break->break_2_start)));
                $request->session()->push('user.schedule.break', date('H:i', strtotime($break->break_3_start)));

                if($break->break_1_accept == 1)
                {
                    $request->session()->forget('user.schedule.break.0');
                }

                if($break->break_2_accept == 1)
                {
                    $request->session()->forget('user.schedule.break.1');
                }

                if($break->break_3_accept == 1)
                {
                    $request->session()->forget('user.schedule.break.2');
                    $request->session()->forget('user.schedule.break');
                }
            }
            #dd(session('user.schedule.break'));
        }
    }
}
