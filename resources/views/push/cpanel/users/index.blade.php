@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
   	<div class="medium-12 columns">

   		<h4><i class="fa fa-users"></i> PUSH Users</h4>

   		<form action="{{ route('users_index') }}" method="get">
			<div class="row">
				<div class="large-6 columns">
					<div class="row collapse">
						<div class="large-10 columns">
							<input name="search" type="text" placeholder="Search" value="{{ $search }}" autocomplete="off">
						</div>
						<div class="large-2 columns">
							<button class="button button-small postfix">Search</button>
						</div>
					</div>
				</div>
			</div>
   		</form>

		{!! str_replace('/?', '?', $users->appends(['search'=>$search])) !!}
		<table width="100%">
			<thead>
				<tr>
					<th width="5%"></th>
					<th width="10%">ID</th>
					<th width="18%">Employee No.</th>
					<th width="20%">NT-Login</th>
					<th width="30%">Name</th>
					<th width="17%">OU</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td><a href="{{ route('users_edit',[$user->ntlogin]) }}"><i class="fa fa-edit"></i></a></td>
					<td>{{ $user->id }}</td>
					<td>{{ $user->employee_number }}</td>
					<td>{{ $user->ntlogin }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->ou }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! str_replace('/?', '?', $users->appends(['search'=>$search])) !!}
   	</div>
<!-- Stops -->
@stop