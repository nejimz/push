<!-- CSR Non Performance Policy Popup -->
<div id="csrNonPerformancePolicyModal" class="reveal-modal medium" data-reveal aria-labelledby="csrNonPerformancePolicy" aria-hidden="true" role="dialog">
  <img src="{{ asset('img/memo/Memo-GM-20130913-05 -CSR Non-Performance Policy.jpg') }}">
  <br />
  <br />
  <br />
  <div data-alert class="alert-box info">
    By clicking I Accept, you confirm that you have read the terms and conditions, that you understand them and that you agree to be bound by them.
  </div>
  <a href="{{ route('csr_non_performance_policy') }}" class="button success" id="iAcceptCsrNonPerformancePolicyBtn">I Accept</a>
</div>

<script type="text/javascript">
$('#csrNonPerformancePolicyModal').foundation('reveal', 'open', {
  animation_speed: 0,
  close_on_background_click: false
});
</script>