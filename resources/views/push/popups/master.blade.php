
<!-- Required Popup -->
@if(Session::has('user.popup.information_security'))
	<!-- Information Security Policy -->
	@include('push.popups.information_security_policy')
@elseif(Session::has('user.popup.attendance_policy'))
	<!-- Attendance Policy Popup -->
	@include('push.popups.attendance_policy')
@elseif(Session::has('user.popup.csr_non_performance_policy'))
	<!-- CSR Non Performance Policy Popup -->
	@include('push.popups.csr_non_performance_policy')
@elseif(Session::has('user.popup.code_of_conduct'))
	<!-- Code of Conduct Popup -->
	@include('push.popups.code_of_conduct')
@elseif(Session::has('user.popup.zero_tolerance_policy'))
	<!-- Zero Tolerance Policy Popup -->
	@include('push.popups.zero_tolerance_policy')
@elseif(Session::has('user.popup.refresh_policy'))
	<!-- Refresh Policy Popup -->
	@include('push.popups.refresh_policy')

<!-- 
	Agent Schedule Popup are always in the last condition
-->
@elseif(Session::has('user.schedule.week_ending'))
	<!-- Per Week Ending Schedule-->
	@include('push.popups.my_schedule')
@elseif(Session::has('user.schedule.shift_date'))
	<!-- Per Week Ending Schedule-->
	@include('push.popups.my_shift_schedule')
@elseif(Session::has('user.schedule.break'))
	<!-- Per Week Ending Schedule-->
	@include('push.popups.my_break')
@endif
