@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
<div class="row" >
	<div class="medium-12 columns">
		<h4><i class="fa fa-cloud-upload"></i> Upload</h4>
	</div>
</div>
<!-- Container Title -->

<!-- Container Content -->
<form action="{{ route('upload_agent_schedules_store') }}" method="post" enctype="multipart/form-data">

	<div class="row">
		<div class="large-12 columns">
			@if($errors->has())
			  <div data-alert class="alert-box warning">
			    @foreach($errors->all() as $error)
			        {{ $error }}<br />
			    @endforeach
			    <a href="#" class="close">&times;</a>
			  </div>
			@elseif(Session::has('uploadAgentScheduleSuccess'))
			  <div data-alert class="alert-box success">
			        {{ Session::get('uploadAgentScheduleSuccess') }}
			    <a href="#" class="close">&times;</a>
			  </div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<input id="uploadFile" placeholder="Choose File" disabled="disabled" />
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<div class="button file-upload left" id="file-upload">
				Browse <input type="file" id="file" name="file" />
			</div>

			<button type="submit" class="button success left">Save</button>
			{!! csrf_field() !!}
		</div>
	</div>


</form>
<!-- Container Content -->

<script type="text/javascript">
document.getElementById("file").onchange = function () {
  document.getElementById("uploadFile").value = this.value;
};
</script>
<!-- Stops -->
@stop