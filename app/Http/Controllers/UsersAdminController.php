<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Users;
use App\UsersAdmin;

use Auth;

class UsersAdminController extends Controller
{
    public function __construct()
    {
        //  Only Allowed OU's can use the messaging 
        if(UsersAdmin::where('ntlogin', Auth::user()->ntlogin)->count() == 0)
        {
            abort(404);
        }
    }

    public function index()
    {
        //
        $admins = UsersAdmin::leftJoin('users', 'users_admin.ntlogin', '=', 'users.ntlogin')
                    ->whereNotNull('users.id')
                    ->orderBy('name', 'ASC')
                    ->paginate(50);

        $data = compact('admins');
        
        return view('push.cpanel.users.admin.index', $data);
    }

    public function add()
    {
        //
        return view('push.cpanel.users.admin.add');
    }

    public function store(Request $request)
    {
        $rules = [
                'ntlogin'=>'required|unique:users_admin,ntlogin|exists:users,ntlogin'
            ];
        $message = [
                'ntlogin.required'  => 'Ntlogin is required.',
                'ntlogin.unique'    => 'Ntlogin already exists.'
            ];

        $this->validate($request, $rules, $message);
        //
        $admin = new UsersAdmin;
        $admin->ntlogin = trim($request->input('ntlogin'));
        $admin->save();

        return redirect()->route('users_admin_add')->with('success', 'Ntlogin successfully added!');
    }

    public function destroy($ntlogin)
    {
        //
        UsersAdmin::where('ntlogin', $ntlogin)->delete();

        return redirect()->route('users_admin')->with('success', 'Ntlogin successfully deleted!');
    }

}
