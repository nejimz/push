<!-- My Schedule Popup -->
<div id="myScheduleModal" class="reveal-modal" data-reveal aria-labelledby="mySchedule" aria-hidden="true" role="dialog">
<?php
$dst_list = array(1, 2, 3, 11, 12);
$date     = \Carbon\Carbon::now();
$date->tz = 'EST';
/*
echo '<pre>';
print_r($date);
echo '</pre>';
echo $date->toDateTimeString() . '<br />';
echo $date->toDateString() . ' ' . $date->toTimeString() . '<br />';
echo $date->month . '<br />';
echo $date->endOfWeek()->toDateString() . '<br />';
*/
if(in_array($date->month, $dst_list))
{
  $date->subHour(1);
}

$schedule = App\AgentBreakSchedule::where('employee_number', Auth::user()->employee_number)
            ->where('shift_date', $date->toDateString())
            ->where('shift_date_accept', 0)
            ->get();
?>
  <table width="100%">
    <caption><h3 class="left">My Schedule Breaks</h3></caption>
    <thead>
      <tr>
        <th width="40%">Shift Date</th>
        <th width="20%">Break 1</th>
        <th width="20%">Break 2</th>
        <th width="20%">Break 3</th>
      </tr>
    </thead>
    <tbody>
    @foreach($schedule as $row)
      <tr>
        <td>
          {{ date('F d, Y', strtotime($row->shift_date)) }} 
          @if(is_null($row->break_1_start))
            <span class="alert label">Not Scheduled</span>
          @else
            <span class="label">Scheduled</span>
          @endif
        </td>
          @if(is_null($row->break_1_start))
          <td></td>
          <td></td>
          <td></td>
          @else
          <td>{{ date('H:i', strtotime($row->break_1_start)) . ' - ' . date('H:i', strtotime($row->break_1_end)) }} <i>EST</i></td>
          <td>{{ date('H:i', strtotime($row->break_2_start)) . ' - ' . date('H:i', strtotime($row->break_2_end)) }} <i>EST</i></td>
          <td>{{ date('H:i', strtotime($row->break_3_start)) . ' - ' . date('H:i', strtotime($row->break_3_end)) }} <i>EST</i></td>
          @endif
      </tr>
    @endforeach
    </tbody>
  </table>

  <a href="{{ route('my_shift_schedule') }}" class="button success">I Accept</button>
</div>

<script type="text/javascript">
$('#myScheduleModal').foundation('reveal', 'open', {
  animation_speed: 0,
  close_on_background_click: false
});
</script>