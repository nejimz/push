@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
	<div class="row">
     	<div class="large-12 columns">
     		<h4><i class="fa fa-group"></i> 
            {{ $distribution_info->distribution or "ERROR!!" }} 
            <span class="right">
              <a class="button small" data-reveal-id="edit-distribution-modal" href="#"><i class="fa fa-edit"></i> Edit Distribution</a>
              <a class="button small" data-reveal-id="add-distribution-modal"><i class="fa fa-plus"></i> Add User </a>
              <a class="button small" href="{{ route('distribution_index') }}"><i class="fa fa-arrow-left"></i> Return to Distribution list</a>     
            </span>  
        </h4>
     	</div>
  </div>
  <div class="row">
  	<div class="large-12 columns" id="distribution-members">
      <input class="search" placeholder="Search" />
      <table style="width:100%" >        
          <thead>
            <th style="width:10px;"></th>
            <th style="width:180px;">Employee Number</th>
            <th>Name</th>
            <th>NTLogin</th>
            <th style="width:10px;"></th>
          </thead>
        <tbody class="list">
         @foreach($distribution_members as $member)
            <tr>
              <td><a class="" href="{{ route('user_edit', $member->ntlogin) }}"><i class="fa fa-user"></i></a></td>
              <td class="employeeno">{{ $member->employee_number or 'NA' }}</td> 
              <td class="name">{{ $member->name or 'NA'}}</td>                
              <td class="ntlogin">{{ $member->ntlogin or 'NA'}}</td>
              <td><a class="" href="#"><i class="fa fa-remove"></i></a></td>
              </tr>
         @endforeach
         </tbody>
       </table>
  	</div>    	
  </div>

  <div id="edit-distribution-modal" class="reveal-modal tiny custom-modal" data-reveal aria-hidden="true" role="dialog">
      <p><form>
        <label>Distribution Name</label>
        <input type="text" readonly="readonly" name="id" value="{{ $distribution_info->id }}"/>
        <input type="text" name="distribution" value="{{ $distribution_info->distribution }}" />
        <input type="submit" class="button small right" />
        <input type="text" name="old_route" value="{{ Route::currentRouteNAme() }}" />
      </form></p>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  </div>

  <div id="add-distribution-modal" class="reveal-modal tiny custom-modal" data-reveal aria-hidden="true" role="dialog">
      <p><form>
        <label>Search User</label>
        <input type="text" readonly="readonly" name="id" value="{{ $distribution_info->id }}"/>
        <input type="text" name="distribution" value="{{ $distribution_info->distribution }}" />
        <input type="submit" class="button small right" />
        <input type="text" name="old_route" value="{{ Route::currentRouteNAme() }}" />
      </form></p>
      <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  </div>
<!-- Stops Container -->
@stop

@section('scripts')
  @include('push.scripts.distribution_edit_js')
@stop