<!-- Refresh Policy Popup -->
<div id="refreshPolicyModal" class="reveal-modal medium" data-reveal aria-labelledby="refreshPolicy" aria-hidden="true" role="dialog">
  <img src="{{ asset('img/refresh-policy/1.png') }}">
  <br />
  <br />
  <br />
  <div data-alert class="alert-box info">
    By clicking I Accept, you confirm that you have read the terms and conditions, that you understand them and that you agree to be bound by them.
  </div>
  <a href="{{ route('refresh_policy') }}" class="button success" id="iAcceptRefreshPolicyBtn">I Accept</a>
</div>

<script type="text/javascript">
$('#refreshPolicyModal').foundation('reveal', 'open', {
  animation_speed: 0,
  close_on_background_click: false
});
</script>