@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
  <!-- Container Content -->

<div class="row">

  <div class="medium-10 medium-centered columns">
    <form action="{{ route('save_transfer_log') }}" method="post" id="tl-form">
    <fieldset>
      <legend><h4>{{ $title or '' }}</h4></legend>
    <div class="row">
        <div class="small-2 columns">
            <label class="left inline" style="font-weight:bold;">Avaya #</label>
        </div>
        <div class="small-3 columns left">
            <input type="text" id="avayainput" name="avayainput" class="small-4 columns" readonly="readonly" value="{{ $avaya }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-2 columns">
            <label class="left inline">Account</label>
        </div>
        <div class="small-3 columns left">
          @if(count($account) > 1)
            <select id="accountinput" name="accountinput">
              @foreach($account as $accounts)
                <option value="{{ $accounts['id'] }}" @if ($chosenAcct == $accounts['id']) selected="selected"  @endif> {{ $accounts['name'] }}</option>
              @endforeach
            </select>
          @else
            <input type="text" id="accountinput" name="accountinput" class="small-4 columns" readonly="readonly" value="{{ $account }}" />
            <input type="hidden" id="accountID" name="accountID" class="small-4 columns" readonly="readonly" value="{{ $accountId }}" />
          @endif
        </div>
        <div class="small-2 columns">
            <label class="left inline">Agent Skill</label>
        </div>
        <div class="small-5 columns left">
          @if(count($skill) > 1)
            <select id="agentskillinput" name="agentskillinput">
              @foreach($skill as $skills)
                <option value="{{ $skills['id'] }}" @if ($chosenSkill == $skills['id']) selected="selected"  @endif> {{ $skills['skill'] }}</option>
              @endforeach
            </select>
          @else
            <input type="text" id="agentskillinput" name="agentskillinput" class="small-4 columns" readonly="readonly" value="{{ $skill }}" />
            <input type="hidden" id="skillId" name="skillId" class="small-4 columns" value="{{ $skillId }}" />
          @endif
        </div>
    </div>
    <br/>
    <h5 style="color:red;">Please select the required information.</h5>
    <br/>
    <div class="row">
      @include('push.messages.error_message_list')
      @include('push.messages.success_message')
    </div>
      <div class="row">
            <div class="small-4 columns">
                <label class="left inline">Transaction Type :</label>
            </div>
            <div class="small-8 columns">
                  <select id="transactionType" name="transactionType">
                    @foreach($transaction as $transactions)
                      <option value="{{ $transactions['id'] }}">{{ $transactions['transaction'] }}</option>
                    @endforeach
                  </select>
            </div>
      </div>
      <div class="row">
            <div class="small-4 columns">
                <label class="left inline">Call Received :</label>
            </div>
            <div class="small-8 columns">
                  <select id="callsreceived" name="callsreceived">
                    <option value="0">-- Choose a Transaction Type above--</option>
                  </select>
            </div>
      </div>
       <div class="row">
          <div class="small-4 columns">
              <label class="left inline">Ext/Department to Transfer :</label>
          </div>
          <div class="small-8 columns right">
              <input type="text" id="extdeptinput" name="extdeptinput" class="small-4 columns" readonly="readonly" value=""/>
          </div>
      </div>
    <br/>
      <div class="row">
        <div class="small-2 columns">
          <label class="left inline">Notes:</label>
        </div>
        <div class="small-10 columns right">
          <textarea rows="10" id="notesTransferLog" name="notesTransferLog"></textarea>
        </div>
      </div>

    <div class="row">
      <div class="small-4 small-offset-8 columns">
        <button type="reset" class="button secondary">Clear</button>
        <button type="submit" class="button success">Save</button>
      </div>
    </div>

        {!! csrf_field() !!}

    </form>
  </div>
</div>
  <!-- Container Content --> 
<!-- Stops Container -->

    <input type="hidden" id="changeCallReceived" name="changeCallReceived" value="{{ route('populate_calls') }}" />
    <input type="hidden" id="showExtDept" name="showExtDept" value="{{ route('populate_extension') }}" />
    <input type="hidden" id="storeTransferLog" name="storeTransferLog" value="{{ route('save_transfer_log') }}" />

@stop

@section('scripts')
  @include('push.scripts.transfer_log_js')
@stop