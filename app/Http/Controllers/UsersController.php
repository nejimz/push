<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UsersAdmin;
use App\Users;
use App\Ous;

class UsersController extends Controller
{
    public function index(Request $request)
    {
    	$search = '';
    	
    	if($request->has('search'))
    	{
    		$search = trim($request->input('search'));
    		$users = Users::where('name', 'LIKE', "%$search%")
                    ->orWhere('employee_number', 'LIKE', "%$search%")
                    ->orderBy('name', 'ASC')
                    ->paginate(50);
    	}
    	else
    	{
    		$users = Users::orderBy('name', 'ASC')->paginate(50);
    	}

    	$data = compact('users', 'search');

        return view('push.cpanel.users.index', $data);
    }

	public function edit($ntlogin)
    {
		$user = Users::where('ntlogin', $ntlogin)->first();

		if(is_null($user))
		{
			abort(404);
		}
		else
		{
			$ous = Ous::get();

			$data = [	'ntlogin' => $ntlogin, 
						'user' => $user, 
                        'ous' => $ous
					 ];
			return view('push.cpanel.users.edit', $data);
		}
	}

    public function update(Request $request)
    {
        $rules = [
                'employee_number'   => 'required|unique:users,employee_number,' . $request->input('id'),
                'ou'                => 'required'
            ];
        $messages = [
                'employee_number.required'  => 'Ntlogin is required.',
                'employee_number.unique'    => 'Ntlogin already exists.',
                'ou.required'               => 'OU is required..'
            ];

        $this->validate($request, $rules, $messages);

        $user = Users::find($request->input('id'));
        $user->employee_number  = $request->input('employee_number');
        $user->ou               = $request->input('ou');
        $user->save();

        return redirect()->route('users_edit',[$request->input('ntlogin')])->with('success', 'User successfully updated!');
    }

    public function search_json(Request $request)
    {
        //
        $search = trim($request->input('search'));

        $user = Users::where('name', 'LIKE', '%' . $search . '%')
                ->take(20)->lists('name', 'ntlogin');

        //return Response::json($products)->setCallback(Input::get('callback'));
        return response()->json($user);
    }
}