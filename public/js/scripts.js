

$(function(){	

	$(document).foundation();
});
/*	Inbox Page - Start */
/**
	Compose Message - Recepients
**/
$(function(){	
    $("#message-to").tokenInput($("#user_ldap_search").val(),
	{ 
		theme: "facebook",
        preventDuplicates: true,
		minChars: '3',
		resultsLimit: '10',
		searchDelay : 500,
		searchingText : "Searching...",
		placeholder: 'To',
		resultsFormatter: function(item) {
            var string = item.propertyToSearch;
            return "<li>" + item.name + " <span class='right'><small>"+ item.type +"</small></span></li>";
        }
	});
});
/**
	Ntlogin Search using name
**/
var xhr_ntlogin_search = null;
$(function(){
    $("#ntlogin_search").autocomplete({
        source: function (request, response) 
        {
            xhr_ntlogin_search = $.ajax({
                type : 'get',
                url : $("#users_search_json_url").val(),
                data : 'search=' + request.term,
                cache : false,
                dataType : "json",
                beforeSend: function(xhr) 
                {
                    if (xhr_ntlogin_search != null)
                    {
                        xhr_ntlogin_search.abort();
                    }
                }
            }).done(function(data) 
            {
                response($.map( data, function(value, key) 
                {
                    return { label: value, value: key }
                }));

            }).fail(function(jqXHR, textStatus) 
            {
                //console.log('Request failed: ' + textStatus);
            });
        }, 
        minLength: 2,
        autoFocus: true,
        select: function()
        {
            $("#ntlogin_search").focus();
        }
    });
});
/**
	Reset Payslip Password
**/
var xhr_generate_password = null;

$('#generate_button').on('click', function(e){
	generate_password();
});

function generate_password()
{
	xhr_generate_password = $.ajax({
		type  : 'get',
		url   : $("#hidden_generate_route").val(),
		data  : 'data=0',
		cache 	: false,
		dataType : "text",
		beforeSend: function(xhr){
			if (xhr_generate_password != null){
				xhr_generate_password.abort();
			}
	}
	}).done(function(response){
		$("#generated_password").html(response);
		//alert(1);
	}).fail(function(){
		//alert("Pls. refresh your browser.");
	});
}