<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterfileBranch extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'branch';
    public $timestamps = false;
}
