<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Auth;

use App\Roster;
use App\ReleaseCallValidReasons;
use App\ReleaseCallLog;

use Validator;

class ReleaseCallLogController extends Controller
{
    public function releaseCallValidationRules() 
    {
        return [
            'avaya-input' => 'required',
            'tier-input' => 'required',
            'skill-input' => 'required',
            #'select-reason' => 'regex:/[1-9]/'
            'select-reason' => 'numeric|min:1'
        ];
    }

    public function releaseCallValidationMessages() 
    {
        return [
            'avaya-input.required' => 'Your profile is not updated. Please contact local IT to have your AVAYA updated.',
            'tier-input.required' => 'Your profile is not updated. Please contact local IT to have your Tier updated.',
            'skill-input.required' => 'Your profile is not updated. Please contact local IT to have your Skill updated.',
            'select-reason.min' => 'Choose a reason.'
        ];
    }

    public function index()
    {
        //
        $employee_number = Auth::user()->employee_number;
        $agentData = Roster::RetrieveProfile($employee_number)->orderBy('week_ending', 'DESC')->first();
        if(is_null($agentData))
        {
           $data = [
                    'title'=>"Release Call Log",
                    'avaya'=>"",
                    'tier'=>"",
                    'skill'=>"",
                    'supervisor'=>"",
                    'reasons'=>""
                ];
        }
        else 
        {
            $validReasons = ReleaseCallValidReasons::all();

            //For supervisor

            $agent_supervisor = DB::connection('mysql2')
                    ->table('employee')
                    ->select([
                        'firstName','middleName','lastName','suffix','birthDate','gender',
                        'civil_status_name','contactNumber','contactNumber2','telNumber',
                        'oldAddress','street','subd','sitio','brgy','city','province',
                        'empNumber','waveNumber','branchName','empDate','TaxStatusName',
                        'departments.department','departments.position',
                        'tin','sss','philhealth','hdmf'
                    ])
                    ->leftJoin('employeeaccount', 'employeeaccount.empID', '=', 'employee.empID')
                    ->leftJoin('employeeaddress', 'employeeaddress.empID', '=', 'employee.empID')
                    ->leftJoin('employmentinfo', 'employmentinfo.empID', '=', 'employee.empID')
                    ->leftJoin('employeecredentials', 'employeecredentials.empID', '=', 'employee.empID')
                    ->leftJoin('employeepaydetails', 'employeepaydetails.empID', '=', 'employee.empID')

                    ->leftJoin('civil_status', 'civil_status.civil_status_id', '=', 'employeepaydetails.civilStatus')
                    ->leftJoin('taxstatus', 'taxstatus.TaxStatusID', '=', 'employeepaydetails.taxStatus')
                    ->leftJoin('departments', 'departments.idDep', '=', 'employmentinfo.position')
                    ->leftJoin('branch', 'branch.branchID', '=', 'employmentinfo.branchID')
                    ->where('empNumber', $agentData->supervisor_ID)
                    ->first();

            if(is_null($agent_supervisor))
            {
               $data = [
                    'title'=>"Release Call Log",
                    'avaya'=>$agentData->avaya,
                    'tier'=>$agentData->tier,
                    'skill'=>$agentData->organization,
                    'supervisor'=>"",
                    'reasons'=>$validReasons
                ];
            }
            else 
            {

                #dd($agentData);

                $data = [
                    'title'=>"Release Call Log",
                    'avaya'=>$agentData->avaya,
                    'tier'=>$agentData->tier,
                    'skill'=>$agentData->organization,
                    'supervisor'=>$agent_supervisor,
                    'reasons'=>$validReasons
                ];

                
            }
        }

        return view('push.release-call-log.release_call_log', $data);
    }

    public function store(Request $request)
    {
        //
        #dd($request);
        $employee_number = Auth::user()->employee_number;
        $input = [
            'employee_number' => $employee_number,
            'avaya-input' => $request->input('avaya-input'),
            'tier-input' => $request->input('tier-input'),
            'skill-input' => $request->input('skill-input'),
            'select-reason' => $request->input('select-reason')
        ];

        $validator = Validator::make(
            $input,
            $this->releaseCallValidationRules(),
            $this->releaseCallValidationMessages()
        );

        if ($validator->fails())
        {
            return redirect() -> route('release_call_log') -> withInput() -> withErrors($validator);
        }
        else
        {
            $callLog = new ReleaseCallLog;
            $callLog->employee_ID = $employee_number;
            $callLog->reason_ID = $request->input('select-reason');
            $callLog->save();

            return redirect() -> route('release_call_log') -> with('messageSuccess', 'Released call successfully logged.');
        }
    }

    /**
    Views
    **/

    public function rptcms()
    {
        $data = [
            'title'=>"Release Call Report - CMS"
        ];

        return view('push.release-call-log.reports_cms', $data);

    }

    public function rptsupervisor()
    {
        $data = [
            'title'=>"Release Call Report - Supervisor"
        ];

        return view('push.release-call-log.reports_supervisor', $data);

    }

    public function generatereport(Request $request)
    {
        //
       if($request->cms_start >= $request->cms_end)
        {
            echo "$('#report-logs tbody').html('Invalid date argument.'); ";
        }
        else
        {
            $report_table ='';

            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->cms_start, 'America/New_York')->timezone('Asia/Manila');
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->cms_end, 'America/New_York')->timezone('Asia/Manila');

            $logs = ReleaseCallLog::CountReasons()->TimeReport($startDate, $endDate)->get();
            
            foreach ($logs as $log) 
            {   
                #dd(date($log->created_at));
                $dateCarbon = strtotime(date($log->created_at));
                #dd(date('Y-m-d', strtotime('next sunday', $dateCarbon)));
                $agentData = Roster::RetrieveProfile($log->employee_ID)->whereAnd('week_ending', date('Y-m-d', strtotime('next sunday', $dateCarbon)))->first();
                #dd($agentData);
                $report_table .= '<tr><td>'.$log->employee_ID.'</td>'.'<td>'.$agentData->avaya.'</td>'.
                            '<td>'.$log->reason->valid_reason.'</td>'.
                            '<td>'.$log->reason_count. '</td>'.
                            '</tr>';
            }
            if($report_table == '')
            {
                echo "$('#report-logs tbody').html('<tr><td colspan=6><center>No data to return.</center></td></tr>'); ";
            }
            else
            {
                echo "$('#report-logs tbody').html(".json_encode($report_table).")";
            }                
        }   
    }

    /**
    Download RCL
    **/
    public function dlcms(Request $request)
    {
        if($request->cms_start >= $request->cms_end)
        {
            echo "$('#errorMessage').html('<div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Invalid date argument.</div>'); ";
        }
        else
        {

            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->cms_start, 'America/New_York')->timezone('Asia/Manila');
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $request->cms_end, 'America/New_York')->timezone('Asia/Manila');

            $cms_table_query =  ReleaseCallLog::whereBetween('created_at', array($startDate, $endDate));

            if(Auth::user()->ou == 'Supervisors')
            {
                $cms_table_query ->  TeamMembers();
            }
            if(Auth::user()->ou == 'IT Systems')
            {
                $cms_table_query -> TeamMembers();
            }

            $cms_table_result = $cms_table_query->get();


            #dd($cms_table_result);

            $cms_table = "Emp. No.\t"."AVAYA.\t"."Reason\t"."Logged\n";

            foreach ($cms_table_result as $report) 
            {
                $agentData = Roster::RetrieveProfile($report->employee_ID)->orderBy('week_ending', 'DESC')->first();
                $reportDate = Carbon::createFromFormat('Y-m-d H:i:s', $report->created_at, 'Asia/Manila')->timezone('America/New_York');
                $cms_table .= $report->employee_ID."\t".$agentData->avaya."\t".
                            $report->reason->valid_reason."\t".
                            $reportDate."\n";
            }
            header("Content-type: application/vnd.ms-excel charset=UTF-8; encoding=UTF-8");
            header("Content-Disposition: attachment; filename=CMSReport_ReleaseCallLog_".date('Ymdhis').".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");
            print chr(255) . chr(254) . mb_convert_encoding($cms_table, 'UTF-16LE', 'UTF-8');
        }      
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
