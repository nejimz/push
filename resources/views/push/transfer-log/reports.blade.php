@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
  <!-- Starts Container -->

  <!-- Container Content -->
  <div class="row">
    <div class="medium-6 columns">

    <form id="reports-transferlog">
      <fieldset><legend> <h4>{{ $title or '' }}</h4></legend>
      <div class="row">
          {{-- Start --}}
          <div class="medium-2 columns"><label class="inline" style="font-size: 18px;">Start</label></div>
          <div class="medium-10 columns left">
            <input type="text" id="reports-start" name="reports_start" value="{{ date('Y-m-d') }} 00:00:00" data-date-format="%m/%d/%Y" data-time-format="%H:%M:%S" data-date-time readonly="readonly"/>
          </div>
          {{-- Start --}}
        </div>

        <div class="row">
          {{-- End --}}
          <div class="medium-2 columns"><label class="inline" style="font-size: 18px;">End</label></div>
          <div class="medium-10 columns left">
             <input type="text" id="reports-end" name="reports_end" value="{{ date('Y-m-d') }} 23:59:59" data-date-format="%m/%d/%Y" data-time-format="%H:%M:%S"  data-date-time readonly="readonly"/>
          </div>
          {{-- End --}}
        </div>

        <div class="row">
          <div class="medium-2 columns">&nbsp;</div>
          <div class="medium-10 columns">
            <ul class="button-group even-2" id="reports-button">
              <li><button type="button" onclick="" class="button"><i class="fa fa-download"></i> Download</button></li>
              <li><button type="reset" class="button secondary"><i class="fa fa-undo"></i> Reset</button></li>
            </ul>
          </div>
        </div>
        </fieldset>
    </form>

    </div>
  </div>
  <!-- Container Content -->

<!-- Stops Container -->
<!-- Stops Container -->

@stop

@section('scripts')
  @include('push.scripts.transfer_log_js')
@stop