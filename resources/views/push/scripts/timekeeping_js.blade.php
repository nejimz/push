<script type="text/javascript">
	/**
		EzWatch
	**/
	var xhr_ez_employee = null;
	var xhr_ez_lilo = null;
	var xhr_ez_preview = null;

	$(function(){
		$('#ez-dept').val(0);
		$('#ez-button').hide();

		$('#ez-dept').on('change', function(e){
			e.preventDefault();
			Holder.run();
			load_ez_sect(this.value);
		});

		$('#ez-form').on('submit', function(e){
			e.preventDefault();
			Holder.run();
			ezsubmit($("#ez-lilo-url").val(), $(this).serialize());
		});

	});

	function load_ez_sect(dept)
	{
		//Ajax call here
		xhr_ez_employee =	$.ajax({
								type : 'get',
								url : $("#ez-employee-url").val(),
								cache : false,
								data: { 'department': dept },
								dataType : "html",
								beforeSend: function(xhr){
									if (xhr_ez_employee != null){
										xhr_ez_employee.abort();
									}
							}
							}).done(function(response){
								$('#ez-emp').html(response);
							});

		$('#ez-button').show();
	}

	function ezsubmit(link, data)
	{
		//Ajax call here
		xhr_ez_lilo =	$.ajax({
							type : 'get',
							url : link,
							cache : false,
							data: data,
							dataType : "script html",
							beforeSend: function(xhr){
								if (xhr_ez_lilo != null){
									xhr_ez_lilo.abort();
								}
							$('#ez-table tbody').html('<tr><td colspan=6><center>Loading...</center></td></tr>');
						}
						}).done(function(response){
							response;
						});
	}

	function ezdownload(link)
	{
		if($('#ez-start').val() == '')
		{
			$('#ez-table tbody').html('<tr><td colspan=6><div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Invalid date argument.</div></td></tr>');
	    	return false;
		}
		else if($('#ez-end').val() == '')
		{
			$('#ez-table tbody').html('<tr><td colspan=6><div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Invalid date argument.</div></td></tr>');
	    	return false;
		}
		else if($('#ez-dept').val() == 0)
		{
			$('#ez-table tbody').html('<tr><td colspan=6><div data-alert class=\'alert-box alert\' style=\'margin:0px;\'>Please fill-up the provided form.</div></td></tr>');
	    	return false;
		}
		else
		{
			var data =  'ez_start=' + $('#ez-start').val() +
						'&ez_end='  + $('#ez-end').val()   +
						'&department='  + $('#ez-edpt').val()   + 
						'&ez_emp='  + $('#ez-emp').val();
			window.open(link + '?' + $('#ez-form').serialize());
		}
	}

	function ezpreview(empid, transid)
	{
		xhr_ez_preview = $.ajax({
		 					type: 'GET',
							url: $('#ez-preview-url').val(),
							data: 'transid=' + transid + '&empid=' + empid,
							cache: false,
							dataType: 'script',
							beforeSend: function(xhr) {
								if (xhr_ez_preview != null) {
									xhr_ez_preview.abort();
								}						
							}
						}).done(function(response){
							response;
						});
	}
</script>