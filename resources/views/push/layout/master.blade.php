<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PUSH</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/token-input.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/foundation-icons.css') }}" />    
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation-calendar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation-docs.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/push.style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/token-input-facebook.css') }}" />

    <script src="{{ asset('js/vendor/modernizr.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery.js') }}"></script>
    <script src="{{ asset('js/foundation.min.js') }}"></script>    
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/vendor/holder.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery.session.js') }}"></script>
    <script src="{{ asset('js/vendor/date.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-ui.min.js') }}"></script>
   
    <script src="{{ asset('js/jquery.tokeninput.js') }}"></script>
    <script src="{{ asset('js/jquery.infinitescroll.js') }}"></script>
    <script src="{{ asset('js/jquery.tagsinput.js') }}"></script>
    <script src="{{ asset('js/foundation-calendar.min.js') }}"></script>
    <script src="{{ asset('js/helpers/date-helpers.js') }}"></script>
    <script src="{{ asset('js/helpers/string-helpers.js') }}"></script>
    <script src="{{ asset('js/jquery.titlealert.min.js') }}"></script>
    @if(App\UsersAdmin::where('ntlogin',Auth::user()->ntlogin)->count() == 0)
    <script src="{{ asset('js/scripts.security.js') }}"></script>
    <?php $security = 'onContextMenu="return false" onDragStart="return false" onSelectStart="return false" oncopy="return false" oncut="return false" onpaste="return false"'; ?>
    @endif
  </head>

  <body {{ $security or ''}}>

    <div id="pushContainer">

      <div class="large-12 columns">
          <div class="large-3 columns">
            <!--h1 style="margin-bottom: -15px;">PUSH</h1-->
            <!--p>Panasiatic Universal Support HUB</p-->
            <img src="{{ asset('img/logo1.png') }}" class="left" id="pcciLogo"> 
            <img class="left" src="{{ asset('img/loco.png') }}" id="pushLogo">
            <small class="left" id="headerText">Panasiatic Universal Support Hub</small>
          </div>
          <div class="large-6 columns">&nbsp;</div>
          <div class="large-3 columns" style="padding:0px;">
              <a class="th left" href="{{ route('profile') }}" style="margin:10px 10px;">
                <img src="{{ Auth::user()->image }}" width="64px" height="64px">
              </a>
              <p class="left" style="margin-top:20px;">
                <strong> Welcome, </strong> 
                <br/>
                {{ Auth::user()->name }}
              </p>
          </div>
      </div><!-- large-12 columns -->
    </div><!-- pushContainer -->   
   
    <div id="pushContainer" data-equalizer-watch>
        @include('push.menus.master')
      <div id="pushContent" class="large-9 columns" role="content">
        @yield('container', '')
      </div><!-- pushContent -->
    </div><!-- pushContainer -->  
   

    <footer id="pushContainer">
      <div class="large-12 columns">
        <hr/>
        <div class="row">
          <div class="large-12 columns">
            <p class="text-center" style="margin-bottom:0px;">&copy; Panasiatic Call Centers Inc. 2010-{{ date("Y") }}</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Include Popups -->
    @include('push.popups.master')  

    <input type="hidden" id="users_search_json_url" name="users_search_json_url" value="{{ route('users_search_json') }}">
    <input type="hidden" id="notification_url" name="notification_url" value="{{ route('notification_message') }}">
    <input type="hidden" id="break_schedule_notification" name="break_schedule_notification" value="{{ route('my_break_schedule_notification') }}">
    <script src="{{ asset('js/scripts.js') }}"></script>
    <!-- Only have break has the realtime request -->
    @if(Session::has('user.schedule.break'))
    <!--script src="{{ asset('js/agent.schedule.js') }}"></script-->
    @endif
    <!-- Only in the list has the notification -->
    @if(App\OusConversation::where('ou', Auth::user()->ou)->count() != 0)
        <script src="{{ asset('js/messaging.notification.js') }}"></script>
    @elseif(App\UsersConversation::where('ntlogin',Auth::user()->ntlogin)->count() != 0)
        <script src="{{ asset('js/messaging.notification.js') }}"></script>
    @endif
    <!-- Include Custom Scripts -->
    @yield('scripts', '')
  </body>
</html>
