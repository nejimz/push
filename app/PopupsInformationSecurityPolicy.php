<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupsInformationSecurityPolicy extends Model
{
    //
     public $table = 'popups_information_security_policy';
     public $timestamps = false;
}
