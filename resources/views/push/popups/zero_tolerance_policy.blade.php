<!-- Zero Tolerance Popup -->
<div id="zeroToleranceModal" class="reveal-modal medium" data-reveal aria-labelledby="zeroTolerance" aria-hidden="true" role="dialog">
  <img src="{{ asset('img/zero-tolerance/1.jpg') }}">
  <img src="{{ asset('img/zero-tolerance/2.jpg') }}">
  <img src="{{ asset('img/zero-tolerance/3.jpg') }}">
  <img src="{{ asset('img/zero-tolerance/4.jpg') }}">
  <img src="{{ asset('img/zero-tolerance/5.jpg') }}">
  <br />
  <br />
  <br />
  <div data-alert class="alert-box info">
    By clicking I Accept, you confirm that you have read the terms and conditions, that you understand them and that you agree to be bound by them.
  </div>
  <a href="{{ route('zero_tolerance') }}" class="button success" id="iAcceptZeroToleranceBtn">I Accept</a>
</div>

<script type="text/javascript">
$('#zeroToleranceModal').foundation('reveal', 'open', {
  animation_speed: 0,
  close_on_background_click: false
});
</script>