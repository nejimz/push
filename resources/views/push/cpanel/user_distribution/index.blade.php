@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
   	<div class="large-12 columns">

   		<h4><i class="fa fa-users"></i> PUSH User Distribution</h4>

   		<div class="row">
   			<a href="{{ route('user_distribution_create') }}" class="button button-small"><i class="fa fa-plus"></i> Add</a>
   		</div>

   		<form action="{{ route('user_distribution_index') }}" method="get">
			<div class="row">
				<div class="large-6 columns">
					<div class="row collapse">
						<div class="large-10 columns">
							<input name="search" type="text" placeholder="Search" value="{{ $search }}" autocomplete="off">
						</div>
						<div class="large-2 columns">
							<button class="button button-small postfix">Search</button>
						</div>
					</div>
				</div>
			</div>
   		</form>

		@include('push.messages.success_message')

		{!! str_replace('/?', '?', $users->render()) !!}
		<table width="100%">
			<thead>
				<tr>
					<th width="5%"></th>
					<th width="20%">Employee No.</th>
					<th width="20%">NT-Login</th>
					<th width="30%">Name</th>
					<th width="25%">Distribution</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td><a href="{{ route('user_distribution_destroy', [$user->ntlogin, $user->distribution_id]) . '?search=' . $search }}"><i class="fa fa-times"></i></a></td>
					<td>{{ $user->employee_number }}</td>
					<td>{{ $user->ntlogin }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->distribution }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! str_replace('/?', '?', $users->render()) !!}
   	</div>
<!-- Stops -->
@stop