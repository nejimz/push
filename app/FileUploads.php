<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileUploads extends Model
{
    //
     public $table = 'file_uploads';
     public $timestamps = true;
    
    public function type()
    {
    	return $this->hasOne('App\FileUploadTypes', 'id', 'file_type');
    }
}
