<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NetworkUtilization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('network_utilization', function (Blueprint $table) {
            $table->increments('id')->unique;

            $table->integer('inbound')->unsigned();
            $table->integer('outbound')->unsigned();

            $table->datetime('date')->unsigned();
            
            #$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
