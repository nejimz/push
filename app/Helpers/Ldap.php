<?php namespace App\Helpers;

use App\Ous;
use App\Distributions;

class Ldap {
	public static function hosts()
	{
		return env('LDAP_HOST', '10.249.143.6 10.249.143.8 10.249.143.9');
	}

	public static function ldapenv()
	{
		if(getenv('NTLM_USERNAME'))
		{
			return getenv('NTLM_USERNAME');
		}
		else
		{
			$authUser = explode("\\",getenv('AUTH_USER'));
			return utf8_encode($authUser[1]);
		}
	}

	public static function connect($username='')
	{

		if(empty($username))
		{
	   		$username = self::ldapenv();
		}

		$data = "";
		$host = self::hosts();
		$ADuser = 'cHVzaGFkbWluQHBhbmFzaWF0aWNzb2x1dGlvbnMubmV0';
		$ADpass = 'cEBzc3cwcmQ=';

		#	Connect
		$ldap_connection = ldap_connect($host);
		ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
    	ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);

		$ldap_connection_bind = @ldap_bind($ldap_connection, base64_decode($ADuser), base64_decode($ADpass));

		#	If NT Login exist
		if ($ldap_connection_bind)
		{
			$filter="(sAMAccountName=$username)";
        	$result = ldap_search($ldap_connection,"dc=panasiaticsolutions,dc=net",$filter);

			ldap_sort($ldap_connection,$result,"sn");

	        $info = ldap_get_entries($ldap_connection, $result);

	        $infoResults = array();

		    for ( $i = 0; $i < $info['count']; $i++ ) 
		    {
		    	if($info['count'] > 1)
	        	break;
		        $infoResults['cn']						= (isset($info[$i]['cn'][0])) ? $info[$i]['cn'][0] : '';
		        $infoResults['sn']						= (isset($info[$i]['sn'][0])) ? $info[$i]['sn'][0] : '';
		        $infoResults['title']					= (isset($info[$i]['title'][0])) ? $info[$i]['title'][0] : '';
		        $infoResults['description']				= (isset($info[$i]['description'][0])) ? $info[$i]['description'][0] : '';
	        	$infoResults['initials']				= (isset($info[$i]['initials'][0])) ? $info[$i]['initials'][0] : '';
	        	$infoResults['distinguishedname']		= (isset($info[$i]['distinguishedname'][0])) ? $info[$i]['distinguishedname'][0] : '';
	        	$infoResults['instancetype']			= (isset($info[$i]['instancetype'][0])) ? $info[$i]['instancetype'][0] : '';
	        	$infoResults['whencreated']				= (isset($info[$i]['whencreated'][0])) ? $info[$i]['whencreated'][0] : '';
	        	$infoResults['whenchanged']				= (isset($info[$i]['whenchanged'][0])) ? $info[$i]['whenchanged'][0] : '';
	        	$infoResults['displayname']				= (isset($info[$i]['displayname'][0])) ? $info[$i]['displayname'][0] : '';
	        	$infoResults['info']					= (isset($info[$i]['info'][0])) ? $info[$i]['info'][0] : '';
	        	$infoResults['memberof']				= (isset($info[$i]['memberof'][0])) ? $info[$i]['memberof'][0] : '';
	        	$infoResults['department']				= (isset($info[$i]['department'][0])) ? $info[$i]['department'][0] : '';
	        	$infoResults['members']					= (isset($info[$i]['members'][0])) ? $info[$i]['members'][0] : '';
	        	$infoResults['employeenumber']			= (isset($info[$i]['employeenumber'][0])) ? $info[$i]['employeenumber'][0] : '';
	        	$infoResults['name']					= (isset($info[$i]['name'][0])) ? $info[$i]['name'][0] : '';
	        	$infoResults['objectguid']				= self::GUID($info[$i]['objectguid'][0]);
	        	$infoResults['employeeid']				= (isset($info[$i]['employeeid'][0])) ? $info[$i]['employeeid'][0] : '';
	        	$infoResults['objectsid']				= self::SID($info[$i]['objectsid'][0]);
	        	$infoResults['samaccountname']			= (isset($info[$i]['samaccountname'][0])) ? $info[$i]['samaccountname'][0] : '';
	        	$infoResults['samaccounttype']			= (isset($info[$i]['samaccounttype'][0])) ? $info[$i]['samaccounttype'][0] : '';
	        	$infoResults['userprincipalname']		= (isset($info[$i]['userprincipalname'][0])) ? $info[$i]['userprincipalname'][0] : '';
	        	$infoResults['objectcategory']			= (isset($info[$i]['objectcategory'][0])) ? $info[$i]['objectcategory'][0] : '';
	        	$infoResults['mail']					= (isset($info[$i]['mail'][0])) ? $info[$i]['mail'][0] : '';
	        	$infoResults['thumbnailphoto']			= (isset($info[$i]['thumbnailphoto'][0])) ? 'data:image/jpeg;base64,' . base64_encode($info[$i]['thumbnailphoto'][0]) : '';
		    	#$infoResults['proxyaddresses']			= $info[$i]['proxyaddresses'][0];
		    	#$infoResults['useraccountcontrol']		= $info[$i]['useraccountcontrol'][0];
	        	#$infoResults['badpwdcount']			= $info[$i]['badpwdcount'][0];
	        	#$infoResults['codepage']				= $info[$i]['codepage'][0];
	        	#$infoResults['countrycode']			= $info[$i]['countrycode'][0];
	        	#$infoResults['homedirectory']			= $info[$i]['homedirectory'][0];
	        	#$infoResults['homedrive']				= $info[$i]['homedrive'][0];
	        	#$infoResults['badpasswordtime']		= $info[$i]['badpasswordtime'][0];
	        	#$infoResults['pwdlastset']				= $info[$i]['pwdlastset'][0];
	        	#$infoResults['primarygroupid']			= $info[$i]['givenname'][0];
	        	#$infoResults['accountexpires']			= $info[$i]['givenname'][0];
	        	#$infoResults['logoncount']				= $info[$i]['accountexpires'][0];
	        	#$infoResults['showinaddressbook']		= $info[$i]['showinaddressbook'][0];
	        	#$infoResults['legacyexchangedn']		= $info[$i]['legacyexchangedn'][0];
	        	#$infoResults['lockouttime']			= $info[$i]['lockouttime'][0];
	        	#$infoResults['dscorepropagationdata']	= $info[$i]['dscorepropagationdata'][0];
	        	#$infoResults['lastlogontimestamp']		= $info[$i]['lastlogontimestamp'][0];
	        	#$infoResults['manager']				= $info[$i]['manager'][0];
		    }

		    return $infoResults;

	        @ldap_close($ldap_connection);

		}
		else
		{
			echo "PUSHUser account error.";
		}		
	}

	public static function checkEmpty($request){
		return ( isset( $request ) ? $request : '' );
	}

	public static function search($search='')
	{
		$data = "";
		$host = self::hosts();
		$ADuser = 'cHVzaGFkbWluQHBhbmFzaWF0aWNzb2x1dGlvbnMubmV0';
		$ADpass = 'cEBzc3cwcmQ=';

		#	Connect
		$ldap_connection = ldap_connect($host);
		ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
    	ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);

		$ldap_connection_bind = @ldap_bind($ldap_connection, base64_decode($ADuser), base64_decode($ADpass));

		#	If NT Login exist
		if ($ldap_connection_bind)
		{
			$filter =   "(&(objectClass=User)".		 						 
        				"(!(objectClass=Computer))". 						 
        				"(!(userAccountControl:1.2.840.113556.1.4.803:=2))". 
        				"(!(name=*test*))". 								
        				"(!(name=*server*))". 								 
        				"(!(name=*domain*))". 								
        				"(!(name=bac-fwacl*))". 							 
        				"(!(name=*tlprofile*))". 
        				"(!(name=*-RW-*))". 	
        				"(!(samaccountname=a-*))". 							
        				"(|(name=*".$search."*)(samaccountname=*".$search."*)(givenname=*".$search."*)))";
        	$result = ldap_search($ldap_connection,"DC=panasiaticsolutions,DC=net",$filter);
			ldap_sort($ldap_connection,$result,"name");

	        $info = ldap_get_entries($ldap_connection, $result);

	      #  print_r($info);exit;
			$results = [];

			for ($i=0; $i < $info["count"]; $i++) {
				
				$cnName = explode(',', $info[$i]["dn"]);

				if(isset($info[$i]['samaccountname'][0]))
				{
					$results[] = array( 
						'id'	=> $info[$i]['samaccountname'][0].";User", 
						'name'	=> str_replace("\\",'',substr($cnName[0], 3)),
						'type'	=> 'User'
					);
				}
				else
				{
					$results[] = array( 
						'id'	=> substr($cnName[0], 3).";User", 
						'name'	=> substr($cnName[0], 3),
						'type'	=> 'User' 
					);
				}
			}

			$list = Distributions::select('distribution')
						->where('distribution','LIKE','%'.$search.'%')
						->take(10)
						->get();

			 foreach ($list as $value) {
				$results[] = array( 
					'id'	=> $value->distribution.";Group", 
			 		'name'	=> $value->distribution,
			 		'type'	=> 'Group' 
			 	);
			 }			

			// $filter1 =   "(&(objectClass=Group)".		 					 #Check if  Group
   //      				"(!(objectClass=Computer))". 						 #Must not be a Computer
   //      				"(!(userAccountControl:1.2.840.113556.1.4.803:=2))". #Must not be disabled
   //      				"(!(name=*test*))". 								 #Name must not containg string 'test'
   //      				"(!(name=*server*))". 								 #Name must not containg string 'server'
   //      				"(!(name=*domain*))". 								 #Name must not containg string 'domain'
   //      				"(!(name=bac-fwacl*))". 							 #Name must not containg string 'bac-fwacl'
   //      				"(!(name=*-RW-*))". 								 #Name must not containg string '-RW-'
   //      				"(|(name=*".$search."*)(samaccountname=*".$search."*)(givenname=*".$search."*)))";
   //      	$result1 = ldap_search($ldap_connection,"DC=panasiaticsolutions,DC=net",$filter1);
			// ldap_sort($ldap_connection,$result1,"name");

	  //       $info1 = ldap_get_entries($ldap_connection, $result1);

			// for ($i=0; $i < $info1["count"]; $i++) {
				
			// 	$cnName1 = explode(',', $info1[$i]["dn"]);

			// 	if(isset($info1[$i]['samaccountname'][0]))
			// 	{
			// 		$results[] = array( 
			// 			'id'	=> $info1[$i]['samaccountname'][0] , 
			// 			'name'	=> str_replace("\\",'',substr($cnName1[0], 3)),
			// 			'type'	=> 'Group'
			// 		);
			// 	}
			// 	else
			// 	{
			// 		$results[] = array( 
			// 			'id'	=> substr($cnName1[0], 3) , 
			// 			'name'	=> substr($cnName1[0], 3),
			// 			'type'	=> 'Group' 
			// 		);
			// 	}
			// }
			#print_r($results);exit;
			#return json_encode($results);
			return $results;

        	@ldap_close($ldap_connection);

		}
		else
		{
			echo "PUSHUser account error.";
		}		
	}

	public static function SID($entry)
	{
		$sid = "S-";

		$start;

		// Convert Bin to Hex and split into byte chunks
		$sidinhex = str_split(bin2hex($entry), 2);

		// Byte 0 = Revision Level
		$sid = $sid.hexdec($sidinhex[0])."-";

		// Byte 1-7 = 48 Bit Authority
		$sid = $sid.hexdec($sidinhex[6].$sidinhex[5].$sidinhex[4].$sidinhex[3].$sidinhex[2].$sidinhex[1]);

		// Byte 8 count of sub authorities - Get number of sub-authorities
		$subauths = hexdec($sidinhex[7]);

		//Loop through Sub Authorities
		for($i = 0; $i < $subauths; $i++) {
		    $start = 8 + (4 * $i);
		    // X amount of 32Bit (4 Byte) Sub Authorities
		    $sid = $sid."-".hexdec($sidinhex[$start+3].$sidinhex[$start+2].$sidinhex[$start+1].$sidinhex[$start]);
		}

		return $sid;
	}

	public static function GUID($entry){
	   $guidinhex = str_split(bin2hex($entry), 2);
	   $guid = "";
	   //Take the first 4 octets and reverse their order
	   $first = array_reverse(array_slice($guidinhex, 0, 4));
	   foreach($first as $value)
	   {
	      $guid .= $value;
	   }
	   $guid .= "-";
	   //Take the next two octets and reverse their order
	   $second = array_reverse(array_slice($guidinhex, 4, 2, true), true);
	   foreach($second as $value)
	   {
	      $guid .= $value;
	   }
	   $guid .= "-";
	   //Repeat for the next two
	   $third = array_reverse(array_slice($guidinhex, 6, 2, true), true);
	   foreach($third as $value)
	   {
	      $guid .= $value;
	   }
	   $guid .= "-";
	   //Take the next two but do not reverse
	   $fourth = array_slice($guidinhex, 8, 2, true);
	   foreach($fourth as $value)
	   {
	      $guid .= $value;
	   }
	   $guid .= "-";
	   //Take the last part
	   $last = array_slice($guidinhex, 10, 16, true);
	   foreach($last as $value)
	   {
	      $guid .= $value;
	   }
	   return $guid;
	}

	public static function RequestUsers(){
		$host = self::hosts();

		if(empty($username))
		{
	   		$username = self::ldapenv();
		}

		$ADuser = 'cHVzaGFkbWluQHBhbmFzaWF0aWNzb2x1dGlvbnMubmV0';
		$ADpass = 'cEBzc3cwcmQ=';
	
		#	Connect
		$ldap_connection = ldap_connect($host);
		ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
    	ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
		$ldap_connection_bind = @ldap_bind($ldap_connection, base64_decode($ADuser), base64_decode($ADpass));
		#	If NT Login exist

		if ($ldap_connection_bind)
		{
			$dn = "DC=panasiaticsolutions,DC=net"; 
			$filter = "(&(objectClass=Group)(proxyAddresses=*))";
			$justthese = array("proxyaddresses","dn");
			$sr=ldap_search($ldap_connection, $dn, $filter, $justthese);
			ldap_sort($ldap_connection, $sr, 'dn');

			$info = ldap_get_entries($ldap_connection, $sr);

			$results = [];

			for ($i=0; $i < $info["count"]; $i++) {
				$cnName = explode(',', $info[$i]["dn"]);
			    $results[] = substr($cnName[0], 3);
			}
			return $results;

        	@ldap_close($ldap_connection);

		}
		else
		{
			echo "<b>SYSTEM ACCESS ERROR</b> please inform ITProgrammers.";
		}
	}

	public static function ldapOus($request){
		$host = self::hosts();


		$ADuser = 'cHVzaGFkbWluQHBhbmFzaWF0aWNzb2x1dGlvbnMubmV0';
		$ADpass = 'cEBzc3cwcmQ=';
	
		#	Connect
		$ldap_connection = ldap_connect($host);
		ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
    	ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
		$ldap_connection_bind = @ldap_bind($ldap_connection, base64_decode($ADuser), base64_decode($ADpass));
		#	If NT Login exist

		if ($ldap_connection_bind)
		{
			$dn = "DC=panasiaticsolutions,DC=net";
			$filter="(&(|(objectClass=User))(!(objectClass=Computer))(!(userAccountControl:1.2.840.113556.1.4.803:=2))(!(name=*test*))(!(name=*server*))(!(name=*domain*))(!(name=bac-fwacl*))(!(name=*-RW-*))(memberOf=CN=$request,OU=Distribution Group,OU=Domain Administrators,DC=panasiaticsolutions,DC=net))";
			$sr=ldap_search($ldap_connection, $dn, $filter);
			$info = ldap_get_entries($ldap_connection, $sr);

			$printThis = '';
		
				for ($i=0; $i < $info["count"]; $i++) 
				{
					if($info['count'] > 1)
		        	break;
					echo $info[$i]['samaccountname'][0];
				}
			
			return $printThis;
		}
		else
		{
			echo "<b>SYSTEM ACCESS ERROR</b> please inform ITProgrammers.";
		}
	}

}

?>