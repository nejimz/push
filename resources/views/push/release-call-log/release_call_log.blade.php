@extends('push.layout.master')

@section('container')
@parent
<!-- Starts Container -->


        <!-- Container Content -->
<div class="row">
  <div class="medium-10 medium-centered columns">

    <div class="row">
      <div class="large-12 columns">
      @if($errors->has())
        <div data-alert class="alert-box warning">
          @foreach($errors->all() as $error)
              {{ $error }}<br />
          @endforeach
          <a href="#" class="close">&times;</a>
        </div>
      @elseif(Session::has('messageSuccess'))
        <div data-alert class="alert-box success">
              {{ Session::get('messageSuccess') }}
          <a href="#" class="close">&times;</a>
        </div>
      @endif
      </div>
    </div>

  <form id="releaseCallLog" action="{{ route('release_call_store') }}" method="post" style="margin-top:20px;">
    <fieldset>

      <legend><h4>{{ $title or '' }}</h4></legend>

    <div class="row">

      <div class="small-2 columns">
        <label for="avaya-input" class="right inline">Avaya # :</label>
      </div>
      <div class="small-3 columns left">
        <input type="text" id="avaya-input" name="avaya-input" readonly="readonly" value="{{ $avaya }}">
      </div>
   
      <div class="small-2 small-offset-1 columns">
        <label for="tier-input" class="right inline">Tier :</label>
      </div>
      <div class="small-3 columns left">
        <input type="text" id="tier-input" name="tier-input" readonly="readonly" value="{{ $tier }}">
      </div>

    </div>

    <div class="row">

      <div class="small-2 columns">
        <label for="supervisor-input" class="right inline">Supervisor :</label>
      </div>
      <div class="small-3 columns left">
        @if($supervisor == "")
        <input type="text" id="supervisor-input" name="supervisor-input" readonly="readonly" value="">
        @else
        <input type="text" id="supervisor-input" name="supervisor-input" readonly="readonly" value="{{ $supervisor->lastName . ', ' .$supervisor->firstName . ' ' . $supervisor->suffix }}">
        @endif
      </div>

      <div class="small-2 small-offset-1 columns">
        <label for="skill-input" class="right inline">Skill :</label>
      </div>
      <div class="small-3 columns left">
        <input type="text" id="skill-input" name="skill-input" readonly="readonly" value="{{ $skill }}">
      </div>

    </div>

    <div class="row">
      <div class="small-2 columns">
        <label for="select-reason" class="right inline">Reason :</label>
      </div>
      <div class="small-9 columns left">
        <select id="select-reason" name="select-reason">
          <option value="0"> - - S E L E C T - - </option>
          @if($reasons != "")
            @foreach ($reasons as $reason) 
              <option value="{{ $reason->reason_ID }}"> {{ $reason->valid_reason }} </option>          
            @endforeach
          @endif
        </select>
      </div>
    </div>

    <div class="row">
      <div class="small-4 small-offset-8 columns">
        <button type="reset" class="button secondary">Clear</button>
        <button type="submit" class="button success">Save</button>
      </div>
    </div>

        {!! csrf_field() !!}

    </fieldset>
  </form>

  </div>
</div>
        <!-- Container Content -->

<!-- Stops Container -->
@stop