@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
   	<div class="medium-12 columns">

   		<h4><i class="fa fa-users"></i> PUSH Roster</h4>

   		<form action="{{ route('users_index') }}" method="get">
			<div class="row">
				<div class="large-6 columns">
					<div class="row collapse">
						<div class="large-10 columns">
							<input name="search" type="text" placeholder="Search" value="{{ $search }}" autocomplete="off">
						</div>
						<div class="large-2 columns">
							<button class="button button-small postfix">Search</button>
						</div>
					</div>
				</div>
			</div>
   		</form>

		{!! str_replace('/?', '?', $agents->appends(['search'=>$search])) !!}
		<table width="100%">
			<thead>
				<tr>
					<th width="15%">Employee No.</th>
					<th width="10%">Avaya</th>
					<th width="35%">Name</th>
					<th width="20%">Organization</th>
					<th width="15%">WP</th>
					<th width="5%">Tier</th>
				</tr>
			</thead>
			<tbody>
				@foreach($agents as $agent)
				<tr>
					<td>{{ $agent->employee_ID }}</td>
					<td>{{ $agent->avaya }}</td>
					<td>{{ $agent->name }}</td>
					<td>{{ $agent->organization }}</td>
					<td>{{ $agent->work_pattern }}</td>
					<td>{{ $agent->tier }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		{!! str_replace('/?', '?', $agents->appends(['search'=>$search])) !!}
   	</div>
<!-- Stops -->
@stop