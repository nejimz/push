@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

	<!--{{ Auth::user()->employee_number }}-->

	<!-- Container Title -->
	<div class="row">
	  <div class="medium-12 columns">
		<div class="row">
			<div class="medium-12 columns">
				<dl class="sub-nav">
					<dt><i class="fa fa-line-chart"></i> {{$specMetric}}</dt>
				</dl>
			</div>
		</div>
		<!-- Tabs -->
	  </div>
	</div>
	<!-- Container Title -->

	<div class="row">
		<div class="medium-5 columns">
			{{-- Form --}}
			<form id="metricForm">
				<fieldset>
    			<legend>2015 PB Calendar - Fourth Quarter</legend>
    			<input type="hidden" id="metric" value="{{ $specMetric }}">
					{{-- Month --}}
				<div class="row">
					<div class="medium-7 columns">
					  <select id="metric_mm" name="metric_mm">
					    <option value = "1">January</option>
                        <option value = "2">February</option>
                        <option value = "3">March</option>
                        <option value = "4">April</option>
                        <option value = "5">May</option>
                        <option value = "6">June</option>
                        <option value = "7">July</option>
                        <option value = "8">August</option>
                        <option value = "9">September</option>
                        <option value = "10">October</option>
                        <option value = "11">November</option>
                        <option value = "12">December</option>
					  </select>
					</div>
					{{-- Month --}}

					{{-- WE --}}
					<div class="medium-5 columns" id="weekEndings">
					  <select id="metric_we" name="metric_we">
                        <option value = "15">15</option>
                        <option value = "30">30</option>
					  </select>
					</div>
					{{-- WE --}}
            </form>
            {{-- Form --}}
            </div>
            <div class="row">
	            <div class="large-12 columns">
				  <ul class="button-group small expand radius toggle" data-toggle="buttons-radio" style="text-align: center;">
					<li style="display: inline-block;">
						<input type="radio" id="r1" name="r-group" data-toggle="button">
						<label class="button" for="r1">Week 1</label>
					</li>
					<li style="display: inline-block;">
						<input type="radio" id="r2" name="r-group" data-toggle="button">
						<label class="button" for="r2">Week 2</label>
					</li>
					<li style="display: inline-block;">
						<input type="radio" id="r3" name="r-group" data-toggle="button">
						<label class="button" for="r3">Week 3</label>
					</li>
				  </ul>
				</div>
			</div>
        </div>

		<div class="medium-7 columns condensed-table">
			<table>
																<tr><th></th>		<th>Target</th>		<th>Weight</th>
																	<th></th>		<th>Target</th>		<th>Weight</th></tr>
				<tr><th style="background-color: #EEEEEE;" width="10%">QA</th>		<td>92.0%</td>		<td style="font-weight:bold;">00.00</td>
					<th style="background-color: #EEEEEE;" width="10%">Transfer</th><td>20.0%</td>		<td style="font-weight:bold;">00.00</td>
				<tr><th style="background-color: #EEEEEE;" width="10%">SchedAd</th>	<td>90.0%</td>		<td style="font-weight:bold;">00.00</td>
					<th style="background-color: #EEEEEE;" width="10%">ORI</th>		<td>100.0%</td>		<td style="font-weight:bold;">00.00</td></tr>
				<tr><th style="background-color: #EEEEEE;" width="10%">Attendance</th><td>00.00</td>	<td style="font-weight:bold;">00.00</td>
					<th style="background-color: #EEEEEE;" width="10%">SCORE</th>	<td colspan="2" style="font-weight:bold;">100.00</td></tr>
			</table>
		</div>
	</div>

	<div class="row">
		<!-- Tab Content -->
		<div class="medium-12 columns">
			  <div class="" id="weekly">
			    <table id="weeklyTab" width="100%">
					<thead>
						<tr>
						  <th class="centerHeader" rowspan="2"></th>
						  <th class="centerHeader" rowspan="2">MON</th>
						  <th class="centerHeader" rowspan="2">TUE</th>
						  <th class="centerHeader" rowspan="2">WED</th>
						  <th class="centerHeader" rowspan="2">THUR</th>
						  <th class="centerHeader" rowspan="2">FRI</th>
						  <th class="centerHeader" rowspan="2">SAT</th>
						  <th class="centerHeader" rowspan="2">SUN</th>
						  <th class="centerHeader" colspan="3">WTD</th>
						</tr>
						<tr>
						  <th class="centerHeader">Orig</th>
						  <th class="centerHeader">Score</th>
						  <th class="centerHeader">Pts</th>
						</tr>
					</thead>
					<tbody>
						<tr>
						  <th style="text-align: left;">QA</th>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						</tr>

						<tr>
						  <th style="text-align: left;">Transfer Rate</th>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						</tr>

						<tr>
						  <th style="text-align: left;">Schedule Adherence</th>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						</tr>

						<tr>
						  <th style="text-align: left;">Attendance</th>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						</tr>

						<tr>
						  <th style="text-align: left;">ORI</th>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						  <td style="text-align: center;">00.00</td>
						</tr>
					</tbody>
				<tbody id="body_weekly" class="metricTableBody" ></tbody>
				</table>
			  </div>
		</div>
		<!-- Tab Content -->

<!-- Stops -->
@stop

@section('scripts')
  @include('push.scripts.metrics_js')
@stop