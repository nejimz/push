@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
    <div class="large-12 columns">

      <h4><i class="fa fa-users"></i> PUSH Add Special Permission</h4>

      @include('push.messages.error_message_list')
      @include('push.messages.success_message')

      <form  action="{{ route('users_special_store') }}" method="post">

        <div class="row">
          <div class="large-6 columns">
            <input id="ntlogin_search" name="ntlogin" type="text" placeholder="Ntlogin" value="{{ old('ntlogin') }}" autocomplete="off" required autofocus />
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <select id="type" name="type">
                <option value="" selected="selected">-- Select --</option>
              @foreach($types as $type)
                <option value="{{ ucwords($type) }}" {{ ((old('type') == $type)? 'selected="selected"' : '') }}>{{ ucwords($type) }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <div class="large-6 columns">
            <button class="button">Add</button>
            <a href="{{ route('users_special') }}" class="button secondary">Back</a>
          </div>
        </div>

        {!! csrf_field() !!}
      </form>
    </div>
<!-- Stops -->
@stop