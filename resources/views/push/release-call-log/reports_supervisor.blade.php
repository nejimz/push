@extends('push.layout.master')

@section('container')
@parent
<!-- Starts Container -->

  <!-- Container Content -->
  <div class="row">
    <div class="medium-6 columns">

    <form id="reports-CMS">
      <fieldset><legend> <h4>{{ $title or '' }}</h4></legend>
      <div class="row">
          {{-- Start --}}
          <div class="medium-2 columns"><label class="inline" style="font-size: 18px;">Start</label></div>
          <div class="medium-10 columns left">
            <input type="text" id="cms-start" name="cms_start" value="{{ date('Y-m-d') }} 00:00:00" data-date-format="%m/%d/%Y" data-time-format="%H:%M:%S" data-date-time readonly="readonly"/>
          </div>
          {{-- Start --}}
        </div>

        <div class="row">
          {{-- End --}}
          <div class="medium-2 columns"><label class="inline" style="font-size: 18px;">End</label></div>
          <div class="medium-10 columns left">
             <input type="text" id="cms-end" name="cms_end" value="{{ date('Y-m-d') }} 23:59:59" data-date-format="%m/%d/%Y" data-time-format="%H:%M:%S"  data-date-time readonly="readonly"/>
          </div>
          {{-- End --}}
        </div>

        <div class="row">
          <div class="medium-2 columns">&nbsp;</div>
          <div class="medium-10 columns">
            <ul class="button-group even-2" id="cms-button">
              <li><button type="button" onclick="cmsdownload('{{ route('download_CMS') }}')" class="button"><i class="fa fa-download"></i> Download</button></li>
              <li><button type="submit" class="button success"><i class="fa fa-check"></i> Generate</button></li>
            </ul>
          </div>
        </div>
        </fieldset>
        <input type="hidden" id="sup-reports-url" value="{{ route('generate_report') }}"/>
    </form>

    </div>

  </div>

  <div class="row">

  <div class="large-12 large-centered columns">

    <table id="report-logs" width="100%">
      <thead><th width="13%" style="font-size:12px;">Employee ID</th><th width="9%">AVAYA</th><th width="70%">Reason</th><th width="10%">Count</th></thead>
      <tbody>
      </tbody>
    </table>

  </div>

  </div>
  <!-- Container Content -->

<!-- Stops Container -->
@stop


@section('scripts')
  @include('push.scripts.release_call_log_js')
@stop