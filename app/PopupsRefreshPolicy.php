<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupsRefreshPolicy extends Model
{
    //
     public $table = 'popups_refresh_policy';
     public $timestamps = false;
}
