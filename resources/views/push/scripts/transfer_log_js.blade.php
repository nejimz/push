<script type="text/javascript">

	var xhr_populate_calls = null;
	var xhr_populate_extDept = null;
	var xhr_store_transfer_log = null;

$(function(){
	$("#accountinput").on('change',function(){
		var acct = $("#accountinput").val();
		window.location.assign(window.location.pathname+'?acctID='+acct);
	});
	$("#agentskillinput").on('change',function(){
		var acct = $("#accountinput").val();
		var skill = $("#agentskillinput").val();
		window.location.assign(window.location.pathname+'?acctID='+acct+'&skillID='+skill);
	});
	$("#transactionType").on('change',function(e){
		e.preventDefault();
		ShowCalls();
	});
	$("#callsreceived").on('change',function(e){
		e.preventDefault();
		ShowExtDept();
	});
	
	function ShowCalls() {
		var trans = $("#transactionType").val();
		xhr_populate_calls = $.ajax({
			type : 'get',
			url : $("#changeCallReceived").val(),
			cache : false,
			data: { transID : trans },
			dataType : "html",
			beforeSend: function(xhr){

				if (xhr_populate_calls != null){
					xhr_populate_calls.abort();
				}
		}
		}).done(function(response){
			$('#callsreceived').html(response);
		});
	}

	
	function ShowExtDept() {
		var call = $("#callsreceived").val();
		xhr_populate_extDept = $.ajax({
			type : 'get',
			url : $("#showExtDept").val(),
			cache : false,
			data: { callId : call },
			dataType : "html",
			beforeSend: function(xhr){
				if (xhr_populate_extDept != null){
					xhr_populate_extDept.abort();
				}
		}
		}).done(function(response){
			$('#extdeptinput').val(response);
		});
	}

});

</script>