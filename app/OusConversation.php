<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OusConversation extends Model
{
    //
    public $table = 'ous_conversation';
    public $timestamps = false;
}
