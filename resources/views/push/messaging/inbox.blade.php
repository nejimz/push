@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
		
		<div class="row">
        	<div class="medium-8 columns">
        		<h4><i class="fa fa-inbox"></i> Inbox</h4>
        	</div>
        </div><!-- row  # -->
        <div class="row">
		    <div class="medium-4 columns">
				<div class="row collapse">
					<div class="small-12 columns">
						<form action="{{ route('inbox') }}" id="search-conversation">
							<input type="text" placeholder="Search by subject" name="search" value="{{ $search or '' }}" />
							<input type="hidden" name="sort" value="{{ Input::get('sort', '') }}" />
						</form>
					</div>
				</div>
			</div>

			 <div class="medium-7 columns" style="font-size:11pt;">	
			 		<span class="right">
			 			<a title="Compose" href="{{ route('compose_message') }}" title="Compose Message"><i class="fa fa-envelope"></i> Compose</a>
			 		</span>
			 		<span class="right" id="reply-button"></span>
			 		<a class="right hide" id="reply-button" title="Reply" href="{{ route('reply_message', [$conversation_id, $conversation_member_id, str_random(40)]) }}" style="margin-right:20px;"><i class="fa fa-mail-reply"></i> Reply</a> 
				@if(!is_null($conversation_id))
					@if(count($my_message) > 0)
						<a class="right" title="Reply" href="{{ route('reply_message', [$conversation_id, $conversation_member_id, str_random(40)]) }}" style="margin-right:20px;"><i class="fa fa-mail-reply"></i> Reply</a> 
						<!--a class="" title="Forward" href="#forward"><i class="fa fa-mail-forward"></i> Forward</a-->
					@endif
				@endif
			</div>
		</div>
		<div class="row" data-equalizer>
			<div class="medium-4 columns" data-equalizer-watch style="height:620px; margin-bottom:20px;">
				<div class="row">
					<div class="large-12 columns">
						<a href="{{ route('inbox', array_merge(Input::all(), ['sort'=>'unread'])) }}" class="message-sort">Unread</a>
						<a href="{{ route('inbox', Input::except('sort')) }}" class="message-sort">All</a>
					</div>
				</div>
				<div class="panel out-panel" id="thread-list">
					<div class="row" id="my_messages">
						<ul id="items" style="list-style:none; margin:2px 0px 0px 3px;">
						@if($my_inbox->count() == 0)
							<li class="item centered">
								<a class="button secondary expand" href="#" style="margin-bottom: 0px; border-bottom:1px solid #fff;">
									<strong>No result.</strong>
								</a>
							</li>
						@endif
						<?php $count = 1; ?>
						<!-- Message Layout Start-->
						@foreach($my_inbox as $row)

						<?php
							$no_cache = str_random(40);
							$user = App\ConversationMessages::select([
											'users.ntlogin AS recepientsNTLogin',
											'users.name AS recepientsName'
										])
									->leftJoin('users','conversation_messages.ntlogin','=','users.ntlogin')
									->where('conversation_id', $row->convo_id)
									->orderBy('created_at','DESC')
									->first();

							$url_data['conversation_id'] = $row->convo_id;

							$url_data['conversation_member_id'] = $row->convo_member_id;

							$url_data['cache'] = $no_cache;

							$inbox_item_url = route('showmessages', $url_data);				
							
							$read_status = ($row->last_viewed == 0) ? 'unread_item': 'read_item';

						?>
							<li class="item" data-tooltip aria-haspopup="true" class="has-tip" title="{{ $row->convo_subj }}">
								<a class="button secondary expand show-message {{$read_status}}" href="{{ $inbox_item_url }}" style="margin-bottom: 0px; border-bottom:1px solid #fff;">
									<i class="left thread-envelope"></i>
									<span class="left thread-details-name">
										<strong>
											{{ $row->name }}
										</strong>
									</span>
									<br />
									<!--small><em class="left thread-details">{{ date('D, m/d/Y H:i:s', strtotime($row->last_reply)) }}</em></small-->
									<small title="{{ date('Y-m-d h:i:s',strtotime($row->last_reply)) }}"><em class="left thread-details">{{ \Carbon\Carbon::parse($row->last_reply)->diffForHumans() }}</em></small>
									<br />
									<span class="left thread-details">{{ $row->convo_subj }}</span>
								</a>
								<a class="close delete-message" href="{{ route('delete_message', $row->convo_member_id) }}">&times;</a>
								<?php $active = ''; ?>
							</li>							
						@endforeach
						<!-- Message Layout End-->
						</ul>

			<?php Input::merge( array( 'search' => urlencode(Input::get('search') ) ) ); ?>
			<span style="display:none;">{!! str_replace('/?','?',$my_inbox->appends(Input::except('page'))->render()) !!}</span>

					</div>
				</div>
				<p></p>
			</div>

			<div class="medium-8 columns" data-equalizer-watch>
				<div class="panel out-panel" id="message-list"></div>
			</div>
		</div>
<!-- Stops -->
@stop

@section('scripts')
  @include('push.scripts.conversation_js')
@stop