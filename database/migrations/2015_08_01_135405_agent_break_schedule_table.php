<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgentBreakSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('agent_break_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_number', 100)->index();
            $table->integer('avaya')->index();
            //  Shift Date
            $table->date('shift_date');
            $table->tinyInteger('shift_date_accept')->default(0);   //  Accept Yes/No
            //  Week Ending
            $table->date('week_ending');
            $table->tinyInteger('week_ending_accept')->default(0);  //  Accept Yes/No
            $table->datetime('week_ending_accepted_at')->nullable()->default(null);//  Timestamp whole WE schedule accept
            //  Break 1
            $table->time('break_1_start')->nullable()->default(null);
            $table->time('break_1_end')->nullable()->default(null);
            $table->tinyInteger('break_1_accept')->default(0);
            $table->datetime('break_1_accepted_at')->nullable()->default(null);
            //  Break 2
            $table->time('break_2_start')->nullable()->default(null);
            $table->time('break_2_end')->nullable()->default(null);
            $table->tinyInteger('break_2_accept')->default(0);
            $table->datetime('break_2_accepted_at')->nullable()->default(null);
            //  Break 3
            $table->time('break_3_start')->nullable()->default(null);
            $table->time('break_3_end')->nullable()->default(null);
            $table->tinyInteger('break_3_accept')->default(0);
            $table->datetime('break_3_accepted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('agent_break_schedule');
    }
}
