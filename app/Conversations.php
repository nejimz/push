<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Conversations extends Model
{
    //
    public $table = 'conversations';
    public $timestamps = false;
   /**
    Members
    **/
    public function members()
    {
    	return $this->belongsToMany('App\ConversationMembers', 'id', 'conversation_id');
    }

   /**
    Messages
    **/
    public function messages()
    {
    	return $this->belongsToMany('App\ConversationMessages', 'id', 'conversation_id');
    }

   /**
    MyInbox
    **/
    public function scopeMyInbox($query, $ntlogin, $search = null, $sort = null)
    {        
            $query->select([
                    'conversation_members.id AS convo_member_id',
                    'conversations.id AS convo_id',
                    'conversations.subject AS convo_subj',
                    DB::raw("MAX(users.name) AS name"),
                    DB::raw("MAX(conversation_messages.created_at) AS last_reply"),
                    DB::raw("CASE WHEN conversation_members.last_view > MAX(conversation_messages.created_at) THEN 1 ELSE 0 END AS last_viewed")
                    ])
                    ->leftJoin('conversation_messages', 'conversations.id', '=', 'conversation_messages.conversation_id')
                    ->leftJoin('conversation_members', 'conversations.id', '=', 'conversation_members.conversation_id')
                    ->leftJoin('users', 'users.ntlogin', '=', 'conversation_messages.ntlogin')
                    ->where('conversation_members.ntlogin', $ntlogin)
                    ->where('conversation_members.deleted_at', 0)
                    ->groupBy('conversations.id','conversation_members.id','conversations.subject','conversation_members.last_view')
                    ->orderBy(DB::raw("MAX(conversation_messages.created_at)"), 'DESC');

            if(!is_null($search)){
                $query->where(function($q) use($search){
                    $q->where('conversations.subject','LIKE', '%'.$search.'%')
                      ->orWhere('name','LIKE', '%'.$search.'%');
                  });
            }

            return $query;
    }
}
