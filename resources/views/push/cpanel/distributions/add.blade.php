@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->
	<div class="row">
       	<div class="large-12 columns">
       		<h4><i class="fa fa-group"></i> Distributions </h4>
       	</div>
    </div>

    <div class="row">
    	<div class="large-12 columns">
    		<table style="width:100%;">
    			<thead><th>Distribution</th></thead>
    			<tbody>
	    			@foreach ($distributions as $distribution)
	    				<tr>
	    					<td><a href="{{ route('distribution_edit', [$distribution->id]) }}">{{ $distribution->distribution }}</a></td>
	    				</tr>
	    			@endforeach
    			</tbody>
    		</table>
    		<span class='right'>{!! (new Landish\Pagination\ZurbFoundation($distributions))->render() !!}</span>
    	</div>    	
    </div>
<!-- Stops Container -->
@stop

{{-- @section('scripts')
  @include('push.scripts.timekeeping_js')
@stop --}}