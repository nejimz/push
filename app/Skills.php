<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skills extends Model
{
    //
     public $table = 'call_center_skills';
     public $timestamps = false;

     public function account()
     {
    	return $this->hasOne('App\Accounts', 'id', 'account_id');
     }

     public function transactions()
     {
     	return $this->hasMany('App\TransferLogTransactions', 'skill_id', 'id');
     }
}
