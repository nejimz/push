<!-- Attendance Policy Popup -->
<div id="attendancePolicyModal" class="reveal-modal medium" data-reveal aria-labelledby="attendancePolicy" aria-hidden="true" role="dialog">
  <img src="{{ asset('img/attendance-policy/1.jpg') }}">
  <img src="{{ asset('img/attendance-policy/2.jpg') }}">
  <img src="{{ asset('img/attendance-policy/3.jpg') }}">
  <img src="{{ asset('img/attendance-policy/4.jpg') }}">

  <img src="{{ asset('img/attendance-policy/5.jpg') }}">
  <img src="{{ asset('img/attendance-policy/6.jpg') }}">
  <img src="{{ asset('img/attendance-policy/7.jpg') }}">
  <img src="{{ asset('img/attendance-policy/8.jpg') }}">
  <br />
  <br />
  <br />
  <div data-alert class="alert-box info">
    By clicking I Accept, you confirm that you have read the terms and conditions, that you understand them and that you agree to be bound by them.
  </div>
  <a href="{{ route('attendance_policy') }}" class="button success" id="iAcceptAttendancePolicyBtn">I Accept</a>
</div>

<script type="text/javascript">
$('#attendancePolicyModal').foundation('reveal', 'open', {
  animation_speed: 0,
  close_on_background_click: false
});
</script>