<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MasterfileCivilStatus;

class MasterfileEmployeePayDetails extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'employeepaydetails';
    public $timestamps = false;

    public function getCivilStatusFormatAttribute()
    {
    	$row = MasterfileCivilStatus::where('civil_status_id', $this->civilStatus)->first();

        return $row->civil_status_name;

    }

    public function getTaxStatusFormatAttribute()
    {
    	$row = MasterfileTaxStatus::where('TaxStatusID', $this->taxStatus)->first();

        return $row->TaxStatusName;
    }
}
