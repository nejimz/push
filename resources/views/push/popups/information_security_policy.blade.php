<!-- Information Security Popup -->
    <div id="informationSecurityModal" class="reveal-modal medium" data-reveal aria-labelledby="informationSecurity" aria-hidden="true" role="dialog">
      <img src="{{ asset('img/it-policy-2014/page_1.jpg') }}">
      <img src="{{ asset('img/it-policy-2014/page_2.jpg') }}">
      <img src="{{ asset('img/it-policy-2014/page_3.jpg') }}">
      <img src="{{ asset('img/it-policy-2014/page_4.jpg') }}">
      <img src="{{ asset('img/it-policy-2014/page_5.jpg') }}">
      <br />
      <br />
      <br />
      <div data-alert class="alert-box info">
        By clicking I Accept, you confirm that you have read the terms and conditions, that you understand them and that you agree to be bound by them.
      </div>
      <a href="{{ route('information_security') }}" class="button success" id="iAcceptInformationSecurityBtn">I Accept</a>
    </div>

    <script type="text/javascript">
    $('#informationSecurityModal').foundation('reveal', 'open', {
      animation_speed: 0,
      close_on_background_click: false
    });
    </script>