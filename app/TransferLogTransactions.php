<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferLogTransactions extends Model
{
    //
     public $table = 'transfer_log_transactions';
     public $timestamps = false;

     public function calldetails()
     {
     	return $this->hasMany('App\TransferLogCallDetails', 'transaction_id', 'id');
     }
}
