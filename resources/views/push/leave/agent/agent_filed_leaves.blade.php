@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

  <div class="row">
    <div class="large-12 columns">
      <h4><i class="fa fa-sign-in"></i> Download Agent Filed Leave</h4>
    </div>
  </div><!-- row -->

  <form action="{{ $action }}" method="post" >
    <div class="row">

      <div class="large-6 columns">

          <div class="row">
            <div class="large-12 columns">
              <label for="reason" class="">Reason:</label>
              <input type="text" name="leave_date" id="leave_date" placeholder="Date" data-date-format="%m/%d/%Y" data-date />
            </div>
          </div>

          <div class="row">
            <div class="large-9 columns">
              {!! csrf_field() !!}
              <button type="submit" class="button">Download</button>
            </div>
            <div class="large-3 columns">&nbsp;</div>
          </div>
      </div><!-- large-6 columns -->

    </div><!-- row -->
  </form>

<!-- Stops -->
@stop