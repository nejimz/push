<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Announcements;
use App\UsersAnnouncement;
use Validator;


use Auth;

class DashboardController extends Controller
{
    /**
    Announcement Validator Rules
    **/
    public function announcementValidationRules() 
    {
        return [
            'editor1' => 'required'
        ];
    }
    /**
    Announcement Validator Messages
    **/
    public function announcementValidationMessages() 
    {
        return [
            'editor1.required' => 'Message is required.'
        ];
    }
    /**
    Index
    **/
    public function index(Request $request)
    {
        //
        $ntlogin = Auth::user()->ntlogin;
        $allowed_post = UsersAnnouncement::where('ntlogin', $ntlogin)->count();
        $count_announcement = Announcements::where('active', 1)->count();
        $announcements = Announcements::where('active', 1)
                        ->leftJoin('users', 'users.ntlogin', '=', 'announcements.ntlogin')
                        ->orderBy('created_at', 'DESC')
                        ->get();

        $data = [
            'allowed_post'=>$allowed_post,
            'count_announcement'=>$count_announcement,
            'announcements'=>$announcements
        ];

        return view('push.dashboard', $data);
    }
    /**
    Store
    **/
    public function store(Request $request)
    {
        //
        $validator = Validator::make(
            $request->all(),
            $this->announcementValidationRules(),
            $this->announcementValidationMessages()
        );

        if ($validator->fails())
        {
            return redirect() -> route('dashboard') -> withErrors($validator);
        }
        else
        {
            $announcement = new Announcements;
            $announcement->message  = $request->editor1;
            $announcement->ntlogin  = Auth::user()->ntlogin;
            $announcement->active   = 1;
            $announcement->save();

            return redirect() -> route('dashboard') -> with('announcementSuccess', 'Message successfully posted!');
        }
    }

}
