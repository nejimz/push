/**
	Inbox Notification
**/
var xhr_inbox = null;

$(function(){
	//Call message notification function on first load of page
	var start_id = setTimeout(call_message_notification, 5000);
	//Call message notification function on interval 1 min and 30 sec
	var refresh_id = setInterval(call_message_notification, 90000);//90000
});
//Message notification function
function call_message_notification()
{
	xhr_inbox =	$.ajax({
		type : 'get',
		url : $("#notification_url").val(),
		data : 'data=0',
		cache : false,
		/*headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},*/
		dataType : "json",
		beforeSend: function(xhr){
			if (xhr_inbox != null)
			{
				xhr_inbox.abort();
			}
		}
	}).done(function(data){
		//console.log(data);
		var  count = data['message_count'];
		
		if(count != 0)
		{
			retresponse = '<span class="round alert label">' + count + '</span>';

			$.titleAlert("You have " + count + " unread messages!",{ stopOnFocus:true });
		}
		else
		{
			$.titleAlert.stop();
			document.title = "PUSH";
			retresponse = '';
		}

		$("#message_notification").html(retresponse);

		$(window).focus(function() {
		    $.titleAlert.stop();
			document.title = "PUSH";
		});

		/*if(response)
		{
			retresponse = '<span class="round alert label">'+response+'</span>';
			if(response>1)
			{								
				$.titleAlert("You have "+response+" unread messages!",{ stopOnFocus:true });
			}
			else
			{
				$.titleAlert("You have "+response+" unread message!",{ stopOnFocus:true });
			}
			
		}
		else
		{
			$.titleAlert.stop();
			document.title = "PUSH";
			retresponse = '';
		}

		$("#message_notification").html(retresponse);

		$(window).focus(function() {
		    $.titleAlert.stop();
			document.title = "PUSH";
		});*/
	});
}

