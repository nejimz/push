<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferLogTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('transfer_log_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('skill_id')->references('id')->on('call_center_skills');
            $table->string('transaction');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('transfer_log_transactions');
    }
}
