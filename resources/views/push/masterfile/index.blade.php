@extends('push.layout.master')

@section('container')
@parent
<!-- Starts -->

  <div class="row">
    <div class="large-12 columns">
      <h4><i class="fa fa-users"></i> Masterfile</h4>
    </div>
  </div><!-- row -->

  <div class="row">
    <div class="large-6 columns">

      <form action="{{ route('masterfile_index') }}" method="get">
        <div class="row">
          <div class="large-8 columns">
            <input type="text" name="search" value="{{ $search }}" />
          </div>
          <div class="large-4 columns">
            &nbsp;
          </div>
        </div>

        <div class="row">
          <div class="large-8 columns">
            <select name="status">
              <option value="1" {{ ($status == 1)? 'selected="selected"' : '' }}>Active</option>
              <option value="2" {{ ($status == 2)? 'selected="selected"' : '' }}>NLE</option>
            </select>
          </div>
          <div class="large-4 columns">
            &nbsp;
          </div>
        </div>

        <div class="row">
          <div class="large-8 columns">
            <button type="submit" class="button"><i class="fa fa-search"></i> Search</button>
            <a href="{{ route('masterfile_create') }}" class="button success"><i class="fa fa-plus"></i> Employee</a>
          </div>
          <div class="large-4 columns">
            &nbsp;
          </div>
        </div>
      </form>

    </div>
    <div class="large-6 columns">
      &nbsp;
    </div>
  </div><!-- row -->

  <div class="row">
    <div class="large-12 columns">
      {!! str_replace('/?', '?', $employees->render()) !!}
      <table width="100%">
        <thead>
          <tr>
            <th width="10%"></th>
            <th width="15%">Employee No.</th>
            <th width="20%">Last Name</th>
            <th width="20%">First Name</th>
            <th width="20%">Middle Name</th>
            <th width="15%">Department</th>
          </tr>
        </thead>
        <tbody>
          @foreach($employees as $row)
            <tr>
              <td>
                <a title="Edit Employee" href="{{ route('masterfile_edit', $row->empID) }}"><i class="fa fa-edit fa-lg"></i></a>
                <a title="Employee Profile" href="{{ route('masterfile_profile', $row->empID) }}"><i class="fa fa-user fa-lg"></i></a>
                @if($status == 1)
                  <a title="Employee Separation" href="{{ route('masterfile_separation_create', $row->empID) }}"><i class="fa fa-unlink fa-lg"></i></a>
                @else
                  <a title="Edit Employee Separation" href="{{ route('masterfile_separation_edit', $row->empID) }}"><i class="fa fa-unlink fa-lg"></i></a>
                @endif
              </td>
              <td>{{ $row->employee_number }}</td>
              <td>{{ $row->lastName }}</td>
              <td>{{ $row->firstName }}</td>
              <td>{{ $row->middleName }}</td>
              <td>{{ $row->department_name or '' }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {!! str_replace('/?', '?', $employees->render()) !!}
    </div>
  </div><!-- row -->

<!-- Stops -->
@stop